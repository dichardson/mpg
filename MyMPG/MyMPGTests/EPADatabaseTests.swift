//
//  EPADatabaseTests.swift
//  MyMPG
//
//  Created by Douglas Richardson on 3/17/15.
//  Copyright (c) 2015 True Labs, Inc. All rights reserved.
//

import UIKit
import XCTest
import MyMPG

class EPADatabaseTests: XCTestCase {
    
    var database : EPAVehicleDatabase!
    
    override func setUp() {
        super.setUp()
        // Put setup code here. This method is called before the invocation of each test method in the class.
        database = EPAVehicleDatabase()
    }
    
    override func tearDown() {
        // Put teardown code here. This method is called after the invocation of each test method in the class.
        super.tearDown()
    }
    
    func testYears() {
        let years = database.years()
        XCTAssertGreaterThan(years.count, 0)
  
        let descending = years.sorted { $0 > $1 }
        XCTAssertEqual(years, descending)
        
        let yearsAscending = database.years(order: .Ascending)
        let ascending = years.sorted { $0 < $1 }
        XCTAssertEqual(yearsAscending, ascending)
        
        XCTAssertNotEqual(ascending, descending)
    }
    
    func testYearMakes() {
        let makesIn1885 = database.makesForYear(1885)
        XCTAssertEqual(makesIn1885.count, 0)
        
        let makes = database.makesForYear(1990)
        XCTAssertGreaterThan(makes.count, 0)
    }
    
    func testYearMakeModels() {
        let toyotaModelsIn1885 = database.modelsForYear(1885, make: "Toyota")
        XCTAssertEqual(toyotaModelsIn1885.count, 0)
        
        let nonexistantBrandModel = database.modelsForYear(2000, make: "Non Existent Brand")
        XCTAssertEqual(nonexistantBrandModel.count, 0)
        
        let models = database.modelsForYear(2005, make: "Toyota")
        XCTAssertGreaterThan(models.count, 0)
    }
    
    func testVehiclesByYearMakeModel() {
        let modelsIn1776 = database.epaVehiclesForYear(1776, make: "Toyota", model: "Corolla")
        XCTAssertEqual(modelsIn1776.count, 0)
        
        let fakeMakes = database.epaVehiclesForYear(1995, make: "Not A Real Make", model: "Corolla")
        XCTAssertEqual(fakeMakes.count, 0)
        
        let fakeModels = database.epaVehiclesForYear(1995, make: "Toyota", model: "Not A Real Model")
        XCTAssertEqual(fakeModels.count, 0)
        
        let vehicles = database.epaVehiclesForYear(1998, make: "Toyota", model: "Corolla")
        XCTAssertGreaterThan(vehicles.count, 0)
    }
    
    func testVehiclesById() {
        let vehicleWithIdThatDoesNotExist = database.epaVehicleForId(9999999)
        XCTAssertTrue(vehicleWithIdThatDoesNotExist == nil)
        
        let vehicle = database.epaVehicleForId(1)
        XCTAssertTrue(vehicle != nil)
    }

    func testYearQueryPerformance() {
        // This is an example of a performance test case.
        self.measureBlock() {
            self.database.years()
            return
        }
    }
    
    func testMakeQueryPerformance() {
        // This is an example of a performance test case.
        self.measureBlock() {
            self.database.makesForYear(1995)
            return
        }
    }
    
    func testModelQueryPerformance() {
        // This is an example of a performance test case.
        self.measureBlock() {
            self.database.modelsForYear(1995, make: "Toyota")
            return
        }
    }
    
    func testVehicleQueryPerformance() {
        // This is an example of a performance test case.
        self.measureBlock() {
            self.database.epaVehiclesForYear(1995, make: "Toyota", model: "Corolla")
            return
        }
    }
    
    func testVehicleByIdPerformance() {
        self.measureBlock() {
            self.database.epaVehicleForId(1)
            return
        }
    }
}
