//
//  UnitTests.swift
//  MyMPG
//
//  Created by Douglas Richardson on 3/13/15.
//  Copyright (c) 2015 True Labs, Inc. All rights reserved.
//

import UIKit
import XCTest
import MyMPG

class UnitTests: XCTestCase {
    
    let defaultAccuracy = 0.000001

    func testVolume() {
        let lPerUSGal = 3.785411784
        let usGalPerL = 1 / lPerUSGal
        let lPerImpGal = 4.54609
        let impGalPerL = 1 / lPerImpGal
        let usGalInOneImpGal = lPerImpGal * usGalPerL
        let impGalInOneUSGal = lPerUSGal * impGalPerL
        
        // From liters
        XCTAssertEqual(1, Volume(value: 1, unit: .liter).changeUnit(.liter).value)
        XCTAssertEqual(usGalPerL, Volume(value: 1, unit: .liter).changeUnit(.usGallon).value)
        XCTAssertEqual(impGalPerL, Volume(value: 1, unit: .liter).changeUnit(.imperialGallon).value)
        
        // From us gallons
        XCTAssertEqual(Volume(value: 1, unit: .usGallon).changeUnit(.liter).value, lPerUSGal)
        XCTAssertEqual(1, Volume(value: 1, unit: .usGallon).changeUnit(.usGallon).value)
        XCTAssertEqualWithAccuracy(Volume(value: 1, unit: .usGallon).changeUnit(.imperialGallon).value, impGalInOneUSGal, defaultAccuracy)
        
        // From imperial gallons
        XCTAssertEqual(Volume(value: 1, unit: .imperialGallon).changeUnit(.liter).value, lPerImpGal)
        XCTAssertEqualWithAccuracy(Volume(value: 1, unit: .imperialGallon).changeUnit(.usGallon).value, usGalInOneImpGal, defaultAccuracy)
        XCTAssertEqual(1, Volume(value: 1, unit: .imperialGallon).changeUnit(.imperialGallon).value)
    }
    
    func testValuePerVolumeUnit() {
        let litersPerUSGallon = 3.785411784
        let usGallonsPerLiter = 1.0 / litersPerUSGallon
        let litersPerImpGallon = 4.54609
        let impGallonsPerLiter = 1.0 / litersPerImpGallon
        
        // Working with values/unit is a little confusing so here are some examples.
        // To go from $/usGallon to $/Liter:
        //  $/usGallon * usGallons/Liter = $/Liter
        // To go from $/usGallon to $/imperialGallon:
        //  $/usGallon * usGallons/Liter * Liter/imperialGallons = $/imperialGallons
        let pricePerLiterOfOneUSGallon = 1.0 * usGallonsPerLiter
        let pricePerLiterOfOneImpGallon = 1.0 * impGallonsPerLiter
        let pricePerUSGallonOfOneLiter = 1.0 * litersPerUSGallon
        let pricePerImpGallonOfOneLiter = 1.0 * litersPerImpGallon
        let pricePerImpGallonOfOneUSGallon = pricePerLiterOfOneUSGallon * litersPerImpGallon
        let pricePerUSGallonOfOneImpGallon = pricePerLiterOfOneImpGallon * litersPerUSGallon
        
        
        // From per liter
        XCTAssertEqual(1, ValuePerVolumeUnit(value: 1, perUnit: .liter).changeUnit(.liter).value)
        XCTAssertEqualWithAccuracy(ValuePerVolumeUnit(value: 1, perUnit: .liter).changeUnit(.usGallon).value, pricePerUSGallonOfOneLiter, defaultAccuracy)
        XCTAssertEqual(pricePerImpGallonOfOneLiter, ValuePerVolumeUnit(value: 1, perUnit: .liter).changeUnit(.imperialGallon).value)
        
        // From per US Gallon
        XCTAssertEqual(pricePerLiterOfOneUSGallon, ValuePerVolumeUnit(value: 1, perUnit: .usGallon).changeUnit(.liter).value)
        XCTAssertEqual(1, ValuePerVolumeUnit(value: 1, perUnit: .usGallon).changeUnit(.usGallon).value)
        XCTAssertEqualWithAccuracy(ValuePerVolumeUnit(value: 1, perUnit: .usGallon).changeUnit(.imperialGallon).value, pricePerImpGallonOfOneUSGallon, defaultAccuracy)
        
        // From per Imperial Gallon
        XCTAssertEqual(1, ValuePerVolumeUnit(value: 1, perUnit: .imperialGallon).changeUnit(.imperialGallon).value)
        XCTAssertEqualWithAccuracy(ValuePerVolumeUnit(value: 1, perUnit: .imperialGallon).changeUnit(.usGallon).value, pricePerUSGallonOfOneImpGallon, defaultAccuracy)
        XCTAssertEqual(pricePerLiterOfOneImpGallon, ValuePerVolumeUnit(value: 1, perUnit: .imperialGallon).changeUnit(.liter).value)
        
        // From liters to something to liters
        XCTAssertEqual(1, ValuePerVolumeUnit(value: 1, perUnit: .liter).changeUnit(.liter).changeUnit(.liter).value)
        XCTAssertEqualWithAccuracy(ValuePerVolumeUnit(value: 1, perUnit: .liter).changeUnit(.usGallon).changeUnit(.liter).value, 1.0, defaultAccuracy)
        XCTAssertEqual(1, ValuePerVolumeUnit(value: 1, perUnit: .liter).changeUnit(.imperialGallon).changeUnit(.liter).value)
    }
    
    func testFuelEfficiency() {
        
        // From MPG
        XCTAssertEqual(1, FuelEfficiency(value: 1, unit: .mpg).changeUnit(.mpg)!.value)
        XCTAssertEqualWithAccuracy(FuelEfficiency(value: 1, unit: .mpg).changeUnit(.kmL)!.value, 0.425143707, 0.000000001)
        XCTAssertEqualWithAccuracy(FuelEfficiency(value: 1, unit: .mpg).changeUnit(.l100km)!.value, 235.215, 0.001)
        XCTAssertEqualWithAccuracy(FuelEfficiency(value: 1, unit: .mpg).changeUnit(.mpgImperial)!.value, 1.200949925, 0.000000001)
        
        // From km/L
        XCTAssertEqual(1, FuelEfficiency(value: 1, unit: .kmL).changeUnit(.kmL)!.value)
        XCTAssertEqualWithAccuracy(FuelEfficiency(value: 1, unit: .kmL).changeUnit(.mpg)!.value, 2.35214583, 0.00000001)
        XCTAssertEqual(NSDecimalNumber(integer: 100), FuelEfficiency(value: 1, unit: .kmL).changeUnit(.l100km)!.value)
        XCTAssertEqualWithAccuracy(FuelEfficiency(value: 1, unit: .kmL).changeUnit(.mpgImperial)!.value, 2.82480936, 0.00000001)
        
        // From L/100km
        XCTAssertEqual(1, FuelEfficiency(value: 1, unit: .l100km).changeUnit(.l100km)!.value)
        XCTAssertEqual(NSDecimalNumber(integer: 100), FuelEfficiency(value: 1, unit: .l100km).changeUnit(.kmL)!.value)
        XCTAssertEqualWithAccuracy(FuelEfficiency(value: 1, unit: .l100km).changeUnit(.mpg)!.value, 235.214583, 0.000001)
        XCTAssertEqualWithAccuracy(FuelEfficiency(value: 1, unit: .l100km).changeUnit(.mpgImperial)!.value, 282.480936, 0.000001)
        
        // From MPG Imperial
        XCTAssertEqual(1, FuelEfficiency(value: 1, unit: .mpgImperial).changeUnit(.mpgImperial)!.value)
        XCTAssertEqualWithAccuracy(FuelEfficiency(value: 1, unit: .mpgImperial).changeUnit(.kmL)!.value, 0.35400618, 0.00000001)
        XCTAssertEqualWithAccuracy(FuelEfficiency(value: 1, unit: .mpgImperial).changeUnit(.l100km)!.value, 282.480936, 0.000001)
        XCTAssertEqualWithAccuracy(FuelEfficiency(value: 1, unit: .mpgImperial).changeUnit(.mpg)!.value, 0.832674184, 0.000000001)
        
    }
    
    func testFuelEfficiencyWithZero() {
        // From MPG
        XCTAssertEqual(FuelEfficiency(value: 0, unit: .mpg).changeUnit(.mpg)!.value, 0)
        XCTAssertEqual(FuelEfficiency(value: 0, unit: .mpg).changeUnit(.kmL)!.value, 0)
        XCTAssertTrue(FuelEfficiency(value: 0, unit: .mpg).changeUnit(.l100km) == nil)
        XCTAssertEqual(FuelEfficiency(value: 0, unit: .mpg).changeUnit(.mpgImperial)!.value, 0)
        
        // From km/L
        XCTAssertEqual(FuelEfficiency(value: 0, unit: .kmL).changeUnit(.mpg)!.value, 0)
        XCTAssertEqual(FuelEfficiency(value: 0, unit: .kmL).changeUnit(.kmL)!.value, 0)
        XCTAssertTrue(FuelEfficiency(value: 0, unit: .kmL).changeUnit(.l100km) == nil)
        XCTAssertEqual(FuelEfficiency(value: 0, unit: .kmL).changeUnit(.mpgImperial)!.value, 0)
        
        // From L/100km
        XCTAssertTrue(FuelEfficiency(value: 0, unit: .l100km).changeUnit(.mpg) == nil)
        XCTAssertTrue(FuelEfficiency(value: 0, unit: .l100km).changeUnit(.kmL) == nil)
        XCTAssertEqual(FuelEfficiency(value: 0, unit: .l100km).changeUnit(.l100km)!.value, 0)
        XCTAssertTrue(FuelEfficiency(value: 0, unit: .l100km).changeUnit(.mpgImperial) == nil)
        
        // From MPG Imperial
        XCTAssertEqual(FuelEfficiency(value: 0, unit: .mpgImperial).changeUnit(.mpg)!.value, 0)
        XCTAssertEqual(FuelEfficiency(value: 0, unit: .mpgImperial).changeUnit(.kmL)!.value, 0)
        XCTAssertTrue(FuelEfficiency(value: 0, unit: .mpgImperial).changeUnit(.l100km) == nil)
        XCTAssertEqual(FuelEfficiency(value: 0, unit: .mpgImperial).changeUnit(.mpgImperial)!.value, 0)
    }
    
    func testDistance() {
        // From mile
        XCTAssertEqual(Distance(value: 1, unit: .mile).changeUnit(.kilometer).value, 1.609344)
        XCTAssertEqual(Distance(value: 1, unit: .mile).changeUnit(.mile).value, 1)
        
        // From kilometer
        XCTAssertEqualWithAccuracy(Distance(value: 1, unit: .kilometer).changeUnit(.mile).value, 0.621371, defaultAccuracy)
        XCTAssertEqual(Distance(value: 1, unit: .kilometer).changeUnit(.kilometer).value, 1)
    }
    
    func testValuePerDistanceUnit() {
        let onePerMile = ValuePerDistanceUnit(value: 1, perUnit: .mile)
        let onePerKm = ValuePerDistanceUnit(value: 1, perUnit: .kilometer)
        
        // From per mile
        XCTAssertEqual(onePerMile.changeUnit(.mile).value, 1)
        XCTAssertEqualWithAccuracy(onePerMile.changeUnit(.kilometer).value, 0.6213711922373, 0.0000000000001)
        
        // From per km
        XCTAssertEqual(onePerKm.changeUnit(.kilometer).value, 1)
        XCTAssertEqualWithAccuracy(onePerKm.changeUnit(.mile).value, 1.6093440, 0.0000001)
    }
}
