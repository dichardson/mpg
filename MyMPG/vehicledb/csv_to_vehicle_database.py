#!/usr/bin/python
# Usage: csv_to_vehicle_database.py <input_csv> <output_database>
# Convert an EPA vehicle database in comma separated value format
# into a sqlite database.

import csv
import sqlite3
import sys

if len(sys.argv) != 3:
    print "Missing required argument"
    print "Usage: csv_to_vehicle_database.py <input_csv> <output_database>"
    sys.exit(1)

inputCSVFile = sys.argv[1]
databaseFile = sys.argv[2]

print "Reading: %s" % inputCSVFile
print "Output: %s" % databaseFile

conn = sqlite3.connect(databaseFile)
c = conn.cursor()
c.execute('''
create table epa_vehicles(
    id integer primary key,
    year integer not null,
    make text not null,
    model text not null,
    transmission text not null,
    drive text not null,
    cylinders integer null,
    displacementX10 integer null,
    engine_description text null,
    transmission_description text null,
    fuel_type text null,
    turbocharged boolean,
    supercharged boolean,
    highway integer not null,
    city integer not null,
    combined integer not null)
        ''')

# CSV fields defined at http://www.fueleconomy.gov/feg/ws/index.shtml
ID='id'
YEAR='year'
MAKE='make'
MODEL='model'
DRIVE='drive'
TRANSMISSION='trany'
CYLINDERS='cylinders'
DISPLACEMENT='displ'
ENGINE_DESCRIPTION='eng_dscr'
TRANSMISSION_DESCRIPTION='trans_dscr'
FUEL_TYPE='fuelType'
TURBOCHARGED='tCharger'
SUPERCHARGED='sCharger'
HIGHWAY='highway08'
CITY='city08'
COMBINED='comb08'

def intOrNone(s):
    try:
        i = int(s)
        if str(i) != s:
            print "Loss of precision when convertion %s to integer" % s
            sys.exit(1)
        return i
    except ValueError as e:
        print "intOrNone: %s" % e
        return None

def intOrExit(s):
    i = intOrNone(s)
    if i == None:
        sys.exit(1)
    return i

uniquifyingColums=['year','make','model','transmission','drive','cylinders','displacementX10','engine_description','transmission_description','fuel_type','turbocharged','supercharged']
commaSeparatedUniquifyingColumns=",".join(uniquifyingColums)

def duplicates():
    c = conn.cursor()
    return c.execute(
            'select * from '
            '(select count(*) cnt, min(id) minid, ' + commaSeparatedUniquifyingColumns + ' from epa_vehicles ' + 
            'group by ' + commaSeparatedUniquifyingColumns + ' ) where cnt > 1'
            )

def delete_duplicates():
    andClause = " and ".join([x + '=?' for x in uniquifyingColums])
    deleteSQL = 'delete from epa_vehicles where id != ? and ' + andClause
    c = conn.cursor()
    dupes = list(duplicates())
    print "Deleting dupes from %d sets containing duplicates" % len(dupes)
    c.executemany(deleteSQL, [x[1:] for x in dupes])

with open(inputCSVFile, 'rU') as csvfile:
    reader = csv.DictReader(csvfile, delimiter=',', quotechar='"')
    for row in reader:
        displacementStr = row[DISPLACEMENT]
        displacementX10 = None
        cylinders = None
        if len(displacementStr) == 0 or displacementStr == "NA":
            displacementX10 = None
        else:
            try:
                floatDisplacementX10 = float(row[DISPLACEMENT])*10.0
                displacementX10 = int(floatDisplacementX10)
                if displacementX10 != float(displacementX10):
                    print "ERROR: loss of precision converting displacement %s for id %s" % (row[DISPLACEMENT], row[ID])
                    sys.exit(1)
            except ValueError as e:
                print e
                sys.exit(1)

        if len(row[CYLINDERS]) == 0 or row[CYLINDERS] == "NA":
            cylinders = None
        else:
            cylinders = intOrExit(row[CYLINDERS])

        turbocharged = row[TURBOCHARGED] == 'T'
        supercharged = row[SUPERCHARGED] == 'S'
    
        highway = intOrExit(row[HIGHWAY])
        city = intOrExit(row[CITY])
        combined = intOrExit(row[COMBINED])

        vehicle_row = [row[ID], row[YEAR], row[MAKE], row[MODEL], row[TRANSMISSION], row[DRIVE], cylinders, displacementX10, row[ENGINE_DESCRIPTION], row[TRANSMISSION_DESCRIPTION], row[FUEL_TYPE], turbocharged, supercharged, highway, city, combined]
        c.execute("insert into epa_vehicles values (?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)", vehicle_row)

print "Building index"
c.execute('''
create index year_index on epa_vehicles(year,make,model)
''')

# Make sure each EPA vehicle ID has a unique (year,make,model,transmission,drive,cylinders,displacementX10) set,
# otherwise the user won't know which one to pick if there are seemingly duplicate options.
print "Looking for duplicates."

delete_duplicates()
remainingDupes = list(duplicates())
if len(remainingDupes) > 0:
    for dupe in remainingDupes:
        print dupe
    print "ERROR: Found %d sets of duplicate vehicles. This creates ambiguity when user selects cars in app." % len(remainingDupes)
    print "This means you need to include more columns to distinguish between different car IDs."
    sys.exit(1)

print "Analyzing to update stats for query planner"
c.execute('ANALYZE')

conn.commit()

conn.close()

print "OK"
