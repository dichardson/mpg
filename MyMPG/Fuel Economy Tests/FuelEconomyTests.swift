//
//  FuelEconomyTests.swift
//  MyMPG
//
//  Created by Douglas Richardson on 3/27/15.
//  Copyright (c) 2015 True Labs, Inc. All rights reserved.
//

import UIKit
import XCTest
import MyMPG
import CoreData

class FuelEconomyTests: XCTestCase {
    
    var moc : NSManagedObjectContext!
    var car : Car!

    override func setUp() {
        super.setUp()
        
        let modelURL = NSBundle.mainBundle().URLForResource("MPG", withExtension: "momd")!
        let managedObjectModel = NSManagedObjectModel(contentsOfURL: modelURL)!
        let persistentStoreCoordinator = NSPersistentStoreCoordinator(managedObjectModel: managedObjectModel)
        var error : NSError?
        if persistentStoreCoordinator.addPersistentStoreWithType(NSInMemoryStoreType, configuration: nil, URL: nil, options: nil, error: &error) == nil {
            println("Error adding persistent store. \(error!)")
            abort()
        }
        
        moc = NSManagedObjectContext()
        moc.persistentStoreCoordinator = persistentStoreCoordinator
        
        car = NSEntityDescription.insertNewObjectForEntityForName("Car", inManagedObjectContext: moc) as! Car
        car.nickname = "test"
        car.odometerUnit = DistanceUnit.kilometer.rawValue
    }
    
    override func tearDown() {
        // Put teardown code here. This method is called after the invocation of each test method in the class.
        super.tearDown()
        moc = nil
        car = nil
    }
    
    private func fillupOdometer(odometer : NSDecimalNumber, volume : NSDecimalNumber, volumeUnit: VolumeUnit) -> Fillup {
        let f = NSEntityDescription.insertNewObjectForEntityForName("Fillup", inManagedObjectContext: moc) as! Fillup
        f.car = car
        f.date = NSDate()
        f.odometer = odometer
        f.volume = volume
        f.volumeUnit = volumeUnit.rawValue
        return f
    }
    
    private func fillupOdometer(odometer : NSDecimalNumber, liters : NSDecimalNumber) -> Fillup {
        return fillupOdometer(odometer, volume: liters, volumeUnit: .liter)
    }

    func testFuelEconomiesFrom2Fillups() {
        let f1 = fillupOdometer(0, liters: 1)
        let f2 = fillupOdometer(100, liters: 10)
        let ecos = fuelEconomiesFromFillups([f2, f1])
        XCTAssertEqual(ecos.count, 1)
        let eco = ecos[0]
        XCTAssertEqual(eco.beginOdometer, 0)
        XCTAssertEqual(eco.endOdometer, 100)
        XCTAssertEqual(eco.fillupCount, 1)
        XCTAssertEqual(eco.kilometers, 100)
        XCTAssertEqual(eco.liters, 10)
        XCTAssertTrue(eco.currencyCode == nil)
        XCTAssertTrue(eco.cost == nil)
        XCTAssertEqual(eco.fuelEfficiency!.changeUnit(.kmL)!.value, 10)
    }
    
    func testFuelEconomiesWithPartialFillup() {
        let f1 = fillupOdometer(0, liters: 1)
        let f2 = fillupOdometer(50, liters: 5)
        f2.isPartial = true
        let f3 = fillupOdometer(100, liters: 5)
        let ecos = fuelEconomiesFromFillups([f3, f2, f1])
        
        XCTAssertEqual(ecos.count, 1)
        let eco = ecos[0]
        XCTAssertEqual(eco.beginOdometer, 0)
        XCTAssertEqual(eco.endOdometer, 100)
        XCTAssertEqual(eco.fillupCount, 2)
        XCTAssertEqual(eco.kilometers, 100)
        XCTAssertEqual(eco.liters, 10)
        XCTAssertTrue(eco.currencyCode == nil)
        XCTAssertTrue(eco.cost == nil)
        XCTAssertEqual(eco.fuelEfficiency!.changeUnit(.kmL)!.value, 10)
    }
    
    func testFuelEconomiesWithMissedFillup() {
        let f1 = fillupOdometer(0, liters: 10)
        let f2 = fillupOdometer(1000, liters: 10)
        f2.missedLastFillup = true
        let f3 = fillupOdometer(1100, liters: 10)
        let ecos = fuelEconomiesFromFillups([f3, f2, f1])
        
        XCTAssertEqual(ecos.count, 1)
        let eco = ecos[0]
        XCTAssertEqual(eco.beginOdometer, 1000)
        XCTAssertEqual(eco.endOdometer, 1100)
        XCTAssertEqual(eco.fillupCount, 1)
        XCTAssertEqual(eco.kilometers, 100)
        XCTAssertEqual(eco.liters, 10)
        XCTAssertTrue(eco.currencyCode == nil)
        XCTAssertTrue(eco.cost == nil)
        XCTAssertEqual(eco.fuelEfficiency!.changeUnit(.kmL)!.value, 10)
    }
    
    func testFuelEconomiesWithMissedAndPartialFillup() {
        let f1 = fillupOdometer(0, liters: 10)
        let f2 = fillupOdometer(1000, liters: 10)
        f2.missedLastFillup = true
        let f3 = fillupOdometer(1050, liters: 5)
        f3.isPartial = true
        let f4 = fillupOdometer(1100, liters: 5)
        let ecos = fuelEconomiesFromFillups([f4, f3, f2, f1])
        
        XCTAssertEqual(ecos.count, 1)
        let eco = ecos[0]
        XCTAssertEqual(eco.beginOdometer, 1000)
        XCTAssertEqual(eco.endOdometer, 1100)
        XCTAssertEqual(eco.fillupCount, 2)
        XCTAssertEqual(eco.kilometers, 100)
        XCTAssertEqual(eco.liters, 10)
        XCTAssertTrue(eco.currencyCode == nil)
        XCTAssertTrue(eco.cost == nil)
        XCTAssertEqual(eco.fuelEfficiency!.changeUnit(.kmL)!.value, 10)
    }

    func testFuelEconomiesWithMultipleVolumeUnits() {
        let f1 = fillupOdometer(0, liters: 1)
        let f2 = fillupOdometer(100, liters: 10)
        let f3 = fillupOdometer(200, volume: 2.641720523581, volumeUnit: .usGallon)
        let ecos = fuelEconomiesFromFillups([f3, f2, f1])
        XCTAssertEqual(ecos.count, 2)
        
        XCTAssertEqual(ecos[0].beginOdometer, 100)
        XCTAssertEqual(ecos[0].endOdometer, 200)
        XCTAssertEqual(ecos[0].fillupCount, 1)
        XCTAssertEqual(ecos[0].kilometers, 100)
        XCTAssertEqualWithAccuracy(ecos[0].liters, 10, 0.000001)
        XCTAssertTrue(ecos[0].currencyCode == nil)
        XCTAssertTrue(ecos[0].cost == nil)
        XCTAssertEqualWithAccuracy(ecos[0].fuelEfficiency!.changeUnit(.kmL)!.value, 10, 0.000001)
        
        XCTAssertEqual(ecos[1].beginOdometer, 0)
        XCTAssertEqual(ecos[1].endOdometer, 100)
        XCTAssertEqual(ecos[1].fillupCount, 1)
        XCTAssertEqual(ecos[1].kilometers, 100)
        XCTAssertEqual(ecos[1].liters, 10)
        XCTAssertTrue(ecos[1].currencyCode == nil)
        XCTAssertTrue(ecos[1].cost == nil)
        XCTAssertEqual(ecos[1].fuelEfficiency!.changeUnit(.kmL)!.value, 10)
    }
    
    func testFuelEconomiesFrom2FillupsWithCosts() {
        let f1 = fillupOdometer(0, liters: 1)
        f1.currency = "USD"
        f1.pricePerVolumeUnit = 1
        
        let f2 = fillupOdometer(100, liters: 10)
        f2.currency = "USD"
        f2.pricePerVolumeUnit = 1
        
        let ecos = fuelEconomiesFromFillups([f2, f1])
        XCTAssertEqual(ecos.count, 1)
        let eco = ecos[0]
        XCTAssertTrue(eco.currencyCode == "USD")
        XCTAssertEqual(eco.cost!, 10)
    }
    
    func testFuelEconomiesFromFillupsWithCostsMultipleCurrencies() {
        let f1 = fillupOdometer(0, liters: 1)
        f1.currency = "USD"
        f1.pricePerVolumeUnit = 1000
        
        let f2 = fillupOdometer(100, liters: 1)
        f2.isPartial = true
        f2.currency = "MXN"
        f2.pricePerVolumeUnit = 1
        
        let f3 = fillupOdometer(200, liters: 1)
        f3.currency = "USD"
        f3.pricePerVolumeUnit = 2000
        
        let ecos = fuelEconomiesFromFillups([f3, f2, f1])
        XCTAssertEqual(ecos.count, 1)
        let eco = ecos[0]
        // Mixed currencies can't simply added together so nil is returned
        XCTAssertTrue(eco.currencyCode == nil)
        XCTAssertTrue(eco.cost == nil)
    }
}
