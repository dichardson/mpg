//
//  LocalizedStringTest.swift
//  MyMPG
//
//  Created by Douglas Richardson on 4/22/15.
//  Copyright (c) 2015 True Labs, Inc. All rights reserved.
//

import UIKit
import XCTest
import MyMPG

class LocalizedStringTest: XCTestCase {

    func testLocalizedString() {
        XCTAssertEqual("NOT_A_LOC_STRING", LocalizedString("NOT_A_LOC_STRING"))
        
        // Since we don't know what language is being used, can't compare directly against English "Options",
        // so just make sure it's not nil and not CAR_OPTIONS.
        let options = LocalizedString("CAR_OPTIONS")
        XCTAssertNotNil(options)
        XCTAssertNotEqual(options, "CAR_OPTIONS")
    }
    
    func testLocalizedFormatString() {
        let s = LocalizedFormatString("ODOMETER_WITH_UNIT_FORMAT", "Value1")
        XCTAssertNotNil(s)
        let r = s.rangeOfString("Value1", options: NSStringCompareOptions.LiteralSearch, range: nil, locale: nil)
        XCTAssertTrue(r != nil)
        let r2 = s.rangeOfString("Value2", options: NSStringCompareOptions.LiteralSearch, range: nil, locale: nil)
        XCTAssertTrue(r2 == nil)
    }

}
