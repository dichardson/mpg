//
//  FillupRowTableViewCell.swift
//  MyMPG
//
//  Created by John Hwang on 3/16/15.
//  Copyright (c) 2015 True Labs, Inc. All rights reserved.
//

import UIKit

class FillupRowTableViewCell: UITableViewCell {
    
    @IBOutlet weak var fillupOdometer: UILabel!
    @IBOutlet weak var fillupVolume: UILabel!
    @IBOutlet weak var fillupDate: UILabel!
    
    @IBOutlet weak var mpgTag: UITextField!
    @IBOutlet weak var detailTag: UITextField!
    
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
    
    override func setSelected(selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        
        // Configure the view for the selected state
    }
    
}
