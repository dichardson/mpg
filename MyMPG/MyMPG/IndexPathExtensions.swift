//
//  IndexPathExtensions.swift
//  MyMPG
//
//  Created by Douglas Richardson on 3/11/15.
//  Copyright (c) 2015 True Labs, Inc. All rights reserved.
//

import Foundation

// Given a 2 dimentional array [[E]], return an NSIndexPath to the first item passing the test.
func indexPathOfFirstObjectPassingTest<E>(array : [[E]], test : (E) -> Bool) -> NSIndexPath? {
    for section in array.startIndex..<array.endIndex {
        for row in array[section].startIndex..<array[section].endIndex {
            if test(array[section][row]) {
                return NSIndexPath(forRow: row, inSection: section)
            }
        }
    }
    return nil
}

// Given a 2 dimentional array [[E]], return an [NSIndexPath] to the items passing the test.
func indexPathsOfObjectsPassingTest<E>(array : [[E]], test : (E) -> Bool) -> [NSIndexPath] {
    var result : [NSIndexPath] = []
    for section in array.startIndex..<array.endIndex {
        for row in array[section].startIndex..<array[section].endIndex {
            if test(array[section][row]) {
                result.append(NSIndexPath(forRow: row, inSection: section))
            }
        }
    }
    return result
}
