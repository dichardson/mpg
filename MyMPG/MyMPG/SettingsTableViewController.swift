//
//  SettingsTableViewController
//  MyMPG
//
//  Created by Douglas Richardson on 3/30/15.
//  Copyright (c) 2015 True Labs, Inc. All rights reserved.
//

import UIKit
import CoreData
import MessageUI

class SettingsTableViewController: UITableViewController, MFMailComposeViewControllerDelegate {
    
    private enum Rows {
        case VolumeUnits
        case FuelEfficiencyUnits
        case Stations
        case Version
        case SendFeedback
        case DeveloperMode
    }
    
    private var sections : [[Rows]] = [
        [.VolumeUnits, .FuelEfficiencyUnits, .Stations],
        [.Version, .SendFeedback]
    ]
    
    private let userPrefs = UserPreferences()
    private let defaultCell = TableViewCellReuseIdAndType<Value1TableViewCell>(reuseId: "Default")
    private let buttonCell = TableViewCellReuseIdAndType<Value1TableViewCell>(reuseId: "ButtonCell")
    private let switchCell = TableViewCellReuseIdAndType<LabelSwitchTableViewCell>(reuseId: "Switch")

    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.navigationItem.title = LocalizedString("PREFERENCES")
        
        #if DEBUG
            sections.append([.DeveloperMode])
            #endif
        
        if !MFMailComposeViewController.canSendMail() {
            if let indexPath = indexPathOfFirstObjectPassingTest(sections, {$0 == .SendFeedback}) {
                sections[indexPath.section].removeAtIndex(indexPath.row)
            }
        }
        
        Analytics.sharedInstance.screenView("Settings")
    }
    
    override func viewWillAppear(animated: Bool) {
        super.viewWillAppear(animated)
        tableView.reloadData()
    }

    // MARK: - Table view data source

    override func numberOfSectionsInTableView(tableView: UITableView) -> Int {
        return sections.count
    }

    override func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return sections[section].count
    }
    
    override func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        switch sections[indexPath.section][indexPath.row] {
        case .VolumeUnits:
            let cell = defaultCell.dequeueFromTableView(tableView)
            cell.textLabel?.text = LocalizedString("FUEL_VOLUME_UNITS_TITLE")
            cell.detailTextLabel?.text = LocaleUnits().volumeUnitDisplayString
            cell.accessoryType = .DisclosureIndicator
            return cell
            
        case .FuelEfficiencyUnits:
            let cell = defaultCell.dequeueFromTableView(tableView)
            cell.textLabel?.text = LocalizedString("FUEL_EFFICIENCY_UNITS_TITLE")
            cell.detailTextLabel?.text = LocaleUnits().fuelEfficiencyUnitsDisplayString
            cell.accessoryType = .DisclosureIndicator
            return cell
            
        case .Stations:
            let cell = defaultCell.dequeueFromTableView(tableView)
            cell.textLabel?.text = LocalizedString("FILL_UP_REMINDERS")
            if userPrefs.fillupRemindersDisabled {
                cell.detailTextLabel?.text = LocalizedString("OFF")
            } else {
                let formatter = NSNumberFormatter()
                let countStr = formatter.stringFromNumber(AppDelegate.sharedInstance.gasStationLocationMonitor.monitoredRegions.count ?? 0) ?? ""
                let text = NSString(format: LocalizedString("REMINDERS_ON_WITH_STATION_COUNT_FORMAT"), countStr)
                cell.detailTextLabel?.text = text as String
            }
            cell.accessoryType = .DisclosureIndicator
            return cell
            
        case .Version:
            let cell = defaultCell.dequeueFromTableView(tableView)
            cell.textLabel?.text = LocalizedString("VERSION")
            cell.detailTextLabel?.text = appVersionString()
            cell.accessoryType = .None
            return cell
            
        case .SendFeedback:
            let cell = defaultCell.dequeueFromTableView(tableView)
            cell.textLabel?.text = LocalizedString("SEND_FEEDBACK")
            cell.detailTextLabel?.text = nil
            cell.accessoryType = .None
            return cell
            
        case .DeveloperMode:
            let cell = switchCell.dequeueFromTableView(tableView)
            cell.label.text = "Developer Mode" // not localized
            cell.switchView.on = userPrefs.developerModeEnabled
            cell.switchView.removeTarget(self, action: nil, forControlEvents: .AllEvents)
            cell.switchView.addTarget(self, action: Selector("developerModeSwitchChanged:"), forControlEvents: .ValueChanged)
            return cell
        }
    }
    
    override func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath) {
        switch sections[indexPath.section][indexPath.row] {
        case .VolumeUnits:
            let vc = SettingsVolumeUnitsTableViewController(style: tableView.style)
            self.showViewController(vc)
        case .FuelEfficiencyUnits:
            let vc = SettingsFuelEfficiencyUnitsTableViewController(style: tableView.style)
            self.showViewController(vc)
        case .Stations:
            let vc = SettingsStationNotificationsTableViewController(style: tableView.style)
            self.showViewController(vc)
        case .Version:
            break
        case .SendFeedback:
            let vc = MFMailComposeViewController()
            let appName = (NSBundle.mainBundle().infoDictionary?["CFBundleName"] as? String) ?? ""
            let model = UIDevice.currentDevice().model
            let iOSVersion = UIDevice.currentDevice().systemName + " " + UIDevice.currentDevice().systemVersion
            vc.setSubject(LocalizedString("FEEDBACK_SUBJECT"))
            let body = NSString(format: LocalizedString("FEEDBACK_BODY_FORMAT_STRING"), appName, appVersionString(), model, iOSVersion)
            vc.setMessageBody(body as String, isHTML: false)
            vc.setToRecipients(["fuel-economy@pev4me.com"])
            vc.mailComposeDelegate = self
            presentViewController(vc, animated: true, completion: nil)
        case .DeveloperMode:
            userPrefs.developerModeEnabled = !userPrefs.developerModeEnabled
        }
    }
    
    override func tableView(tableView: UITableView, shouldHighlightRowAtIndexPath indexPath: NSIndexPath) -> Bool {
        switch sections[indexPath.section][indexPath.row] {
        case .Version:
            return false
        case .DeveloperMode:
            return false
        default:
            return true
        }
    }
    
    // MARK: MFMailComposeViewControllerDelegate
    func mailComposeController(controller: MFMailComposeViewController!, didFinishWithResult result: MFMailComposeResult, error: NSError!) {
        controller.dismissViewControllerAnimated(true, completion: nil)
        
        if result.value == MFMailComposeResultFailed.value {
            println("Failed to send feedback e-mail. \(error)")
        }
    }
    
    func developerModeSwitchChanged(sender : UISwitch) {
        userPrefs.developerModeEnabled = sender.on
    }
}

private func appVersionString() -> String {
    let shortVersion = (NSBundle.mainBundle().infoDictionary?["CFBundleShortVersionString"] as? String) ?? ""
    let version = (NSBundle.mainBundle().infoDictionary?["CFBundleVersion"] as? String) ?? ""
    return "\(shortVersion) (\(version))"
}
