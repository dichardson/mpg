//
//  DriveStyleViewController.swift
//  MyMPG
//
//  Created by John Hwang on 3/5/15.
//  Copyright (c) 2015 True Labs, Inc. All rights reserved.
//

import UIKit

protocol DriveStyleViewControllerDelegate {
    func driveStyleViewControllerDidSelectStyle(controller : DriveStyleViewController, drivestyle : Float?)
}

class DriveStyleViewController: UIViewController {
    
    var delegate : DriveStyleViewControllerDelegate?
    var drivestyle : Float?

    class func newController() -> DriveStyleViewController {
        let sb = UIStoryboard(name: "DriveStyle", bundle: nil)
        return sb.instantiateInitialViewController() as! DriveStyleViewController
    }
    
    @IBOutlet weak var drivingStyleStopGoText: UITextField!
    
    @IBOutlet weak var drivingStyleHighwayText: UITextField!
    
    @IBAction func resetStyle(sender: UIButton) {
        
        drivingStyleSlider.value = 50
        drivingStyleStopGoText.hidden = true
        drivingStyleHighwayText.hidden = true
        delegate?.driveStyleViewControllerDidSelectStyle(self, drivestyle: nil)
    }
    
    @IBOutlet weak var drivingStyleSlider: UISlider!
    
    @IBAction func drivingStyleSliderChanged(sender: AnyObject) {
        
        drivingStyleStopGoText.hidden = false
        drivingStyleHighwayText.hidden = false
        refreshUI()
        delegate?.driveStyleViewControllerDidSelectStyle(self, drivestyle: drivingStyleSlider.value)
        
    }
    
    func refreshUI(){
        var stopGoValue = 100 - Int(drivingStyleSlider.value)
        var highwayValue = Int(drivingStyleSlider.value)
        drivingStyleStopGoText.text = "\(stopGoValue)%"
        drivingStyleHighwayText.text = "\(highwayValue)%"
    }
    
    override func viewDidLoad() {
        
        Analytics.sharedInstance.screenView("Fillup: Driving Style")
        
        drivingStyleStopGoText.hidden = true
        drivingStyleHighwayText.hidden = true
        
        if let ds = drivestyle {
            drivingStyleSlider.value = ds
            drivingStyleStopGoText.hidden = false
            drivingStyleHighwayText.hidden = false
        }
        
        super.viewDidLoad()
        

        // Do any additional setup after loading the view.
        refreshUI()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
}
