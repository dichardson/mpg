//
//  DefaultTableViewCell.swift
//  MyMPG
//
//  Created by Douglas Richardson on 3/18/15.
//  Copyright (c) 2015 True Labs, Inc. All rights reserved.
//

import UIKit

class DefaultTableViewCell: UITableViewCell {
    
    override init(style: UITableViewCellStyle, reuseIdentifier: String?) {
        super.init(style: .Default, reuseIdentifier: reuseIdentifier)
    }
    
    required init(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
    }
}
