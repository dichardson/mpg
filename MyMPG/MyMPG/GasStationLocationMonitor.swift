//
//  GasStationLocationMonitor
//  MyMPG
//
//  Created by Douglas Richardson on 2/18/15.
//  Copyright (c) 2015 True Labs, Inc. All rights reserved.
//

import Foundation
import CoreLocation
import UIKit

private let CoreLocationAuthorizationStatusDidChangeNotification = "CoreLocationAuthorizationChanged"
private let LocationUpdatedNotification = "LocationUpdatedNotification"

private let GasStationRegionRadiusInMeters = 1.0
private let CurrentLocationRequiredAccuracy = 100.0
private let MaxRegionMonitoringCount = 20 // Defined in Apple's doc for startMonitoringForRegion
private let LocationStalenessThresholdSeconds = 10.0
private let LocationUpdateTimeoutSeconds = 10.0
private let NearbyStationsRequestTimeout = 10.0
private let SecondsBeforeGasStationNotificationFired = 60.0 * 5.0

let LocalNotificationEventKey = "Event"
let GasStationDetectedEvent = "GasStationDetected"
let GasStationDetectedRegionIDKey = "RegionID"

//
// GasStationLocationMonitor keeps track of gas stations being monitored.
//
class GasStationLocationMonitor : NSObject, CLLocationManagerDelegate {
    
    let manager = CLLocationManager()
    
    override init() {
        super.init()
        manager.delegate = self
    }
    
    // TODO: Now that Swift supports the set type and monitoredRegions returns the set type, use that instead
    // of an array of CLRegions.
    var monitoredRegions : [CLRegion] {
        get {
            var regions : [CLRegion] = []
            for region in manager.monitoredRegions {
                if let r = region as? CLRegion {
                    regions.append(r)
                }
            }
            return regions
        }
    }
    
    deinit {
        manager.delegate = nil
    }
    
    class func capableOfShowingNearbyStations() -> Bool {
        return CLLocationManager.isMonitoringAvailableForClass(CLCircularRegion)
    }
    
    private func requestAlwaysAuthorization(callback : () -> ()) {
        
        if CLLocationManager.authorizationStatus() != .NotDetermined {
            // status already set. requesting a new status won't fire the delegate method
            // so just return now.
            callback()
            return
        }
        
        var observer : NSObjectProtocol?
        observer = NSNotificationCenter.defaultCenter().addObserverForName(
            CoreLocationAuthorizationStatusDidChangeNotification,
            object: self,
            queue: NSOperationQueue.mainQueue(),
            usingBlock: { (notification) -> Void in
                println("Got authorization request response")
                if let o = observer {
                    NSNotificationCenter.defaultCenter().removeObserver(o)
                }
                callback()
        })
        println("Making authorization request")
        manager.requestAlwaysAuthorization()
    }
    
    func currentLocation(var callback : (CLLocation?, UIAlertController?) -> ()) {
        requestAlwaysAuthorization { () -> () in
            if CLLocationManager.authorizationStatus() != .AuthorizedAlways {
                let alert = openSettingsAlertWithTitle(LocalizedString("LOCATION_SERVICES_DISABLED_TITLE"),
                    message: LocalizedString("LOCATION_SERIVCES_DISABLED_MESSAGE"))
                callback(nil, alert)
                return
            }
            
            let staleTime = NSDate(timeIntervalSinceNow: -LocationStalenessThresholdSeconds)
            
            var callbackOnce : ((CLLocation?, UIAlertController?) -> ())? = nil
            
            var observer : NSObjectProtocol?
            observer = NSNotificationCenter.defaultCenter().addObserverForName(LocationUpdatedNotification, object: self, queue: NSOperationQueue.mainQueue(), usingBlock: {
                (notification : NSNotification!) -> Void in
                if let locations = notification.userInfo?["Locations"] as? [CLLocation] {
                    // the locations are in time order, with the most recent being the last.
                    let possibleLocations = locations.filter({
                        ($0.timestamp.earlierDate(staleTime) == staleTime) &&
                            ($0.horizontalAccuracy <= CurrentLocationRequiredAccuracy)})
                    
                    if let location = possibleLocations.last {
                        callbackOnce?(location, nil)
                    }
                }
            })
            
            callbackOnce = {
                (location, alert) in
                callbackOnce = nil
                if let o = observer {
                    NSNotificationCenter.defaultCenter().removeObserver(o)
                    observer = nil
                }
                self.manager.stopUpdatingLocation()
                callback(location, alert)
            }
            
            
            let timeout = dispatch_time(DISPATCH_TIME_NOW, Int64(LocationUpdateTimeoutSeconds * Double(NSEC_PER_SEC)))
            dispatch_after(timeout, dispatch_get_main_queue()) { () -> Void in
                println("Timeout occurred")
                let alert = failureAlertWithTitle(LocalizedString("LOCATION_FAILED_TITLE"),
                    message: LocalizedString("LOCATION_TIMEOUT_MESSAGE"))
                callbackOnce?(nil, alert)
            }
            
            // Per startUpdatingLocation Apple docs, call stop followed by start
            // to generate a new initial location event, which we're waiting for in the observer.
            self.manager.stopUpdatingLocation()
            self.manager.startUpdatingLocation()
        }
    }
    
    /* Start monitoring the given station. If the station cannot be monitored, an 
    alert is presented over the given view controller. Since this method should *only*
    be called in response to a direct user action (like requesting a location) a view
    controller should always be immediately available. Said in another way, if you don't
    have a view controller when calling this method, you may be doing something wrong.
    */
    
    func startMonitoringRegionID(regionID : String, coordinate : CLLocationCoordinate2D, callback : (Bool, UIAlertController?) -> ()) {
        
        // Make sure we are allowed to monitor regions and put up local notifications.
        // Registering for user notifications will prompt the user the first time it's called.
        AppDelegate.sharedInstance.requestDesiredUserNotificationSettings()
            
        self.requestAlwaysAuthorization({ () -> () in
            if CLLocationManager.authorizationStatus() != .AuthorizedAlways {
                let alert = openSettingsAlertWithTitle(LocalizedString("LOCATION_SERVICES_DISABLED_TITLE"),
                    message: LocalizedString("LOCATION_SERIVCES_DISABLED_MESSAGE"))
                callback(false, alert)
                return
            }
            
            let region = CLCircularRegion(center: coordinate, radius: GasStationRegionRadiusInMeters, identifier: regionID)
            self.manager.startMonitoringForRegion(region)
            callback(true, nil)
        })
    }
    
    func stopMonitoringRegionID(regionID : String) {
        for region in manager.monitoredRegions {
            if let r = region as? CLRegion {
                if r.identifier == regionID {
                    manager.stopMonitoringForRegion(r)
                }
            } else {
                println("Unexpectedly got non CLRegion in monitoredRegion set")
            }
        }
    }
    
    func locationManager(manager: CLLocationManager!, didChangeAuthorizationStatus status: CLAuthorizationStatus) {
        NSNotificationCenter.defaultCenter().postNotificationName(
            CoreLocationAuthorizationStatusDidChangeNotification,
            object: self,
            userInfo: nil)
    }
    
    func locationManager(manager: CLLocationManager!, didDetermineState state: CLRegionState, forRegion region: CLRegion!) {
    }
    
    func locationManager(manager: CLLocationManager!, didEnterRegion region: CLRegion!) {
        println("Region entered: \(region)")
        
        switch UIApplication.sharedApplication().applicationState {
        case .Active:
            println("Application Active. Not doing anything.")
            // Application is active. Don't need to notify the user.
            break
        case .Inactive:
            println("Application Inactive. Not doing anything.")
            // Not sure what to do for this case. Need to test.
            break
        case .Background:
            // App is running, but in the background. This could happen if the app was launched
            // due to a CoreLocation region monitoring event.
            
            if UserPreferences().fillupRemindersDisabled {
                println("Detected gas station, but fill-up reminders are disabled.")
                return
            }
            
            let n = UILocalNotification()
            var userInfo = [LocalNotificationEventKey : GasStationDetectedEvent]
            if region.identifier != nil {
                userInfo[GasStationDetectedRegionIDKey] = region.identifier
            }
            n.userInfo = userInfo
            //Get station name
            var station : MyMPG.Station?
            if let stationURL = NSURL(string:region.identifier){
                if let id = AppDelegate.sharedInstance.managedObjectContext?.persistentStoreCoordinator?.managedObjectIDForURIRepresentation(stationURL) {
                    station = AppDelegate.sharedInstance.managedObjectContext?.objectWithID(id) as? MyMPG.Station
                }
            }
            let stationName = station?.name ?? LocalizedString("GAS_STATION_GENERIC")
            n.alertBody = NSString(format: LocalizedString("GAS_STATION_ARRIVAL_ALERT_BODY_FORMAT"), stationName) as String
            n.alertAction = LocalizedString("GAS_STATION_ARRIVAL_ALERT_ACTION")
            n.fireDate = NSDate(timeIntervalSinceNow: SecondsBeforeGasStationNotificationFired)
            UIApplication.sharedApplication().scheduleLocalNotification(n)
            
            println("Scheduled local notification for fill-up reminder")
            Analytics.sharedInstance.eventWithCategory(.UINotification, action: "scheduled", label: "arrive_station")
            break
            
        }
    }
    
    func locationManager(manager: CLLocationManager!, didExitRegion region: CLRegion!) {
        
        println("Region exited: \(region)")
        
        // Cancel any GasStationDetectedEvent Local Notifications that haven't fired by the time
        // the region was exited.
        
        let app = UIApplication.sharedApplication()
        for notification in app.scheduledLocalNotifications {
            if let n = notification as? UILocalNotification {
                if n.userInfo?[LocalNotificationEventKey] as? String == GasStationDetectedEvent {
                    if region.identifier == n.userInfo?[GasStationDetectedRegionIDKey] as? String {
                        app.cancelLocalNotification(n)
                        println("CANCEL NOTIFICATION")
                        Analytics.sharedInstance.eventWithCategory(.UINotification, action: "cancel", label: "arrive_station")
                    }
                }
            }
        }
    }
    
    func locationManager(manager: CLLocationManager!, didUpdateLocations locations: [AnyObject]!) {
        NSNotificationCenter.defaultCenter().postNotificationName(LocationUpdatedNotification,
            object: self,
            userInfo: ["Locations" : locations])
    }
}

func openSettingsAlertWithTitle(title : String, #message : String) -> UIAlertController {
    let alert = UIAlertController(title: title, message: message, preferredStyle: .Alert)
    // If we can get a URL to open settings, use that to give the user a convenient
    // option for getting to settings.
    if let settingsURL = NSURL(string:UIApplicationOpenSettingsURLString) {
        let cancelAction = UIAlertAction(title: LocalizedString("CANCEL"), style: .Cancel, handler: nil)
        let openSettingsAction = UIAlertAction(title: LocalizedString("OPEN_SETTINGS"), style: .Default) {
            (_) -> Void in
            UIApplication.sharedApplication().openURL(settingsURL)
            return // make swift happy about Void return
        }
        alert.addAction(openSettingsAction)
        alert.addAction(cancelAction)
    } else {
        // settings URL not available, so just show an OK button.
        let okAction = UIAlertAction(title: LocalizedString("OK"), style: .Default, handler: nil)
        alert.addAction(okAction)
    }
    return alert
}

func failureAlertWithTitle(title : String, #message : String) -> UIAlertController {
    let alert = UIAlertController(title: title, message: message, preferredStyle: .Alert)
    let okAction = UIAlertAction(title: LocalizedString("OK"), style: .Default, handler: nil)
    alert.addAction(okAction)
    return alert
}

extension GasStationLocationMonitor {
    func startMonitoringStation(station : MyMPG.Station, callback : (Bool, UIAlertController?) -> ()) {
        startMonitoringRegionID(station.regionID, coordinate: station.coordinate, callback: callback)
    }
    
    func stopMonitoringStation(station : MyMPG.Station) {
        stopMonitoringRegionID(station.regionID)
    }
}

extension MyMPG.Station {
    var regionID : String {
        get {
            assert(!self.objectID.temporaryID, "Don't use temporary IDs for regionIDs because they change")
            return self.objectID.URIRepresentation().absoluteString!
        }
    }
    
    var coordinate : CLLocationCoordinate2D {
        get {
            return CLLocationCoordinate2D(latitude: self.latitude.doubleValue, longitude: self.longitude.doubleValue)
        }
    }
}
