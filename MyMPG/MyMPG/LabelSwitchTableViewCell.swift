//
//  LabelSwitchTableViewCell.swift
//  MyMPG
//
//  Created by Douglas Richardson on 3/27/15.
//  Copyright (c) 2015 True Labs, Inc. All rights reserved.
//

import UIKit

class LabelSwitchTableViewCell: UITableViewCell {
    
    let label = UILabel()
    let switchView = UISwitch()
    
    override init(style: UITableViewCellStyle, reuseIdentifier: String?) {
        super.init(style: .Default, reuseIdentifier: reuseIdentifier)
        contentView.addSubview(label)
        contentView.addSubview(switchView)
    }
    
    required init(coder aDecoder: NSCoder) {
        fatalError("init(coder:) not implemented")
    }
    
    override func layoutSubviews() {
        super.layoutSubviews()
        let b = CGRectInset(contentView.bounds, 16, 0)
        
        let switchSize = switchView.sizeThatFits(b.size)
        switchView.frame = CGRectMake(CGRectGetMaxX(b)-switchSize.width, CGRectGetMidY(b)-switchSize.height/2.0, switchSize.width, switchSize.height)
        
        let bMinusSwitchSize = CGSizeMake(CGRectGetWidth(b)-switchSize.width, switchSize.height)
        let labelDesiredSize = label.sizeThatFits(bMinusSwitchSize)
        let labelSize = CGSizeMake(min(bMinusSwitchSize.width, labelDesiredSize.width), labelDesiredSize.height)
        label.frame = CGRectMake(CGRectGetMinX(b), CGRectGetMidY(b) - labelSize.height/2.0, labelSize.width, labelSize.height)
    }
}

class SubtitleSwitchTableViewCell: UITableViewCell {
    
    let label = UILabel()
    let subtitle = UILabel()
    let switchView = UISwitch()
    
    override init(style: UITableViewCellStyle, reuseIdentifier: String?) {
        super.init(style: .Default, reuseIdentifier: reuseIdentifier)
        subtitle.font = UIFont.systemFontOfSize(UIFont.smallSystemFontSize())
        contentView.addSubview(label)
        contentView.addSubview(subtitle)
        contentView.addSubview(switchView)
    }
    
    required init(coder aDecoder: NSCoder) {
        fatalError("init(coder:) not implemented")
    }
    
    override func layoutSubviews() {
        super.layoutSubviews()
        let b = CGRectInset(contentView.bounds, 16, 0)
        
        let switchSize = switchView.sizeThatFits(b.size)
        switchView.frame = CGRectMake(CGRectGetMaxX(b)-switchSize.width, CGRectGetMidY(b)-switchSize.height/2.0, switchSize.width, switchSize.height)
        
        let bMinusSwitchSize = CGSizeMake(CGRectGetWidth(b)-switchSize.width, switchSize.height)
        
        let labelDesiredSize = label.sizeThatFits(bMinusSwitchSize)
        let labelSize = CGSizeMake(min(bMinusSwitchSize.width, labelDesiredSize.width), labelDesiredSize.height)
        
        let subtitleDesiredSize = subtitle.sizeThatFits(bMinusSwitchSize)
        let subtitleSize = CGSizeMake(min(bMinusSwitchSize.width, subtitleDesiredSize.width), subtitleDesiredSize.height)
        let combinedLabelHeights = labelSize.height + subtitleSize.height
        
        label.frame = CGRectMake(CGRectGetMinX(b), CGRectGetMidY(b) - combinedLabelHeights/2.0, labelSize.width, labelSize.height)
        subtitle.frame = CGRectMake(CGRectGetMinX(b), CGRectGetMaxY(label.frame), subtitleSize.width, subtitleSize.height)
    }
}
