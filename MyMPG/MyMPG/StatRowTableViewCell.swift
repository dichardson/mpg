//
//  StatRowTableViewCell.swift
//  MyMPG
//
//  Created by Douglas Richardson on 3/5/15.
//  Copyright (c) 2015 True Labs, Inc. All rights reserved.
//

import UIKit

class StatRowTableViewCell: UITableViewCell {
    let leftLabel = UILabel()
    let middleLabel = UILabel()
    let rightLabel = UILabel()
    
    override init(style: UITableViewCellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        
        leftLabel.textAlignment = .Left
        middleLabel.textAlignment = .Center
        rightLabel.textAlignment = .Right
        
        contentView.addSubview(leftLabel)
        contentView.addSubview(middleLabel)
        contentView.addSubview(rightLabel)
    }

    required init(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override func layoutSubviews() {
        super.layoutSubviews()
        let padding = CGFloat(8)
        let b = contentView.bounds
        let availableWidth = CGRectGetWidth(b) - padding - padding
        let labelWidth = floor(availableWidth / 3.0)
        
        let h = CGRectGetHeight(b)
        
        leftLabel.frame = CGRectMake(padding, 0, labelWidth, h)
        middleLabel.frame = CGRectMake(CGRectGetMaxX(leftLabel.frame), 0, labelWidth, h)
        rightLabel.frame = CGRectMake(CGRectGetMaxX(middleLabel.frame), 0, labelWidth, h)
    }
}
