//
//  SubtitleTableViewCell.swift
//  MyMPG
//
//  Created by Douglas Richardson on 3/5/15.
//  Copyright (c) 2015 True Labs, Inc. All rights reserved.
//

import UIKit

class SubtitleTableViewCell: UITableViewCell {
    
    override init(style: UITableViewCellStyle, reuseIdentifier: String?) {
        super.init(style: .Subtitle, reuseIdentifier: reuseIdentifier)
    }

    required init(coder aDecoder: NSCoder) {
        // This class is useful for tableviews implemented in code with registerClass,
        // not interface builder where a detail cell can be specified directly without the class.
        fatalError("init(coder:) has not been implemented")
    }
}

class Value1TableViewCell: UITableViewCell {
    
    override init(style: UITableViewCellStyle, reuseIdentifier: String?) {
        super.init(style: .Value1, reuseIdentifier: reuseIdentifier)
    }
    
    required init(coder aDecoder: NSCoder) {
        // This class is useful for tableviews implemented in code with registerClass,
        // not interface builder where a detail cell can be specified directly without the class.
        fatalError("init(coder:) has not been implemented")
    }
}
