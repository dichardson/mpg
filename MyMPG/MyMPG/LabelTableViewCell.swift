//
//  LabelTableViewCell.swift
//  MyMPG
//
//  Created by Douglas Richardson on 3/11/15.
//  Copyright (c) 2015 True Labs, Inc. All rights reserved.
//

import UIKit

class LabelTableViewCell: SingleViewCell {
    var label : UILabel { get { return view as! UILabel } }
    
    override init(style: UITableViewCellStyle, reuseIdentifier: String?) {
        super.init(style: .Default, reuseIdentifier: reuseIdentifier)
        setSubview(UILabel())
    }

    required init(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}
