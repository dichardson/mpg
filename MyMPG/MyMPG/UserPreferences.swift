//
//  UserPreferences.swift
//  MyMPG
//
//  Created by Douglas Richardson on 3/30/15.
//  Copyright (c) 2015 True Labs, Inc. All rights reserved.
//

import Foundation

class UserPreferences {
    let userDefaults = NSUserDefaults.standardUserDefaults()
    
    private let fuelVolumeUnitsKey = "fuelVolumeUnits"
    var fuelVolumeUnits : VolumeUnit? {
        get {
            if let v = userDefaults.stringForKey(fuelVolumeUnitsKey) {
                return VolumeUnit(rawValue: v)
            } else {
                return nil
            }
        }
        set {
            if let value = newValue {
                userDefaults.setObject(value.rawValue, forKey: fuelVolumeUnitsKey)
            } else {
                userDefaults.removeObjectForKey(fuelVolumeUnitsKey)
            }
        }
    }
    
    private let fuelEfficiencyUnitsKey = "fuelEfficiencyUnits"
    var fuelEfficiencyUnits : FuelEfficiencyUnits? {
        get {
            if let v = userDefaults.stringForKey(fuelEfficiencyUnitsKey) {
                return FuelEfficiencyUnits(rawValue: v)
            } else {
                return nil
            }
        }
        set {
            if let value = newValue {
                userDefaults.setObject(value.rawValue, forKey: fuelEfficiencyUnitsKey)
            } else {
                userDefaults.removeObjectForKey(fuelEfficiencyUnitsKey)
            }
        }
    }
    
    private let fillupRemindersDisabledKey = "fillupRemindersDisabled"
    var fillupRemindersDisabled : Bool {
        get {
            return userDefaults.boolForKey(fillupRemindersDisabledKey)
        }
        set {
            userDefaults.setBool(newValue, forKey: fillupRemindersDisabledKey)
        }
    }
    
    private let developerModeEnabledKey = "developerModeEnabled"
    var developerModeEnabled : Bool {
        get {
            return userDefaults.boolForKey(developerModeEnabledKey)
        }
        set {
            userDefaults.setBool(newValue, forKey: developerModeEnabledKey)
        }
    }
}
