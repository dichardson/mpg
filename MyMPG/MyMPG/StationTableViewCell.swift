//
//  StationTableViewCell.swift
//  MyMPG
//
//  Created by Douglas Richardson on 2/13/15.
//  Copyright (c) 2015 True Labs, Inc. All rights reserved.
//

import UIKit

class StationTableViewCell: UITableViewCell {
    
    // using a textfield as a label to get disabled color for free
    @IBOutlet var labelTextField : UITextField!

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
