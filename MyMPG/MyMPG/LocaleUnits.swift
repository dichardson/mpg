//
//  LocaleUnits.swift
//  MyMPG
//
//  Created by Douglas Richardson on 2/12/15.
//  Copyright (c) 2015 True Labs, Inc. All rights reserved.
//

import Foundation

struct LocaleUnits {
    let distanceUnit : DistanceUnit
    let volumeUnit : VolumeUnit
    let fuelEfficiencyUnits : FuelEfficiencyUnits
    let locale : NSLocale
    let isoCurrencyCode : String
    
    // MARK: Display Strings for the UI
    let distanceUnitDisplayString : String
    let abbreviatedVolumeUnitDisplayString : String
    let volumeUnitDisplayString : String
    let pricePerVolumeFormatString : String
    let fuelEfficiencyUnitsDisplayString : String
    let shortFuelEfficiencyUnitsDisplayString : String
    
    // Capture Units currently in use. This value should be cached long enough to provide
    // a consistent experience, but not longer than necessary in case the user changes settings
    // that would effect which units are in use.
    // For example, if collecting Fill Up information, capture the units that should currently
    // be in use and cache them for the life of that Fill Up screen, but no longer.
    init() {
        locale = NSLocale.currentLocale()
        
        // First, automatically determine the units...
        var (volumeUnit, distanceUnit, fuelEfficiencyUnits) = locale.autoDetectUnits()
        
        // Then, if the user has overriden them, use those instead.
        let userPrefs = UserPreferences()
        if let usersUnits = userPrefs.fuelEfficiencyUnits {
            fuelEfficiencyUnits = usersUnits
        }
        
        if let usersUnits = userPrefs.fuelVolumeUnits {
            volumeUnit = usersUnits
        }
        
        self.volumeUnit = volumeUnit
        self.distanceUnit = distanceUnit
        self.fuelEfficiencyUnits = fuelEfficiencyUnits
        
        isoCurrencyCode = (locale.objectForKey(NSLocaleCurrencyCode) as? String) ?? "USD"
        let distanceKeys = DistanceUnitLocalizationKeys(distanceUnit:distanceUnit)
        distanceUnitDisplayString = LocalizedString(distanceKeys.distanceUnitAbbreviationKey)
        
        let volumeKeys = VolumeUnitLocalizationKeys(volumeUnit:volumeUnit)
        abbreviatedVolumeUnitDisplayString = LocalizedString(volumeKeys.abbreviatedVolumeUnitKey)
        volumeUnitDisplayString = LocalizedString(volumeKeys.volumeUnitKey)
        pricePerVolumeFormatString = LocalizedString(volumeKeys.pricePerVolumeFormatKey)
        
        let fuelEfficiencyUnitKeys = FuelEfficiencyLocalizationKeys(fuelEfficiencyUnits: fuelEfficiencyUnits)
        fuelEfficiencyUnitsDisplayString = LocalizedString(fuelEfficiencyUnitKeys.fuelEfficiencyUnitsKey)
        shortFuelEfficiencyUnitsDisplayString = LocalizedString(fuelEfficiencyUnitKeys.shortFuelEfficiencyUnitsKey)
    }
}

struct DistanceUnitLocalizationKeys {
    let distanceUnitAbbreviationKey : String
    let distanceUnitKey : String
    
    init(distanceUnit : DistanceUnit) {
        switch(distanceUnit) {
        case .mile:
            distanceUnitAbbreviationKey = "MILES_ABBREV"
            distanceUnitKey = "MILES"
        case .kilometer:
            distanceUnitAbbreviationKey = "KM_ABBREV"
            distanceUnitKey = "KILOMETERS"
        }
    }
}

struct VolumeUnitLocalizationKeys {
    let abbreviatedVolumeUnitKey : String
    let volumeUnitKey : String
    let pricePerVolumeFormatKey : String
    
    init(volumeUnit : VolumeUnit) {
        switch(volumeUnit) {
        case .usGallon:
            abbreviatedVolumeUnitKey = "US_GALLON_ABBREV"
            volumeUnitKey = "US_GALLONS_LABEL"
            pricePerVolumeFormatKey = "PRICE_PER_GALLON_LABEL_FORMAT"
        case .imperialGallon:
            abbreviatedVolumeUnitKey = "IMP_GALLON_ABBREV"
            volumeUnitKey = "IMP_GALLONS_LABEL"
            pricePerVolumeFormatKey = "PRICE_PER_GALLON_LABEL_FORMAT"
        case .liter:
            abbreviatedVolumeUnitKey = "LITER_ABBREV"
            volumeUnitKey = "LITERS_LABEL"
            pricePerVolumeFormatKey = "PRICE_PER_LITER_LABEL_FORMAT"
        }
    }
}

struct FuelEfficiencyLocalizationKeys {
    let fuelEfficiencyUnitsKey : String
    let shortFuelEfficiencyUnitsKey : String
    init(fuelEfficiencyUnits : FuelEfficiencyUnits) {
        switch(fuelEfficiencyUnits) {
        case .mpg:
            fuelEfficiencyUnitsKey = "MILES_PER_GALLON"
            shortFuelEfficiencyUnitsKey = "MPG_SHORT"
        case .kmL:
            fuelEfficiencyUnitsKey = "KM_PER_LITER"
            shortFuelEfficiencyUnitsKey = fuelEfficiencyUnitsKey
        case .l100km:
            fuelEfficiencyUnitsKey = "LITERS_PER_100KM"
            shortFuelEfficiencyUnitsKey = fuelEfficiencyUnitsKey
        case .mpgImperial:
            fuelEfficiencyUnitsKey = "MILES_PER_GALLON_IMPERIAL"
            shortFuelEfficiencyUnitsKey = "MPG_SHORT"
        }
    }
}
