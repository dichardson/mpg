//
//  LocaleExtensions.swift
//  MyMPG
//
//  Created by Douglas Richardson on 4/3/15.
//  Copyright (c) 2015 True Labs, Inc. All rights reserved.
//

import Foundation

extension NSLocale {
    // Auto-detect the user's units based only on the current locale. This does not take into
    // account the user's explicit preferences.
    func autoDetectUnits() -> (VolumeUnit, DistanceUnit, FuelEfficiencyUnits) {
        
        var volumeUnit = VolumeUnit.liter
        var distanceUnit = DistanceUnit.kilometer
        var fuelEfficiencyUnits = FuelEfficiencyUnits.kmL
        
        let useMetric = (objectForKey(NSLocaleUsesMetricSystem) as? NSNumber)?.boolValue ?? true
        if useMetric {
            volumeUnit = .liter
            distanceUnit = .kilometer
            fuelEfficiencyUnits = .kmL
        } else {
            volumeUnit = .usGallon
            distanceUnit = .mile
            fuelEfficiencyUnits = .mpg
        }
        
        if let code = (objectForKey(NSLocaleCountryCode) as? String)?.uppercaseString {
            if let units = countrySpecificFuelEfficiencyUnits[code] {
                fuelEfficiencyUnits = units
            }
        }
        
        if let code = (objectForKey(NSLocaleCountryCode) as? String)?.uppercaseString {
            if let units = countrySpecificFuelEfficiencyUnits[code] {
                fuelEfficiencyUnits = units
            }
        }
        
        return (volumeUnit, distanceUnit, fuelEfficiencyUnits)
    }
}

private let countrySpecificFuelEfficiencyUnits : [String : FuelEfficiencyUnits] = [
    // From:
    // https://www.academia.edu/1212019/A_review_on_global_fuel_economy_standards_labels_and_technologies_in_the
    // http://en.wikipedia.org/wiki/Fuel_economy_in_automobiles#Units_of_measure
    // http://en.wikipedia.org/wiki/Fuel_efficiency
    // and many extrapolated from the wikepedia entry (i.e., the statement that most european nations use L/100 km).
    "AL": .l100km,  // Albania
    "AR": .kmL,     // Argentinia
    "AU": .l100km,  // Australia
    "BA": .l100km,  // Bosnia and Herzegovina
    "BE": .l100km,  // Belgium
    "BG": .l100km,  // Bulgaria
    "BR": .kmL,     // Brazil
    "CA": .l100km,  // Canada
    "CH": .l100km,  // Switzerland
    "CL": .kmL,     // Chile
    "CN": .l100km,  // China
    "CO": .kmL,     // Colombia
    "CY": .l100km,  // Cyprus
    "CZ": .l100km,  // Czech Republic
    "DE": .l100km,  // Germany
    "DK": .kmL,     // Denmark
    "EE": .l100km,  // Estonia
    "ES": .l100km,  // Spain
    "FR": .l100km,  // France
    "GB": .l100km,  // United Kingdom
    "GR": .l100km,  // Greece
    "HK": .l100km,  // Hong Kong
    "HR": .l100km,  // Croatia
    "HU": .l100km,  // Hungary
    "ID": .kmL,     // Indonesia
    "IE": .l100km,  // Ireland
    "IL": .l100km,  // Israel
    "IM": .l100km,  // Isle of Man
    "IN": .kmL,     // India
    "IS": .l100km,  // Iceland
    "IT": .kmL,     // Italy
    "JP": .kmL,     // Japan
    "KR": .kmL,     // South Korea
    "LI": .l100km,  // Liechtenstein
    "LT": .l100km,  // Lithuania
    "LU": .l100km,  // Luxembourg
    "LV": .l100km,  // Latvia
    "MC": .l100km,  // Monaco
    "MD": .l100km,  // Moldova
    "ME": .l100km,  // Montenegro
    "MK": .l100km,  // Macedonia
    "MT": .l100km,  // Malta
    "MX": .kmL,     // Mexico
    "NL": .kmL,     // Netherlands
    "NZ": .l100km,  // New Zealand
    "PK": .l100km,  // Pakistan
    "PL": .l100km,  // Poland
    "PT": .l100km,  // Portugal
    "RO": .l100km,  // Romania
    "RS": .l100km,  // Serbia
    "RU": .l100km,  // Russia
    "SG": .kmL,     // Singapore
    "SI": .l100km,  // Slovenia
    "SK": .l100km,  // Slovakia
    "SM": .l100km,  // San Marino
    "TR": .l100km,  // Turkey
    "UA": .l100km,  // Ukraine
    "US": .mpg,     // United States
    "ZA": .l100km,  // South Africa
]
