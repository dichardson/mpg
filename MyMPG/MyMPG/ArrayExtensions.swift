//
//  ArrayExtensions.swift
//  MyMPG
//
//  Created by Douglas Richardson on 2/12/15.
//  Copyright (c) 2015 True Labs, Inc. All rights reserved.
//

import Foundation

extension Array {
    func indexOfFirstObjectPassingTest(test : (Element) -> Bool) -> Int? {
        for i in startIndex..<endIndex {
            let e = self[i]
            if test(e) {
                return i
            }
        }
        return nil
    }
    
    func indexesOfObjectsPassingTest(test : (Element) -> Bool) -> [Int] {
        var result = [Int]()
        
        for i in startIndex..<endIndex {
            let e = self[i]
            if test(e) {
                result.append(i)
            }
        }
        return result
    }
}
