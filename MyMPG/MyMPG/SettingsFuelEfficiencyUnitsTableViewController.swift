//
//  SettingsFuelEfficiencyUnitsTableViewController.swift
//  MyMPG
//
//  Created by Douglas Richardson on 3/31/15.
//  Copyright (c) 2015 True Labs, Inc. All rights reserved.
//

import UIKit

class SettingsFuelEfficiencyUnitsTableViewController: UITableViewController {

    private let fuelEfficiencyUnits : [FuelEfficiencyUnits] = [.kmL, .l100km, .mpg, .mpgImperial]
    private let userPrefs = UserPreferences()

    override func viewDidLoad() {
        super.viewDidLoad()
        self.navigationItem.title = LocalizedString("FUEL_EFFICIENCY_UNITS_TITLE")
        Analytics.sharedInstance.screenView("Settings: Fuel Efficiency")
    }

    // MARK: - Table view data source

    override func numberOfSectionsInTableView(tableView: UITableView) -> Int {
        return 1
    }

    override func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return fuelEfficiencyUnits.count + 1 // +1 for automatic
    }

    private let defaultCell = TableViewCellReuseIdAndType<Value1TableViewCell>(reuseId: "Default")

    override func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        let cell = defaultCell.dequeueFromTableView(tableView)
        if indexPath.row == 0 {
            cell.textLabel?.text = settingDisplayStringForFuelEfficiencyUnits(nil)
            let (_, _, autoDetectFuelEfficiencyUnit) = NSLocale.currentLocale().autoDetectUnits()
            let locKeys = FuelEfficiencyLocalizationKeys(fuelEfficiencyUnits: autoDetectFuelEfficiencyUnit)
            cell.detailTextLabel?.text = LocalizedString(locKeys.fuelEfficiencyUnitsKey)
            cell.accessoryType = userPrefs.fuelEfficiencyUnits == nil ? .Checkmark : .None
        } else {
            let fuelEfficiencyUnit = fuelEfficiencyUnits[indexPath.row - 1]
            cell.textLabel?.text = settingDisplayStringForFuelEfficiencyUnits(fuelEfficiencyUnit)
            cell.detailTextLabel?.text = nil
            cell.accessoryType = userPrefs.fuelEfficiencyUnits == fuelEfficiencyUnit ? .Checkmark : .None
        }
        
        return cell
    }

    override func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath) {
        if indexPath.row == 0 {
            userPrefs.fuelEfficiencyUnits = nil
        } else {
            userPrefs.fuelEfficiencyUnits = fuelEfficiencyUnits[indexPath.row - 1]
        }
        tableView.reloadData()
    }
    
    override func viewWillDisappear(animated: Bool) {
        if userPrefs.fuelEfficiencyUnits != nil {
            var action = (NSLocale.currentLocale().objectForKey(NSLocaleCountryCode) as? String)?.uppercaseString ?? "N/A"
            var label = userPrefs.fuelEfficiencyUnits?.rawValue
            Analytics.sharedInstance.eventWithCategory(.SelectFuelEfficiencyUnit, action: action, label: label)
        }
    }
}

private func settingDisplayStringForFuelEfficiencyUnits(fuelEfficiencyUnits : FuelEfficiencyUnits?) -> String {
    if let unit = fuelEfficiencyUnits {
        return LocalizedString(FuelEfficiencyLocalizationKeys(fuelEfficiencyUnits: unit).fuelEfficiencyUnitsKey)
    } else {
        return LocalizedString("AUTOMATIC_SETTING")
    }
}