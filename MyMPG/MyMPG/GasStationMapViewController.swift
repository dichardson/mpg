//
//  GasStationMapViewController.swift
//  MyMPG
//
//  Created by Douglas Richardson on 3/5/15.
//  Copyright (c) 2015 True Labs, Inc. All rights reserved.
//

import UIKit
import MapKit
import CoreLocation
import AddressBook

private let searchDistanceInMeters = CLLocationDistance(1000.0)

protocol GasStationMapViewControllerDelegate {
    func gasStationMapViewControllerDidSelectStation(controller : GasStationMapViewController, station : GasStationMapViewController.Station)
}

class GasStationMapViewController: UIViewController, MKMapViewDelegate {
    
    struct Station {
        let location : CLLocation
        let name : String
        let address : String
    }
    
    var delegate : GasStationMapViewControllerDelegate?
    
    var mapView : MKMapView?
    var shouldFindGasStationsOnMapRegionChange = false
    
    override func loadView() {
        mapView = MKMapView()
        self.view = mapView
    }

    override func viewDidLoad() {
        super.viewDidLoad()
        
        Analytics.sharedInstance.screenView("Fillup: Station Map")

        mapView?.delegate = self
        mapView?.mapType = .Standard
        mapView?.showsUserLocation = true
        mapView?.zoomEnabled = true
        
        AppDelegate.sharedInstance.gasStationLocationMonitor.currentLocation({ (location, alert) -> () in
            if let coord = location?.coordinate {
                let r = searchDistanceInMeters
                let region = MKCoordinateRegionMakeWithDistance(coord, r, r)
                self.mapView?.region = region
                self.findGasStationsInCurrentMapRegion()
                self.shouldFindGasStationsOnMapRegionChange = true
            } else if let alertVC = alert {
                self.presentViewController(alertVC, animated: true, completion: nil)
            }
        })
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    // MARK: MKMapViewDelegate
    func mapView(mapView: MKMapView!, viewForAnnotation annotation: MKAnnotation!) -> MKAnnotationView! {
        
        if let station = annotation as? GasStationAnnotation {
            let GasStationId = "GasStation"
            var pin = mapView.dequeueReusableAnnotationViewWithIdentifier(GasStationId) as? MKPinAnnotationView
            if pin == nil {
                pin = MKPinAnnotationView(annotation: nil, reuseIdentifier: GasStationId)
            }
            pin?.annotation = annotation
            pin?.animatesDrop = true
            pin?.canShowCallout = true
            pin?.leftCalloutAccessoryView = UIButton.buttonWithType(.ContactAdd) as! UIButton

            return pin
        }
        
        return nil
    }
    
    func mapView(mapView: MKMapView!, regionDidChangeAnimated animated: Bool) {
        if shouldFindGasStationsOnMapRegionChange {
            findGasStationsInCurrentMapRegion()
        }

    }
    
    // MARK: Misc
    
    func findGasStationsInCurrentMapRegion() {
        let req = MKLocalSearchRequest()
        req.naturalLanguageQuery = "gasoline"
        if let region = mapView?.region {
            req.region = region
        }
        let search = MKLocalSearch(request: req)
        search.startWithCompletionHandler { (response, error) -> Void in
            if response != nil {
                let annotations = response.mapItems.map { GasStationAnnotation(mapItem: ($0 as! MKMapItem)) }
                let currentAnnotations = NSSet(array: self.mapView?.annotations ?? [])
                let newAnnotations = annotations.filter{!currentAnnotations.containsObject($0)}
                self.mapView?.addAnnotations(newAnnotations)
            }
        }
    }
    
    func mapView(mapView: MKMapView!, annotationView view: MKAnnotationView!, calloutAccessoryControlTapped control: UIControl!) {

        if let annotation = view.annotation as? GasStationAnnotation{
            let loc = CLLocation(latitude: annotation.coordinate.latitude, longitude: annotation.coordinate.longitude)
            let mappedstation = GasStationMapViewController.Station(location: loc, name: annotation.title, address: annotation.subtitle)
            
            delegate?.gasStationMapViewControllerDidSelectStation(self, station: mappedstation)
            
            Analytics.sharedInstance.eventWithCategory(.UIAction, action: "select_gas_station", label: "map_view")
        }
    }
    
}

private class GasStationAnnotation : NSObject, MKAnnotation
{
    let mapItem : MKMapItem
    
    init(mapItem: MKMapItem) {
        self.mapItem = mapItem
    }
    
    // MARK: MKAnnotation
    @objc var coordinate: CLLocationCoordinate2D { get { return mapItem.placemark.coordinate } }
    @objc var title: String! { get { return mapItem.name } }
    
    @objc var subtitle: String! {
        get {
            return mapItem.placemark.addressDictionary[kABPersonAddressStreetKey] as? String
        }
    }
    
    // Provide isEqual and hash for NSSet
    @objc override func isEqual(object: AnyObject?) -> Bool {
        if let o = object as? GasStationAnnotation {
            return mapItem.isEqual(o.mapItem)
        } else {
            return false
        }
    }
    
    @objc override var hash: Int { get { return mapItem.hash } }
}
