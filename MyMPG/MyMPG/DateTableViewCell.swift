//
//  DateTableViewCell.swift
//  MyMPG
//
//  Created by Douglas Richardson on 2/13/15.
//  Copyright (c) 2015 True Labs, Inc. All rights reserved.
//

import UIKit

class DateTableViewCell: UITableViewCell {
    
    @IBOutlet var label : UILabel!
    @IBOutlet var dateLabel : UILabel!
    @IBInspectable var selectedColor : UIColor?
    
    let dateFormatter = NSDateFormatter()
    
    var date : NSDate = NSDate() {
        didSet {
            dateLabel?.text = dateFormatter.stringFromDate(date)
        }
    }

    override func awakeFromNib() {
        super.awakeFromNib()
        dateFormatter.dateStyle = .MediumStyle
        dateFormatter.timeStyle = .ShortStyle
    }

    override func setSelected(selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
}
