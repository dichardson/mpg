//
//  DistanceUnitSelectionControllerDelegate.swift
//  MyMPG
//
//  Created by Douglas Richardson on 3/11/15.
//  Copyright (c) 2015 True Labs, Inc. All rights reserved.
//

import UIKit

protocol DistanceUnitSelectionControllerDelegate {
    func distanceUnitSelectionController(controller : DistanceUnitSelectionController, didSelectUnit unit: DistanceUnit)
}

class DistanceUnitSelectionController: UITableViewController {
    private let rows : [DistanceUnit] = [.kilometer, .mile]
    var selectedUnit = DistanceUnit.kilometer
    var distanceUnitDelegate : DistanceUnitSelectionControllerDelegate?

    override func viewDidLoad() {
        super.viewDidLoad()
        Analytics.sharedInstance.screenView("Odometer Unit Selection")
    }
    
    override func numberOfSectionsInTableView(tableView: UITableView) -> Int {
        return 1
    }

    override func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return rows.count
    }

    override func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        let reuseId = "cell"
        var cell = (tableView.dequeueReusableCellWithIdentifier(reuseId) as? UITableViewCell)
            ?? UITableViewCell(style: .Default, reuseIdentifier: reuseId)
        
        let unit = rows[indexPath.row]
        let locKeys = DistanceUnitLocalizationKeys(distanceUnit: unit)
        cell.textLabel?.text = LocalizedString(locKeys.distanceUnitKey)
        cell.accessoryType = selectedUnit == unit ? .Checkmark : .None
        
        return cell
    }
    
    override func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath) {
        
        let oldIndexPath = NSIndexPath(forRow: rows.indexOfFirstObjectPassingTest({self.selectedUnit == $0})!, inSection: 0)
        
        selectedUnit = rows[indexPath.row]
        
        tableView.cellForRowAtIndexPath(oldIndexPath)?.accessoryType = .None
        tableView.cellForRowAtIndexPath(indexPath)?.accessoryType = .Checkmark
        
        tableView.deselectRowAtIndexPath(indexPath, animated: true)
        
        distanceUnitDelegate?.distanceUnitSelectionController(self, didSelectUnit: selectedUnit)
    }
    
    override func viewWillDisappear(animated: Bool) {
        var action = (NSLocale.currentLocale().objectForKey(NSLocaleCountryCode) as? String)?.uppercaseString ?? "N/A"
        var label = selectedUnit.rawValue
        Analytics.sharedInstance.eventWithCategory(.SelectOdometerUnit, action: action, label: label)
        
        super.viewWillDisappear(animated)
    }
}
