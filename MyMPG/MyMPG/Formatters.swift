//
//  Formatters.swift
//  MyMPG
//
//  Created by Douglas Richardson on 4/9/15.
//  Copyright (c) 2015 True Labs, Inc. All rights reserved.
//

import Foundation

// Standardized number formatter used for displaying fuel efficiencies
// in MPG, km/L, and L/100km.
func makeFuelEfficiencyNumberFormatter(minDigits: Int) -> NSNumberFormatter {
    let f = NSNumberFormatter()
    f.numberStyle = .DecimalStyle
    f.maximumFractionDigits = 1
    f.minimumFractionDigits = minDigits
    f.roundingMode = .RoundHalfUp
    return f
}

