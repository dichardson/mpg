//
//  Units.swift
//  MyMPG
//
//  Created by Douglas Richardson on 2/12/15.
//  Copyright (c) 2015 True Labs, Inc. All rights reserved.
//

import Foundation

// ******************************************************************************
// Enumerations used to give units to raw numbers. Each enumeration value
// has a raw value (e.g., kilometer = "km") that is intended to be used as
// a serialized unit identifier (i.e., stored in a core data database). As such,
// the raw values must not be changed.
// ******************************************************************************

public enum DistanceUnit : String {
    case kilometer = "km"
    case mile = "mile"
}

public struct Distance {
    public let value : Double
    public let unit : DistanceUnit
    
    public init(value : Double, unit : DistanceUnit) {
        self.value = value
        self.unit = unit
    }
    
    public func changeUnit(unit : DistanceUnit) -> Distance {
        switch self.unit {
        case .mile:
            switch unit {
            case .mile:
                return self
            case .kilometer:
                let v = kmPerMile * value
                return Distance(value: v, unit: unit)
            }
        case .kilometer:
            switch unit {
            case .mile:
                let v = milePerKm * value
                return Distance(value: v, unit: unit)
            case .kilometer:
                return self
            }
        }
    }
}

public struct ValuePerDistanceUnit {
    public let value : Double
    public let perUnit : DistanceUnit
    
    public init(value : Double, perUnit : DistanceUnit) {
        self.value = value
        self.perUnit = perUnit
    }
    
    public func changeUnit(unit : DistanceUnit) -> ValuePerDistanceUnit {
        let factor = Distance(value: 1.0, unit: perUnit).changeUnit(unit).value
        let newValue = value / factor
        return ValuePerDistanceUnit(value: newValue, perUnit: unit)
    }
}

public enum VolumeUnit : String {
    case liter = "L"
    case usGallon = "usgal"
    case imperialGallon = "impgal"
}

/*
-----------------------
VOLUME CONVERSION NOTES
-----------------------
1 US liquid gallon is defined as 231 in^3.

1 liter is defined to be 1000 cm^3.

1 inch is precisely 2.54cm.

1 gallon = 231 in^3 = 231 * 2.54^3 cm^3 = 3785.411784 cm^3 = 3.785411784 Liters (precisely).
To compute, use bc from command line (note I multiply by 0.001 instead of dividing by 1000 so
bc will limit the decimal places):

$ echo "scale=100; 231*2.54^3*0.001"|bc
3.785411784

1 liter = 1000 cm^3 = 1000/(2.54^3) in^3 = 1000/16.387064 in^3 = 61.02374409473228 in^3 / 231 =
0.26417205235815... gallons, which is not a terminating sequence. To compute from bc:

$ echo "scale=100; 1000/2.54^3/231"|bc
.2641720523581484153798999216091625079592661827038894218225427281546\
181185555267453037547790335721108
*/

public struct Volume {
    public let value : Double
    public let unit : VolumeUnit
    
    public init(value : Double, unit : VolumeUnit) {
        self.value = value
        self.unit = unit
    }
    
    public func changeUnit(newUnit : VolumeUnit) -> Volume {
        switch self.unit {
        case .liter:
            switch newUnit {
            case .liter:
                return self
            case .usGallon:
                return Volume(value: value * usGallonsPerLiter, unit: .usGallon)
            case .imperialGallon:
                return Volume(value: value * imperialGallonPerLiter, unit: .imperialGallon)
            }
        case .usGallon:
            switch newUnit {
            case .liter:
                return Volume(value: value * litersPerUSGallon, unit: .liter)
            case .usGallon:
                return self
            case .imperialGallon:
                let impGallons = value * litersPerUSGallon * imperialGallonPerLiter
                return Volume(value: impGallons, unit: .imperialGallon)
            }
        case .imperialGallon:
            switch newUnit {
            case .liter:
                return Volume(value: value * litersPerImperialGallon, unit: .liter)
            case .usGallon:
                let usGallons = value * litersPerImperialGallon * usGallonsPerLiter
                return Volume(value: usGallons, unit: .usGallon)
            case .imperialGallon:
                return self
            }
        }
    }
}

public struct ValuePerVolumeUnit {
    public let value : Double
    public let perUnit : VolumeUnit
    
    public init(value : Double, perUnit : VolumeUnit) {
        self.value = value
        self.perUnit = perUnit
    }
    
    public func changeUnit(unit : VolumeUnit) -> ValuePerVolumeUnit {
        let factor = Volume(value: 1.0, unit: perUnit).changeUnit(unit).value
        let newValue = value / factor
        return ValuePerVolumeUnit(value: newValue, perUnit: unit)
    }
}

private let kmPerMile = 1.609344 // precisely
private let milePerKm = 1.0 / kmPerMile // repeating, not represented precisely
private let litersPerUSGallon = 3.785411784 // precise from 231*2.54^3/1000
private let usGallonsPerLiter = 1.0 / litersPerUSGallon
private let litersPerImperialGallon = 4.54609 // precise
private let imperialGallonPerLiter = 1.0 / litersPerImperialGallon
private let mpgToKmL = kmPerMile / litersPerUSGallon
private let kmLToMPG = milePerKm / usGallonsPerLiter
private let imperialMPGToKmL = kmPerMile / litersPerImperialGallon
private let kmLToImperialMPG = milePerKm / imperialGallonPerLiter

private func kmlToL100Km(value : Double) -> Double? {
    if value.isZero {
        return nil
    }
    let r = (1.0 / value) * 100.0
    return r.isNaN ? nil : r
}

private func l100KmToKmL(value : Double) -> Double? {
    if value.isZero {
        return nil
    }
    let r = (1.0 / value) * 100.0
    return r.isNaN ? nil : r
}

public enum FuelEfficiencyUnits : String {
    case mpg = "mpg"
    case kmL = "kml"
    case l100km = "l100km"
    case mpgImperial = "mpg_imperial"
}

public struct FuelEfficiency {
    public let value : Double
    public let unit : FuelEfficiencyUnits
    
    public init(value : Double, unit : FuelEfficiencyUnits) {
        self.value = value
        self.unit = unit
    }
    
    public func changeUnit(newUnit : FuelEfficiencyUnits) -> FuelEfficiency? {
        switch unit {
        case .mpg:
            switch newUnit {
            case .mpg:
                return self
            case .kmL:
                return FuelEfficiency(value: value * mpgToKmL, unit: .kmL)
            case .l100km:
                if let v = kmlToL100Km(value * mpgToKmL) {
                    return FuelEfficiency(value: v, unit: .l100km)
                } else {
                    return nil
                }
            case .mpgImperial:
                let v = value * mpgToKmL * kmLToImperialMPG
                return FuelEfficiency(value: v, unit: .mpgImperial)
            }
        case .kmL:
            switch newUnit {
            case .mpg:
                return FuelEfficiency(value: value * kmLToMPG, unit: .mpg)
            case .kmL:
                return self
            case .l100km:
                if let v = kmlToL100Km(value) {
                    return FuelEfficiency(value: v, unit: .l100km)
                } else {
                    return nil
                }
            case .mpgImperial:
                let v = value * kmLToImperialMPG
                return FuelEfficiency(value: v, unit: .mpgImperial)
            }
        case .l100km:
            switch newUnit {
            case .mpg:
                if let v = l100KmToKmL(value) {
                    return FuelEfficiency(value: v * kmLToMPG, unit: .mpg)
                } else {
                    return nil
                }
            case .kmL:
                if let v = l100KmToKmL(value) {
                    return FuelEfficiency(value: v, unit: .kmL)
                } else {
                    return nil
                }
            case .l100km:
                return self
            case .mpgImperial:
                if let v = l100KmToKmL(value) {
                    return FuelEfficiency(value: v * kmLToImperialMPG, unit: .mpgImperial)
                } else {
                    return nil
                }
            }
        case .mpgImperial:
            switch newUnit {
            case .mpg:
                let v = value * imperialMPGToKmL * kmLToMPG
                return FuelEfficiency(value: v, unit: .mpgImperial)
            case .kmL:
                let v = value * imperialMPGToKmL
                return FuelEfficiency(value: v, unit: .mpgImperial)
            case .l100km:
                if let v = kmlToL100Km(value * imperialMPGToKmL) {
                    return FuelEfficiency(value: v, unit: .mpgImperial)
                } else {
                    return nil
                }
            case .mpgImperial:
                return self
            }
        }
    }
}
