//
//  StatsTests.swift
//  MyMPG
//
//  Created by Douglas Richardson on 3/27/15.
//  Copyright (c) 2015 True Labs, Inc. All rights reserved.
//

import UIKit
import XCTest
import MyMPG

class StatsTests: XCTestCase {
    
    func testStatsWithNoFuelEconomies() {
        let stats = statsFromEconomies([])
        XCTAssertTrue(stats.bestFuelEfficiency == nil)
        XCTAssertTrue(stats.averageFuelEfficiency == nil)
        XCTAssertTrue(stats.costPerDistance == nil)
        XCTAssertTrue(stats.costPerFillup == nil)
        XCTAssertTrue(stats.costPerVolume == nil)
        XCTAssertTrue(stats.currencyCode == nil)
    }

    func testStatsWithOneFuelEconomy() {
        let fc = FuelEconomy(kilometers: 100, liters: 10, date: NSDate(), beginOdometer: NSDecimalNumber(string: "0"), endOdometer: NSDecimalNumber(string: "100"), cost: NSDecimalNumber(string: "20.0"), currencyCode: "USD", fillupCount: 1)

        let stats = statsFromEconomies([fc])
        
        XCTAssertEqual(stats.bestFuelEfficiency!.changeUnit(.kmL)!.value, 10)
        XCTAssertEqual(stats.averageFuelEfficiency!.changeUnit(.kmL)!.value, 10)
        XCTAssertEqual(stats.costPerDistance!.changeUnit(.kilometer).value, 0.2)
        XCTAssertEqual(stats.costPerFillup!.doubleValue, 20.0)
        XCTAssertEqual(stats.costPerVolume!.changeUnit(.liter).value, 2.0)
        XCTAssertEqual(stats.currencyCode!, "USD")
    }
    
    func testStatsWithTwoFuelEconomiesBestFirst() {
        let fc1 = FuelEconomy(kilometers: 50, liters: 4, date: NSDate(), beginOdometer: NSDecimalNumber(string: "0"), endOdometer: NSDecimalNumber(string: "50"), cost: NSDecimalNumber(string: "10.0"), currencyCode: "USD", fillupCount: 1)
        let fc2 = FuelEconomy(kilometers: 50, liters: 6, date: NSDate(), beginOdometer: NSDecimalNumber(string: "50"), endOdometer: NSDecimalNumber(string: "100"), cost: NSDecimalNumber(string: "10.0"), currencyCode: "USD", fillupCount: 1)
        
        let stats = statsFromEconomies([fc1, fc2])
        
        XCTAssertEqual(stats.bestFuelEfficiency!.changeUnit(.kmL)!.value, 12.5)
        XCTAssertEqual(stats.averageFuelEfficiency!.changeUnit(.kmL)!.value, 10)
        XCTAssertEqual(stats.costPerDistance!.changeUnit(.kilometer).value, 0.2)
        XCTAssertEqual(stats.costPerFillup!.doubleValue, 10.0)
        XCTAssertEqual(stats.costPerVolume!.changeUnit(.liter).value, 2.0)
        XCTAssertEqual(stats.currencyCode!, "USD")
    }
    
    func testStatsWithTwoFuelEconomiesBestLast() {
        let fc1 = FuelEconomy(kilometers: 50, liters: 6, date: NSDate(), beginOdometer: NSDecimalNumber(string: "0"), endOdometer: NSDecimalNumber(string: "50"), cost: NSDecimalNumber(string: "10.0"), currencyCode: "USD", fillupCount: 1)
        let fc2 = FuelEconomy(kilometers: 50, liters: 4, date: NSDate(), beginOdometer: NSDecimalNumber(string: "50"), endOdometer: NSDecimalNumber(string: "100"), cost: NSDecimalNumber(string: "10.0"), currencyCode: "USD", fillupCount: 1)
        
        let stats = statsFromEconomies([fc1, fc2])
        
        XCTAssertEqual(stats.bestFuelEfficiency!.changeUnit(.kmL)!.value, 12.5)
        XCTAssertEqual(stats.averageFuelEfficiency!.changeUnit(.kmL)!.value, 10)
        XCTAssertEqual(stats.costPerDistance!.changeUnit(.kilometer).value, 0.2)
        XCTAssertEqual(stats.costPerFillup!.doubleValue, 10.0)
        XCTAssertEqual(stats.costPerVolume!.changeUnit(.liter).value, 2.0)
        XCTAssertEqual(stats.currencyCode!, "USD")
    }
    
    func testStatsWithFuelEconomyWith2Fillups() {
        let fc = FuelEconomy(kilometers: 100, liters: 10, date: NSDate(), beginOdometer: NSDecimalNumber(string: "0"), endOdometer: NSDecimalNumber(string: "100"), cost: NSDecimalNumber(string: "20.0"), currencyCode: "USD", fillupCount: 2)
        
        let stats = statsFromEconomies([fc])
        
        XCTAssertEqual(stats.bestFuelEfficiency!.changeUnit(.kmL)!.value, 10)
        XCTAssertEqual(stats.averageFuelEfficiency!.changeUnit(.kmL)!.value, 10)
        XCTAssertEqual(stats.costPerDistance!.changeUnit(.kilometer).value, 0.2)
        XCTAssertEqual(stats.costPerFillup!.doubleValue, 10.0)
        XCTAssertEqual(stats.costPerVolume!.changeUnit(.liter).value, 2.0)
        XCTAssertEqual(stats.currencyCode!, "USD")
    }

    func testStatsWithMultipleCurrencies() {
        let fc1 = FuelEconomy(kilometers: 50, liters: 6, date: NSDate(), beginOdometer: NSDecimalNumber(string: "0"), endOdometer: NSDecimalNumber(string: "50"), cost: NSDecimalNumber(string: "10.0"), currencyCode: "USD", fillupCount: 1)
        let fc2 = FuelEconomy(kilometers: 50, liters: 8, date: NSDate(), beginOdometer: NSDecimalNumber(string: "50"), endOdometer: NSDecimalNumber(string: "100"), cost: NSDecimalNumber(string: "10.0"), currencyCode: "MXN", fillupCount: 1)
        
        let stats = statsFromEconomies([fc2, fc1])
        
        // The currency code associated with the greatest volume of fuel should win.
        XCTAssertEqual(stats.currencyCode!, "MXN")
        
        let fc3 = FuelEconomy(kilometers: 50, liters: 3, date: NSDate(), beginOdometer: NSDecimalNumber(string: "50"), endOdometer: NSDecimalNumber(string: "100"), cost: NSDecimalNumber(string: "10.0"), currencyCode: "USD", fillupCount: 1)
        let stats2 = statsFromEconomies([fc3, fc2, fc1])
        XCTAssertEqual(stats2.currencyCode!, "USD")
        
    }
}
