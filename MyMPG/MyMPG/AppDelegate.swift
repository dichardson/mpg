//
//  AppDelegate.swift
//  MyMPG
//
//  Created by Douglas Richardson on 2/6/15.
//  Copyright (c) 2015 True Labs, Inc. All rights reserved.
//

import UIKit
import CoreData

let ApplicationDidRegisterUserNotificationSettingsNotification = "ApplicationDidRegisterUserNotificationSettings"
let UIUserNotificationSettingsKey = "UIUserNotificationSettings"

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate, GarageViewControllerDelegate, UINavigationControllerDelegate {

    var window: UIWindow?
    var gasStationLocationMonitor : GasStationLocationMonitor!
  
    class var sharedInstance : AppDelegate {
        get {
            return UIApplication.sharedApplication().delegate as! AppDelegate
        }
    }

    // MARK: UIApplicationDelegate
    func application(application: UIApplication, didFinishLaunchingWithOptions launchOptions: [NSObject: AnyObject]?) -> Bool {
        // Override point for customization after application launch.
        
        #if DEBUG
            let trackingID = "UA-60108717-1"
            #else
            let trackingID = "UA-60108717-3"
            #endif
        
        //Google Analytics settings
        GAI.sharedInstance().trackUncaughtExceptions = true
        #if DEBUG
        GAI.sharedInstance().dispatchInterval = 20
        #else
        GAI.sharedInstance().dispatchInterval = 120
        #endif
        GAI.sharedInstance().logger.logLevel = GAILogLevel.Error
        GAI.sharedInstance().trackerWithTrackingId(trackingID)
        
        
        if launchOptions?[UIApplicationLaunchOptionsLocationKey] != nil {
            // App launched because of a location event. Need to startup the CLManager delegate
            // to obtain the location information as it does not appear in the launch options.
            // This will happen later in this method and the UILocaleNotification will be
            // created in the GasStationLocationMonitor.
            NSLog("Started because of location event")
        }
        
        if launchOptions?[UIApplicationLaunchOptionsLocalNotificationKey] != nil {
            NSLog("Started because of local notification")
        }
        
        // Startup gas station region montoring which send UILocalNotifications.
        self.gasStationLocationMonitor = GasStationLocationMonitor()
        
        if let nav = window?.rootViewController as? UINavigationController {
            nav.delegate = self
            if let garage = nav.viewControllers[0] as? GarageViewController {
                garage.delegate = self
            }
        }
        
        return true
    }

    func applicationWillResignActive(application: UIApplication) {
        // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
        // Use this method to pause ongoing tasks, disable timers, and throttle down OpenGL ES frame rates. Games should use this method to pause the game.
    }

    func applicationDidEnterBackground(application: UIApplication) {
        // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later.
        // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
    }

    func applicationWillEnterForeground(application: UIApplication) {
        // Called as part of the transition from the background to the inactive state; here you can undo many of the changes made on entering the background.
    }

    func applicationDidBecomeActive(application: UIApplication) {
        // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
    }

    func applicationWillTerminate(application: UIApplication) {
        // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
    }
    
    func application(application: UIApplication, didRegisterUserNotificationSettings notificationSettings: UIUserNotificationSettings) {
        NSNotificationCenter.defaultCenter().postNotificationName(
            ApplicationDidRegisterUserNotificationSettingsNotification,
            object: self,
            userInfo: [UIUserNotificationSettingsKey : notificationSettings])
    }
    
    func application(application: UIApplication, didReceiveLocalNotification notification: UILocalNotification) {
        if notification.userInfo?[LocalNotificationEventKey] as? String == GasStationDetectedEvent {
            
            Analytics.sharedInstance.eventWithCategory(.UIAction, action: "open", label: "station_notification")
            // Show fill up screen.
            
            // If the user is one of the known "safe" screen (summary and garage view) then go ahead an show
            // the add fuel screen. Otherwise do nothing, since the user might be in the middle of modifications.
            
            if let nav = self.window?.rootViewController as? UINavigationController {
                if let top = nav.topViewController {
                    
                    var station : Station?
                    if let stationURL = NSURL(string:notification.userInfo?[GasStationDetectedRegionIDKey] as! String) {
                        if let moc = managedObjectContext {
                            if let id = moc.persistentStoreCoordinator?.managedObjectIDForURIRepresentation(stationURL) {
                                station = moc.objectWithID(id) as? Station
                            }
                        }
                    }
                    
                    if let garage = top as? GarageViewController {
                        // Just select the first available car, and then push
                        // the garage and summary view.
                        if let firstCar = garage.cars.first {
                            let summary = SummaryViewController.newControllerWithCar(firstCar)
                            let addFuelVC = AddFuelTableViewController.newTableControllerWithCar(firstCar)
                            addFuelVC.delegate = summary
                            addFuelVC.initialSelectedGasStation = station
                            nav.setViewControllers([garage, summary, addFuelVC], animated: true)
                        } else {
                            NSLog("Garage doesn't have a single car.")
                        }
                    } else if let summary = top as? SummaryViewController {
                        let addFuelVC = AddFuelTableViewController.newTableControllerWithCar(summary.car)
                        addFuelVC.initialSelectedGasStation = station
                        addFuelVC.delegate = summary
                        nav.pushViewController(addFuelVC, animated: true)
                    } else {
                        NSLog("Can't put up Add Fuel because top isn't garage or summary")
                    }
                }
            }
        }
    }
    
    // MARK: - Application Settings
    
    // Requests user notifications settings for the app. Response returned in
    // ApplicationDidRegisterUserNotificationSettingsNotification.
    func requestDesiredUserNotificationSettings() {
        
        // GasStationLocationMonitor needs .Alert to show Alert notifications when
        // the user is at a gas station and it needs .Sound to notify with a sound.
        
        let settings = UIUserNotificationSettings(forTypes: .Alert | .Sound, categories: nil)
        UIApplication.sharedApplication().registerUserNotificationSettings(settings)
    }

    
    // MARK: - Core Data stack
    
    lazy var applicationDocumentsDirectory: NSURL = {
        // The directory the application uses to store the Core Data store file. This code uses a directory named "com.pev4me.MyMPG" in the application's documents Application Support directory.
        let urls = NSFileManager.defaultManager().URLsForDirectory(.DocumentDirectory, inDomains: .UserDomainMask)
        return urls[urls.count-1] as! NSURL
        }()
    
    lazy var managedObjectModel: NSManagedObjectModel = {
        // The managed object model for the application. This property is not optional. It is a fatal error for the application not to be able to find and load its model.
        let modelURL = NSBundle.mainBundle().URLForResource("MPG", withExtension: "momd")!
        return NSManagedObjectModel(contentsOfURL: modelURL)!
        }()
    
    lazy var persistentStoreCoordinator: NSPersistentStoreCoordinator? = {
        // The persistent store coordinator for the application. This implementation creates and return a coordinator, having added the store for the application to it. This property is optional since there are legitimate error conditions that could cause the creation of the store to fail.
        // Create the coordinator and store
        var coordinator: NSPersistentStoreCoordinator? = NSPersistentStoreCoordinator(managedObjectModel: self.managedObjectModel)
        let url = self.applicationDocumentsDirectory.URLByAppendingPathComponent("MyMPG.sqlite")
        var error: NSError? = nil
        var failureReason = "There was an error creating or loading the application's saved data."
        let options = [NSMigratePersistentStoresAutomaticallyOption : true,
            NSInferMappingModelAutomaticallyOption: true]
        if coordinator!.addPersistentStoreWithType(NSSQLiteStoreType, configuration: nil, URL: url, options: options, error: &error) == nil {
            coordinator = nil
            // Report any error we got.
            var dict = [String : AnyObject]()
            dict[NSLocalizedDescriptionKey] = "Failed to initialize the application's saved data"
            dict[NSLocalizedFailureReasonErrorKey] = failureReason
            dict[NSUnderlyingErrorKey] = error
            error = NSError(domain: "YOUR_ERROR_DOMAIN", code: 9999, userInfo: dict)
            // Replace this with code to handle the error appropriately.
            // abort() causes the application to generate a crash log and terminate. You should not use this function in a shipping application, although it may be useful during development.
            NSLog("Unresolved error \(error), \(error!.userInfo)")
            abort()
        }
        
        return coordinator
        }()
    
    lazy var managedObjectContext: NSManagedObjectContext? = {
        // Returns the managed object context for the application (which is already bound to the persistent store coordinator for the application.) This property is optional since there are legitimate error conditions that could cause the creation of the context to fail.
        let coordinator = self.persistentStoreCoordinator
        if coordinator == nil {
            return nil
        }
        var managedObjectContext = NSManagedObjectContext()
        managedObjectContext.persistentStoreCoordinator = coordinator
        return managedObjectContext
        }()
    
    // MARK: - Core Data Saving support
    
    func saveContext (sender : UIViewController) -> Bool {
        if let moc = self.managedObjectContext {
            var error: NSError? = nil
            if moc.hasChanges && !moc.save(&error) {
                NSLog("Error saving managed object context: \(error), \(error!.userInfo)")
                let alert = UIAlertController(title: LocalizedString("ERROR_SAVING"), message: error?.localizedDescription, preferredStyle: .Alert)
                let action = UIAlertAction(title: LocalizedString("OK_ALERT_BUTTON_TITLE"), style: .Default, handler: nil)
                alert.addAction(action)
                sender.presentViewController(alert, animated: true, completion: nil)
                return false
            }
            return true
        }
        return false
    }
    
    // each call to networkActivityStarted must be balanced by networkActivityStopped
    private var networkActivityStartedCount = 0
    func networkActivityStarted() {
        networkActivityStartedCount++
        UIApplication.sharedApplication().networkActivityIndicatorVisible = networkActivityStartedCount > 0
    }
    
    func networkActivitedStopped() {
        networkActivityStartedCount--
        if networkActivityStartedCount < 0 {
            println("Unbalanced network activity calls. \(networkActivityStartedCount)")
        }
        UIApplication.sharedApplication().networkActivityIndicatorVisible = networkActivityStartedCount > 0
    }
    
    // MARK: - Garage View Controller Delegate
    func garageViewControllerDidSelectCar(controller: GarageViewController, car: Car) {
        let summary = SummaryViewController.newControllerWithCar(car)
        controller.showViewController(summary)
    }
    
    // MARK: - UINavigationControllerDelegate
    func navigationController(navigationController: UINavigationController, willShowViewController viewController: UIViewController, animated: Bool) {
        // Allow view controllers to specify a toolbar if they like.
        navigationController.toolbarHidden = viewController.toolbarItems == nil
    }
    
    // MARK: Miscellaneous
    lazy var analyticsUUID: NSUUID = {
        let userDefaults = NSUserDefaults.standardUserDefaults()
        let analyticsUUIDKey = "AnalyticsUUID"
        if let uuidStr = userDefaults.stringForKey(analyticsUUIDKey) {
            if let uuid = NSUUID(UUIDString: uuidStr) {
                return uuid
            }
        }
        
        // Don't have a analytics UUID yet, generate one.
        let uuid = NSUUID()
        userDefaults.setObject(uuid.UUIDString, forKey: analyticsUUIDKey)
        return uuid
    }()
}
