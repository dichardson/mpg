//
//  LineChartTableViewCell.swift
//  MyMPG
//
//  Created by Douglas Richardson on 3/3/15.
//  Copyright (c) 2015 True Labs, Inc. All rights reserved.
//

import UIKit

class LineChartTableViewCell : SingleViewCell {
    var chart : JBLineChartView { get { return view as! JBLineChartView } }
    
    override init(style: UITableViewCellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        setSubview(JBLineChartView())
    }

    required init(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override func layoutSubviews() {
        super.layoutSubviews()
        chart.reloadData()
    }
}


class ChartTableViewCell : SingleViewCell {
    var chart : ChartView { get { return view as! ChartView } }
    
    override init(style: UITableViewCellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        setSubview(ChartView(frame: CGRectZero))
    }

    required init(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override func layoutSubviews() {
        super.layoutSubviews()
//        chart.chart.reloadData()
    }
}