//
//  SettingsVolumeUnitsTableViewController.swift
//  MyMPG
//
//  Created by Douglas Richardson on 3/31/15.
//  Copyright (c) 2015 True Labs, Inc. All rights reserved.
//

class SettingsVolumeUnitsTableViewController: UITableViewController {
    
    private let volumeUnits : [VolumeUnit] = [.liter, .usGallon, .imperialGallon]
    private let userPrefs = UserPreferences()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.navigationItem.title = LocalizedString("FUEL_VOLUME_UNITS_TITLE")
        Analytics.sharedInstance.screenView("Settings: Fuel Volume")
    }
    
    override func numberOfSectionsInTableView(tableView: UITableView) -> Int {
        return 1
    }
    
    override func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return volumeUnits.count
    }
    
    private let defaultCell = TableViewCellReuseIdAndType<Value1TableViewCell>(reuseId: "Default")
    
    override func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        let cell = defaultCell.dequeueFromTableView(tableView)
        
        if indexPath.row == 0 {
            cell.textLabel?.text = settingDisplayStringForVolumeUnit(nil)
            let (autoDetectedVolumeUnit, _, _) = NSLocale.currentLocale().autoDetectUnits()
            let locKeys = VolumeUnitLocalizationKeys(volumeUnit: autoDetectedVolumeUnit)
            cell.detailTextLabel?.text = LocalizedString(locKeys.volumeUnitKey)
            cell.accessoryType = userPrefs.fuelVolumeUnits == nil ? .Checkmark : .None
        } else {
            let volumeUnit = volumeUnits[indexPath.row - 1]
            cell.textLabel?.text = settingDisplayStringForVolumeUnit(volumeUnit)
            cell.detailTextLabel?.text = nil
            cell.accessoryType = userPrefs.fuelVolumeUnits == volumeUnit ? .Checkmark : .None
        }
        
        return cell
    }
    
    override func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath) {
        if indexPath.row == 0 {
            userPrefs.fuelVolumeUnits = nil
        } else {
            userPrefs.fuelVolumeUnits = volumeUnits[indexPath.row - 1]
        }
        tableView.reloadData()
    }
    
    override func viewWillDisappear(animated: Bool) {
        if userPrefs.fuelVolumeUnits != nil {
            var action = (NSLocale.currentLocale().objectForKey(NSLocaleCountryCode) as? String)?.uppercaseString ?? "N/A"
            var label = userPrefs.fuelVolumeUnits?.rawValue
            Analytics.sharedInstance.eventWithCategory(.SelectFuelVolumeUnit, action: action, label: label)
        }
    }

}

private func settingDisplayStringForVolumeUnit(volumeUnit : VolumeUnit?) -> String {
    if let unit = volumeUnit {
        return LocalizedString(VolumeUnitLocalizationKeys(volumeUnit: unit).volumeUnitKey)
    } else {
        return LocalizedString("AUTOMATIC_SETTING")
    }
}
