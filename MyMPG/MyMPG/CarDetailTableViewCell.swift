//
//  CarDetailTableViewCell.swift
//  MyMPG
//
//  Created by Douglas Richardson on 3/18/15.
//  Copyright (c) 2015 True Labs, Inc. All rights reserved.
//

import UIKit

class CarDetailTableViewCell: UITableViewCell, NibTableViewCell {

    @IBOutlet var label: UILabel!
    @IBOutlet var topDetailLabel: UILabel!
    @IBOutlet var bottomDetailLabel: UILabel!
    @IBOutlet var notSetLabel: UILabel!
    
    var epaVehicle : EPAVehicle? {
        didSet {
            var notSetHidden = true
            if let v = epaVehicle {
                let yearFormatter = EPAYearFormatter()
                notSetHidden = true
                let yearStr = yearFormatter.displayYear(v.year) ?? ""
                topDetailLabel.text = NSString(format: LocalizedString("CAR_YEAR_MAKE_MODEL_FORMAT"), yearStr, v.make, v.model) as String
                let separator = LocalizedString("CAR_OPTIONS_SEPARATOR")
                bottomDetailLabel.text = separator.join(v.displayOptionsWithNumberFormatter(NSNumberFormatter()))
            } else {
                notSetHidden = false
            }
            
            notSetLabel.hidden = notSetHidden
            topDetailLabel.hidden = !notSetHidden
            bottomDetailLabel.hidden = !notSetHidden
        }
    }
    
    class func nibName() -> String {
        return "CarDetailTableViewCell"
    }
}
