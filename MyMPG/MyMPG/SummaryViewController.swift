//
//  SummaryViewController.swift
//  MyMPG
//
//  Created by Douglas Richardson on 2/27/15.
//  Copyright (c) 2015 True Labs, Inc. All rights reserved.
//

import Foundation
import CoreData

class SummaryViewController : UITableViewController, JBLineChartViewDataSource, JBLineChartViewDelegate, AddFuelTableViewControllerDelegate {
    
    var car : Car!
    
    private enum Section {
        case Chart
        case Stats
        case Developer
        case Fillups
        case AddFillup
    }
    
    private var sections : [Section] = []
    
    private enum ChartLines {
        case MyFuelEconomy(ArraySlice<FuelEconomy>)
        case EPACityFuelEconomy(CGFloat, Int) // value, count
        case EPAHighwayFuelEconomy(CGFloat, Int) // value, count
        case EPACombinedFuelEconomy(CGFloat, Int) // value, count
    }
    
    private var chartLines : [ChartLines] = []
    
    private let ChartCellId = "Chart"
    private typealias ChartCell = ChartTableViewCell
    private let StatRowCellId = "StatRow"
    private typealias StatRowCell = StatRowTableViewCell
    private let FillupCellId = "Fillup"
    private typealias FillupCell = FillupRowTableViewCell
    private let DeveloperCellId = "Developer"
    private typealias DeveloperCell = LabelTableViewCell
    private let AddFillupCellId = "AddFillup"
    private typealias AddFillupCell = LabelTableViewCell
    
    private var dateFormatter : NSDateFormatter?
    private var numberFormatter : NSNumberFormatter?
    private var userRecordedValueNumberFormatter : NSNumberFormatter?
    private var fuelEfficiencyNumberFormatter : NSNumberFormatter?
    private var chartFuelEfficiencyNumberFormatter : NSNumberFormatter?
    private var fillups : [Fillup] = []
    private var economiesByEndOdometer = [NSDecimalNumber : FuelEconomy]()
    private var chartLineColor = UIColor.redColor()
    private let epaHighwayColor = UIColor.orangeColor()
    private let epaCityColor = UIColor.yellowColor()
    private let epaCombinedColor = UIColor.greenColor()
    private var displayStats : DisplayStats?
    private var localeUnits : LocaleUnits!
    
    private var minChartValue = Int(0)
    private var maxChartValue = Int(0)
    private var leftChartLabel = ""
    private var rightChartLabel = ""
    
    class func newControllerWithCar(car : Car) -> SummaryViewController {
        let sb = UIStoryboard(name: "Summary", bundle: nil)
        let vc = sb.instantiateInitialViewController() as! SummaryViewController
        vc.car = car
        return vc
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        Analytics.sharedInstance.screenView("Summary")
        
        // Prevent the scroll view from cancelling the interactive Chart features when the
        // user taps and holds on it.
        tableView.canCancelContentTouches = false
        
        tableView.registerClass(ChartCell.self, forCellReuseIdentifier: ChartCellId)
        tableView.registerNib(UINib(nibName: "FillupRow", bundle: nil), forCellReuseIdentifier: FillupCellId)
        tableView.registerClass(StatRowCell.self, forCellReuseIdentifier: StatRowCellId)
        tableView.registerClass(DeveloperCell.self, forCellReuseIdentifier: DeveloperCellId)
        tableView.registerClass(AddFillupCell.self, forCellReuseIdentifier: AddFillupCellId)
        
        // Setup developer rows
        developerRows = [
            LabelAction(label: "Add Test Fillups", action: { self.addTestFillups() }),
        ]
        
        addFillupRows = [
            LabelAction(label: LocalizedString("ADD_FUEL"), action: { self.addFillup("row") })
        ]
    }
    
    override func viewWillAppear(animated: Bool) {
        reload()
    }
    
    // MARK: UITableViewDataSource
    override func numberOfSectionsInTableView(tableView: UITableView) -> Int {
        return sections.count
    }
    
    override func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        switch sections[section] {
        case .Chart:
            return 1
        case .Stats:
            if let s = displayStats {
                return s.hasCostBasedStats ? 2 : 1
            } else {
                return 0
            }
        case .Fillups:
            return fillups.count
        case .Developer:
            return developerRows.count
        case .AddFillup:
            return addFillupRows.count
        }
    }
    
    private let ChartHeight = CGFloat(200)
    private let ChartHeaderHeight = CGFloat(40)
    private let ChartFooterHeight = CGFloat(40)
    private var ChartRowHeight : CGFloat { get { return ChartHeight + ChartFooterHeight + ChartHeaderHeight } }
    
    override func tableView(tableView: UITableView, heightForRowAtIndexPath indexPath: NSIndexPath) -> CGFloat {
        switch sections[indexPath.section] {
        case .Chart:
            return ChartRowHeight
        case .Fillups:
            return 60
        default:
            return StandardTableViewRowHeight
        }
    }
    
    override func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        switch sections[indexPath.section] {
        case .Chart:
            var epaCityMPGString = "-"
            var epaCombinedMPGString = "-"
            var epaHighwayMPGString = "-"
            if let vehicleId = car.epaVehicleId?.toInt() {
                if let vehicle = EPAVehicleDatabase().epaVehicleForId(vehicleId) {
                    let cityMPG = FuelEfficiency(value: Double(vehicle.cityMPG), unit: .mpg)
                    if let city = cityMPG.changeUnit(localeUnits.fuelEfficiencyUnits)?.value {
                        epaCityMPGString = chartFuelEfficiencyNumberFormatter?.stringFromNumber(city) ?? epaCityMPGString
                    }
                    let combinedMPG = FuelEfficiency(value: Double(vehicle.combinedMPG), unit: .mpg)
                    if let combined = combinedMPG.changeUnit(localeUnits.fuelEfficiencyUnits)?.value {
                        epaCombinedMPGString = chartFuelEfficiencyNumberFormatter?.stringFromNumber(combined) ?? epaCombinedMPGString
                    }
                    let highwayMPG = FuelEfficiency(value: Double(vehicle.highwayMPG), unit: .mpg)
                    if let highway = highwayMPG.changeUnit(localeUnits.fuelEfficiencyUnits)?.value {
                        epaHighwayMPGString = chartFuelEfficiencyNumberFormatter?.stringFromNumber(highway) ?? epaHighwayMPGString
                    }
                }
            }
            let c = tableView.dequeueReusableCellWithIdentifier(ChartCellId) as! ChartCell
            let chart = c.chart
            chartLineColor = tableView.backgroundColor ?? UIColor.whiteColor()
            chart.backgroundColor = chart.tintColor.colorByScalingSaturation(0.7)
            
            chart.title.centerLabel.text = localeUnits.fuelEfficiencyUnitsDisplayString
            
            chart.chart.showsLineSelection = false
            chart.chart.showsVerticalSelection = true
            chart.xAxis.leftLabel.text = leftChartLabel
            chart.xAxis.rightLabel.text = rightChartLabel
            chart.xAxis.topLegend.label.text = NSString(format: LocalizedString("HIGHWAY"), epaHighwayMPGString) as String
            chart.xAxis.bottomLegend.label.text = NSString(format: LocalizedString("CITY"), epaCityMPGString) as String
            chart.xAxis.midLegend.label.text = NSString(format: LocalizedString("COMBINED"), epaCombinedMPGString) as String
            let highwayIndex = chartLines.indexOfFirstObjectPassingTest({
                switch $0 {
                case .EPAHighwayFuelEconomy(_, _):
                    return true
                default:
                    return false
                }})
            let cityIndex = chartLines.indexOfFirstObjectPassingTest({
                switch $0 {
                case .EPACityFuelEconomy(_, _):
                    return true
                default:
                    return false
                }})
            let combinedIndex = chartLines.indexOfFirstObjectPassingTest({
                switch $0 {
                case .EPACombinedFuelEconomy(_, _):
                    return true
                default:
                    return false
                }})
            
            chart.xAxis.topLegend.hidden = highwayIndex == nil
            chart.xAxis.topLegend.dashView.dashColor = epaHighwayColor
            chart.xAxis.bottomLegend.hidden = cityIndex == nil
            chart.xAxis.bottomLegend.dashView.dashColor = epaCityColor
            chart.xAxis.midLegend.hidden = combinedIndex == nil
            chart.xAxis.midLegend.dashView.dashColor = epaCombinedColor
            
            chart.yAxis.topLabel.text = numberFormatter?.stringFromNumber(NSNumber(integer: maxChartValue))
            chart.chart.maximumValue = CGFloat(maxChartValue)
            chart.yAxis.bottomLabel.text = numberFormatter?.stringFromNumber(NSNumber(integer: minChartValue))
            chart.chart.minimumValue = CGFloat(minChartValue)
            
            // After modifying UILabel's text, may need to layout again.
            chart.xAxis.setNeedsLayout()
            chart.yAxis.setNeedsLayout()
            chart.title.setNeedsLayout()
            chart.setNeedsLayout()
            
            chart.chart.delegate = self
            chart.chart.dataSource = self
            chart.chart.reloadData()
            
            // set the inset so that the separator goes away completely.
            let inset = CGRectGetWidth(view.frame) / 2.0
            c.separatorInset = UIEdgeInsets(top: 0, left: inset, bottom: 0, right: inset)
            
            return c
            
        case .Stats:
            let c = tableView.dequeueReusableCellWithIdentifier(StatRowCellId) as! StatRowCell
            if indexPath.row == 0 {
                c.leftLabel.attributedText = displayStats?.lastFuelEfficiency?.attributedDisplayString
                c.middleLabel.attributedText = displayStats?.averageFuelEfficiency?.attributedDisplayString
                c.rightLabel.attributedText = displayStats?.bestFuelEfficiency?.attributedDisplayString
            } else if indexPath.row == 1 {
                c.leftLabel.attributedText = displayStats?.costPerDistance?.attributedDisplayString
                c.middleLabel.attributedText = displayStats?.costPerFillup?.attributedDisplayString
                c.rightLabel.attributedText = displayStats?.costPerVolume?.attributedDisplayString
            }
            // set the inset so that the separator goes away completely.
            let inset = CGRectGetWidth(tableView.frame) / 2.0
            c.separatorInset = UIEdgeInsets(top: 0, left: inset, bottom: 0, right: inset)
            return c
            
        case .Fillups:
            let fillup = fillups[indexPath.row]
            let dateString = dateFormatter?.stringFromDate(fillup.date) ?? ""
            
            let volumeUnit = VolumeUnit(rawValue: fillup.volumeUnit)!
            let locKeys = VolumeUnitLocalizationKeys(volumeUnit: volumeUnit)
            let volumeString = userRecordedValueNumberFormatter?.stringFromNumber(fillup.volume) ?? ""
            let volumeWithUnits = LocalizedFormatString("VOLUME_WITH_UNITS_FORMAT", volumeString, LocalizedString(locKeys.abbreviatedVolumeUnitKey))
            
            let odometerString = userRecordedValueNumberFormatter?.stringFromNumber(fillup.odometer) ?? ""
            let distanceLocKeys = DistanceUnitLocalizationKeys(distanceUnit: DistanceUnit(rawValue: car.odometerUnit)!)
            let distanceUnitsDisplayString = LocalizedString(distanceLocKeys.distanceUnitAbbreviationKey)
            let odometerWithUnits = LocalizedFormatString("DISTANCE_WITH_UNITS_FORMAT", odometerString, distanceUnitsDisplayString)
            //let labelString = NSString(format: LocalizedString("FILLUP_CELL_LABEL_FORMAT"), odometerWithUnits, volumeWithUnits)

            //Calculate fuel efficiency
            var fuelEfficiencyString = LocalizedString("FUEL_EFFICIENCY_NOT_AVAILABLE")
            if let fuelEfficiency = economiesByEndOdometer[fillup.odometer]?.fuelEfficiency?.changeUnit(localeUnits.fuelEfficiencyUnits)?.value {
                fuelEfficiencyString = fuelEfficiencyNumberFormatter?.stringFromNumber(fuelEfficiency) ?? fuelEfficiencyString
            }
            let fuelEfficiencyStringWithUnits = LocalizedFormatString("FUEL_EFFICIENCY_WITH_UNITS_FORMAT", fuelEfficiencyString, localeUnits.shortFuelEfficiencyUnitsDisplayString)
            
            
            let c = tableView.dequeueReusableCellWithIdentifier(FillupCellId) as! FillupCell
            c.fillupOdometer?.text = odometerWithUnits
            c.fillupVolume?.text = volumeWithUnits
            c.fillupDate?.text = dateString
            c.mpgTag?.text = fuelEfficiencyStringWithUnits
            c.detailTag?.hidden = true
            if fillup.isPartial.boolValue {
                c.detailTag?.hidden = false
                c.detailTag?.text = LocalizedString("PARTIAL_FILLUP_SHORT")
            }
            if fillup.missedLastFillup.boolValue {
                c.detailTag?.hidden = false
                c.detailTag?.text = LocalizedString("MISSED_FILLUP_SHORT")
            }
            if fillup.missedLastFillup.boolValue && fillup.isPartial.boolValue {
                c.detailTag?.hidden = false
                c.detailTag?.text = LocalizedString("PARTIAL_MISSED_FILLUP_SHORT")
            }
            c.accessoryType = .DisclosureIndicator
            return c
            
        case .Developer:
            let c = tableView.dequeueReusableCellWithIdentifier(DeveloperCellId, forIndexPath: indexPath) as! DeveloperCell
            c.label.text = developerRows[indexPath.row].label
            c.label.textAlignment = .Center
            return c
            
        case .AddFillup:
            let c = tableView.dequeueReusableCellWithIdentifier(AddFillupCellId, forIndexPath: indexPath) as! AddFillupCell
            c.label.text = addFillupRows[indexPath.row].label
            c.label.textAlignment = .Center
            c.label.textColor = c.tintColor
            return c
            
        }
    }
    
    override func tableView(tableView: UITableView, titleForHeaderInSection section: Int) -> String? {
        switch sections[section] {
        case .Chart:
            return nil
        case .Stats:
            return nil
        case .Fillups:
            return nil
        case .Developer:
            return "Developer" // don't need to localize
        case .AddFillup:
            return LocalizedString("FILL_UPS")
        }
    }
    
    override func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath) {
        switch sections[indexPath.section] {
        case .Chart:
            break
        case .Stats:
            break
        case .Fillups:
            let fillup = fillups[indexPath.row]
            let vc = AddFuelTableViewController.newTableControllerWithCar(car)
            vc.fillupToEdit = fillup
            vc.delegate = self
            self.showViewController(vc)
        case .Developer:
            developerRows[indexPath.row].action()
            tableView.deselectRowAtIndexPath(indexPath, animated: true)
        case .AddFillup:
            addFillupRows[indexPath.row].action()
            tableView.deselectRowAtIndexPath(indexPath, animated: true)
        }
    }
    
    override func tableView(tableView: UITableView, shouldHighlightRowAtIndexPath indexPath: NSIndexPath) -> Bool {
        // ***************************************** BEGIN WORKAROUND
        // Swift compiler is crashing in this function, but I found a workaround by adding
        // this unneeded code. Reported bug to Apple in radar 20403516 on April 2, 2015.
        // After that bug is fixed this workaround should be removed.
        if sections.count < 0 {
            return false
        }
        // ***************************************** END WORKAROUND
        switch sections[indexPath.section] {
        case .Chart:
            return false
        case .Stats:
            return false
        case .Fillups:
            return true
        case .Developer:
            return true
        case .AddFillup:
            return true
        }
    }

    // MARK: JBLineChartViewDelegate
    func lineChartView(lineChartView: JBLineChartView!, verticalValueForHorizontalIndex horizontalIndex: UInt, atLineIndex lineIndex: UInt) -> CGFloat {
        switch chartLines[Int(lineIndex)] {
        case .MyFuelEconomy(let fuelEconomies):
            let fc = fuelEconomies[Int(horizontalIndex)]
            let fe = fc.fuelEfficiency?.changeUnit(localeUnits.fuelEfficiencyUnits)
            return max(CGFloat(fe?.value ?? 0.0), 0)
        case .EPACityFuelEconomy(let value, _):
            return value
        case .EPAHighwayFuelEconomy(let value, _):
            return value
        case .EPACombinedFuelEconomy(let value, _):
            return value
        }
    }
    
    func lineChartView(lineChartView: JBLineChartView!, colorForLineAtLineIndex lineIndex: UInt) -> UIColor! {
        switch chartLines[Int(lineIndex)] {
        case .MyFuelEconomy(_):
            return chartLineColor
        case .EPAHighwayFuelEconomy(_, _):
            return epaHighwayColor
        case .EPACityFuelEconomy(_, _):
            return epaCityColor
        case .EPACombinedFuelEconomy(_, _):
            return epaCombinedColor
        }
    }
    
    func lineChartView(lineChartView: JBLineChartView!, dotRadiusForDotAtHorizontalIndex horizontalIndex: UInt, atLineIndex lineIndex: UInt) -> CGFloat {
        return 4
    }
    
    func lineChartView(lineChartView: JBLineChartView!, colorForDotAtHorizontalIndex horizontalIndex: UInt, atLineIndex lineIndex: UInt) -> UIColor! {
        return chartLineColor
    }
    
    func lineChartView(lineChartView: JBLineChartView!, lineStyleForLineAtLineIndex lineIndex: UInt) -> JBLineChartViewLineStyle {
        switch chartLines[Int(lineIndex)] {
        case .MyFuelEconomy(_):
            return .Solid
        default:
            return .Dashed
        }
    }
    
    func lineChartView(lineChartView: JBLineChartView!, widthForLineAtLineIndex lineIndex: UInt) -> CGFloat {
        return 1
    }
    
    func lineChartView(lineChartView: JBLineChartView!, didSelectLineAtIndex lineIndex: UInt, horizontalIndex: UInt, touchPoint: CGPoint) {
        var label = view.viewWithTag(12345) as? UILabel
        var isNew = false
        if label == nil {
            let l = UILabel()
            l.tag = 12345
            l.backgroundColor = UIColor.whiteColor()
            l.textColor = lineChartView.tintColor
            l.textAlignment = .Center
            l.adjustsFontSizeToFitWidth = true
            l.minimumScaleFactor = 0.5
            l.bounds = CGRectMake(0, 0, 40, 20)
            view.addSubview(l)
            label = l
            isNew = true
        }
        
        if let l = label {
            
            //let point = lineChartView.headerView.convertPoint(touchPoint, fromView: lineChartView)
            let point = lineChartView.convertPoint(touchPoint, toView: view)
            
            switch chartLines[Int(lineIndex)] {
            case .MyFuelEconomy(let fuelEconomies):
                if let fe = fuelEconomies[Int(horizontalIndex)].fuelEfficiency?.changeUnit(localeUnits.fuelEfficiencyUnits) {
                    l.text = fuelEfficiencyNumberFormatter?.stringFromNumber(fe.value)
                } else {
                    l.text = nil
                }
            case .EPACityFuelEconomy(let value, _):
                l.text = nil
            case .EPAHighwayFuelEconomy(let value, _):
                l.text = nil
            case .EPACombinedFuelEconomy(let value, _):
                l.text = nil
            }
            
            let adjustPosition = { () -> Void in
                l.center = CGPointMake(point.x, CGRectGetMidY(lineChartView.headerView.bounds))
            }
            
            adjustPosition()
        }
        
    }
    
    func didDeselectLineInLineChartView(lineChartView: JBLineChartView!) {
        if let label = view.viewWithTag(12345) as? UILabel {
            label.removeFromSuperview()
        }
    }
    
    // MARK: JBLineChartViewDataSource
    func numberOfLinesInLineChartView(lineChartView: JBLineChartView!) -> UInt {
        return UInt(chartLines.count)
    }
    
    func lineChartView(lineChartView: JBLineChartView!, numberOfVerticalValuesAtLineIndex lineIndex: UInt) -> UInt {
        switch chartLines[Int(lineIndex)] {
        case .MyFuelEconomy(let fuelEconomies):
            return UInt(fuelEconomies.count)
        case .EPACityFuelEconomy(_, let count):
            return UInt(count)
        case .EPAHighwayFuelEconomy(_, let count):
            return UInt(count)
        case .EPACombinedFuelEconomy(_, let count):
            return UInt(count)
        }
    }
    
    func lineChartView(lineChartView: JBLineChartView!, showsDotsForLineAtLineIndex lineIndex: UInt) -> Bool {
        switch chartLines[Int(lineIndex)] {
        case .MyFuelEconomy(_):
            return true
        default:
            return false
        }
    }
    
    func lineChartView(lineChartView: JBLineChartView!, smoothLineAtLineIndex lineIndex: UInt) -> Bool {
        return false
    }
    
    @IBAction func addFuelEntryPressed(sender: UIBarButtonItem) {

        addFillup("navigation")
    }
    
    // MARK: - Add Fuel Table View Controller Delegate
    func addFuelControllerDidAddFuel(controller: AddFuelTableViewController) {
        car = controller.car
        self.navigationController?.popToViewController(self, animated: true)
    }
    
    // MARK: Data Loading and Unloading
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        
        if !self.isViewLoaded() || self.view.window != nil {
            // All the values cleared here are set in reload()
            dateFormatter = nil
            numberFormatter = nil
            userRecordedValueNumberFormatter = nil
            fuelEfficiencyNumberFormatter = nil
            chartFuelEfficiencyNumberFormatter = nil
            localeUnits = nil
            fillups = []
            economiesByEndOdometer.removeAll(keepCapacity: false)
            chartLines = []
            displayStats = nil
        }
    }
    
    private func reload() {
        navigationItem.title = car.nickname
        
        // All the values set here can be cleared in didReceiveMemoryWarning
        if dateFormatter == nil {
            dateFormatter = NSDateFormatter()
            if let f = dateFormatter {
                f.dateStyle = .ShortStyle
                f.timeStyle = .NoStyle
            }
        }
        
        if numberFormatter == nil {
            numberFormatter = NSNumberFormatter()
            if let f = numberFormatter {
                f.numberStyle = .DecimalStyle
            }
        }
        
        if userRecordedValueNumberFormatter == nil {
            userRecordedValueNumberFormatter = UserRecordedValueNumberFormatter()
        }
        
        if fuelEfficiencyNumberFormatter == nil {
            fuelEfficiencyNumberFormatter = makeFuelEfficiencyNumberFormatter(1)
        }
        
        if chartFuelEfficiencyNumberFormatter == nil {
            chartFuelEfficiencyNumberFormatter = makeFuelEfficiencyNumberFormatter(0)
        }
        
        // Grab the LocaleUnits every reload. Doing this means we don't have to worry
        // about NSCurrentLocaleDidChangeNotification as reload is called every time
        // the view is displayed.
        localeUnits = LocaleUnits()
        
        let moc = AppDelegate.sharedInstance.managedObjectContext!
        let req = NSFetchRequest(entityName: "Fillup")
        req.sortDescriptors = [NSSortDescriptor(key: "odometer", ascending: false)]
        req.predicate = NSPredicate(format: "self.car == %@", argumentArray: [car])
        var error : NSError?
        if let fillupsNew = moc.executeFetchRequest(req, error: &error) as? [Fillup] {
            fillups = fillupsNew
        } else {
            NSLog("Error executing Fillup request. %@", error?.localizedDescription ?? "")
            fillups = []
        }
        
        // Get the fuel economy data points for the chart.
        chartLines = []
        let economies = fuelEconomiesFromFillups(fillups)
        let mostRecentFC = economies.first
        let maxFuelEconomyCountForChart = 100
        let fuelEconomiesForChart = economies[0..<min(economies.count, maxFuelEconomyCountForChart)].reverse()
        if fuelEconomiesForChart.count > 0 {
            chartLines.append(.MyFuelEconomy(fuelEconomiesForChart))
        }
        
        var maxFC = 0.0
        var minFC = Double.infinity
        for fc in fuelEconomiesForChart {
            if let fe = fc.fuelEfficiency?.changeUnit(localeUnits.fuelEfficiencyUnits) {
                maxFC = max(maxFC, fe.value)
                minFC = min(minFC, fe.value)
            }
        }
        
        if let vehicleId = car.epaVehicleId?.toInt() {
            if let vehicle = EPAVehicleDatabase().epaVehicleForId(vehicleId) {
                let count = max(2, fuelEconomiesForChart.count)
                
                let cityMPG = FuelEfficiency(value: Double(vehicle.cityMPG), unit: .mpg)
                if let city = cityMPG.changeUnit(localeUnits.fuelEfficiencyUnits)?.value {
                    chartLines.append(.EPACityFuelEconomy(CGFloat(city), count))
                    maxFC = max(maxFC, city)
                    minFC = min(minFC, city)
                }
                
                let highwayMPG = FuelEfficiency(value: Double(vehicle.highwayMPG), unit: .mpg)
                if let highway = highwayMPG.changeUnit(localeUnits.fuelEfficiencyUnits)?.value {
                    chartLines.append(.EPAHighwayFuelEconomy(CGFloat(highway), count))
                    maxFC = max(maxFC, highway)
                    minFC = min(minFC, highway)
                }
                
                let combinedMPG = FuelEfficiency(value: Double(vehicle.combinedMPG), unit: .mpg)
                if let combined = combinedMPG.changeUnit(localeUnits.fuelEfficiencyUnits)?.value {
                    chartLines.append(.EPACombinedFuelEconomy(CGFloat(combined), count))
                    maxFC = max(maxFC, combined)
                    minFC = min(minFC, combined)
                }
            }
        }
        
        // Calculate stats
        let stats = statsFromEconomies(economies)
        displayStats = DisplayStats(stats: stats, mostRecent: mostRecentFC, localeUnits: localeUnits)
        
        println("Fetched \(fillups.count) fillups")
        println("Calculated stats for \(economies.count) fuel economies")
        
        let labelMaker = { (date : NSDate, odometer : NSDecimalNumber, du : DistanceUnit) -> String in
            let dateStr = self.dateFormatter?.stringFromDate(date) ?? ""
            let locKeys = DistanceUnitLocalizationKeys(distanceUnit: du)
            let odometerStr = self.userRecordedValueNumberFormatter?.stringFromNumber(odometer) ?? ""
            let odometerWithUnits = NSString(format: LocalizedString("DISTANCE_WITH_UNITS_FORMAT"), odometerStr, LocalizedString(locKeys.distanceUnitAbbreviationKey))
            return NSString(format: LocalizedString("DATE_ODOMETER_CHART_LABEL_FORMAT"), dateStr, odometerWithUnits) as String
        }
        
        leftChartLabel = ""
        rightChartLabel = ""
        let odometerUnit = DistanceUnit(rawValue: car.odometerUnit) ?? DistanceUnit.kilometer
        if let first = fuelEconomiesForChart.first {
            leftChartLabel = labelMaker(first.date, first.beginOdometer, odometerUnit)
        }
        if let last = fuelEconomiesForChart.last {
            rightChartLabel = labelMaker(last.date, last.endOdometer, odometerUnit)
        }

        // Just to make sure there is some range in the chart (so the Y doesn't go from, say, 1 to 1).
        let minimumChartInterval = 5
        if !minFC.isInfinite && !maxFC.isZero {
            minChartValue = Int(floor(minFC))
            maxChartValue = max(minChartValue+minimumChartInterval, Int(ceil(maxFC)))
        } else {
            minChartValue = 0
            maxChartValue = 10
        }
        
        // Build an index from end odometer to fuel economy for use by the table view MPG rows.
        economiesByEndOdometer.removeAll(keepCapacity: true)
        for e in economies {
            economiesByEndOdometer[e.endOdometer] = e
        }
        
        // Set table view sections
        sections = []
        if chartLines.count > 0 {
            sections.append(.Chart)
        }
        if economies.count > 0 {
            sections.append(.Stats)
        }
        if UserPreferences().developerModeEnabled {
            sections.append(.Developer)
        }
        sections.append(.AddFillup)
        sections.append(.Fillups)
        
        
        // Maintainer note: reloadData should be called after everything else is setup.
        tableView.reloadData()
    }
    
    // Mark: Developer Tools
    private struct LabelAction {
        let label : String
        let action : () -> ()
    }
    
    private func addFillup(analyticsLabel: String) {
        
        Analytics.sharedInstance.eventWithCategory(.UIAction, action: "press_add_fillup_button", label: analyticsLabel)
        
        let vc = AddFuelTableViewController.newTableControllerWithCar(car)
        vc.delegate = self
        vc.shouldSetInitialFirstResponder = true
        self.showViewController(vc)
    }
    
    private func addTestFillups() {
        let moc = AppDelegate.sharedInstance.managedObjectContext!
        var odometer = 1
        let date = NSDate()
        
        let req = NSFetchRequest(entityName: "Fillup")
        req.predicate = NSPredicate(format: "self.car==%@", argumentArray: [car])
        req.sortDescriptors = [NSSortDescriptor(key: "odometer", ascending: false)]
        req.fetchLimit = 1
        var error : NSError?
        if let maxFillup = (moc.executeFetchRequest(req, error: &error))?.first as? Fillup {
            odometer = maxFillup.odometer.integerValue
        } else {
            println("Error quering max odometer: \(error)")
        }
        
        for i in 1...100 {
            let fillup = NSEntityDescription.insertNewObjectForEntityForName("Fillup", inManagedObjectContext: moc) as! Fillup
            
            let plusMinus = Int(arc4random() % 30) - 30
            odometer += 200 + plusMinus
            
            fillup.odometer = NSDecimalNumber(integer: odometer)
            fillup.volume = NSDecimalNumber(integer:10)
            fillup.volumeUnit = VolumeUnit.usGallon.rawValue
            fillup.date = date
            fillup.car = car
            fillup.pricePerVolumeUnit = NSDecimalNumber(string: "2.34")
            fillup.currency = "USD"
        }
        
        AppDelegate.sharedInstance.saveContext(self)
        reload()
    }
    
    private var developerRows : [LabelAction] = []
    private var addFillupRows : [LabelAction] = []
    
}

extension DisplayStats {
    var hasCostBasedStats : Bool {
        get {
            return costPerVolume != nil && costPerFillup != nil && costPerDistance != nil
        }
    }
}
