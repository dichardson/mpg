//
//  Analytics.swift
//  MyMPG
//
//  Created by Douglas Richardson on 4/20/15.
//  Copyright (c) 2015 True Labs, Inc. All rights reserved.
//

import Foundation

class Analytics {
    private let tracker = GAI.sharedInstance().defaultTracker as GAITracker
    
    static let sharedInstance = {
        return Analytics()
    }()
    
    func screenView(screenName : String) {
        tracker.set(kGAIScreenName, value: screenName)
        tracker.send(GAIDictionaryBuilder.createScreenView().build() as [NSObject : AnyObject])
    }
    
    func eventWithCategory(category : EventCategory, action : String, label : String?, value : Int? = nil) {
        let event = GAIDictionaryBuilder.createEventWithCategory(category.rawValue, action: action, label: label, value: value)
        tracker.send(event.build() as [NSObject : AnyObject])
    }
}

enum EventCategory : String {
    case UIAction = "ui_action"
    case UINotification = "ui_notification"
    case DistanceTracked = "distance_tracked"
    case SelectOdometerUnit = "select_odometer_unit"
    case Settings = "settings"
    case SelectFuelEfficiencyUnit = "select_fuel_effiency_unit"
    case SelectFuelVolumeUnit = "select_fuel_volume_unit"
}