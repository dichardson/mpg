//
//  NumberFormatter.swift
//  MyMPG
//
//  Created by Douglas Richardson on 2/14/15.
//  Copyright (c) 2015 True Labs, Inc. All rights reserved.
//

import Foundation

// Treat a lone decimal separator (e.g., ".") as zero.
class LoneDecimalSeparatorFormatter : NSNumberFormatter {
    
    var treatLoneDecimalSeparatorAsZero : Bool = false
    
    override func numberFromString(string: String) -> NSNumber? {
        if treatLoneDecimalSeparatorAsZero {
            if string == self.decimalSeparator {
                return 0
            }
        }
        return super.numberFromString(string)
    }
}

// For parsing and displaying positive volume, distances. Can also be used for parsing
// prices, but output doesn't display currency symbol.
class UserRecordedValueNumberFormatter : LoneDecimalSeparatorFormatter {
    override init() {
        super.init()
        minimum = 0
        minimumIntegerDigits = 1
        maximumFractionDigits = 40
        generatesDecimalNumbers = true
        lenient = true
        usesGroupingSeparator = true
    }

    required init(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}
