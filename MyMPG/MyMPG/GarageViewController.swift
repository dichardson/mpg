//
//  GarageViewController.swift
//  MyMPG
//
//  Created by Douglas Richardson on 2/6/15.
//  Copyright (c) 2015 True Labs, Inc. All rights reserved.
//

import UIKit
import CoreData

protocol GarageViewControllerDelegate {
    func garageViewControllerDidSelectCar(controller : GarageViewController, car : Car)
}

class GarageViewController: UITableViewController, EditCarTableViewControllerDelegate {
    
    var delegate : GarageViewControllerDelegate?
    
    enum Mode {
        case CarPicker
        case RootController
    }
    
    var mode = Mode.RootController
    
    var carPickerCheckedCar : Car?
    
    private enum Section {
        case Cars
        case Settings
    }
    
    private var sections : [Section] = [.Cars, .Settings]
    
    private enum CarRow {
        case Car(MyMPG.Car)
        case AddCar
        case AddSampleData
    }
    
    private var carRows : [CarRow] = []
    
    var cars : [Car] = []
    
    class func newTableController() -> GarageViewController {
        let sb = UIStoryboard(name: "Garage", bundle: nil)
        return sb.instantiateViewControllerWithIdentifier("GarageTableController") as! GarageViewController
    }
    
    private func updateCarRows() {
        carRows = cars.map { CarRow.Car($0) }
        switch mode {
        case .CarPicker:
            break
        case .RootController:
            carRows.append(.AddCar)
            
            if UserPreferences().developerModeEnabled {
                carRows.append(.AddSampleData)
            }
        }
    }
    
    private func reload() {
        cars = fetchCars()
        updateCarRows()
        
        switch mode {
        case .CarPicker:
            navigationItem.title = LocalizedString("CHOOSE_CAR")
            navigationItem.rightBarButtonItem = nil
            navigationItem.leftBarButtonItem = nil
            sections = [.Cars]
        case .RootController:
            sections = [.Cars, .Settings]
            editButton.enabled = cars.count > 0
        }
        
        tableView.reloadData()
    }

    override func viewDidLoad() {
        super.viewDidLoad()
        reload()
        
        if self.cars.count == 0 {
            // Automatically have the user add a new car.
            showEditCarController(nil, analyticsLabel: "automatic")
        }
    }
    
    var developerModeAtTimeOfDisappear : Bool?
    
    override func viewWillAppear(animated: Bool) {
        super.viewWillAppear(animated)
        
        let screenName : String
        switch mode {
        case .CarPicker:
            screenName = "Fillup: Choose Car"
        case .RootController:
            screenName = "Home"
        }
        
        Analytics.sharedInstance.screenView(screenName)
        
        // See if the developer mode has changed since the time we last disappeared. If it has,
        // we need to reload. The reason we don't force reload every time we reappear is because
        // it interrupts table view row deselection animations when returning from the summary screen.
        // Developer mode only changes in debug builds, so it's okay to interrupt the animations.
        if let lastDevMode = developerModeAtTimeOfDisappear {
            if lastDevMode != UserPreferences().developerModeEnabled {
                reload()
            }
            developerModeAtTimeOfDisappear = nil
        }
    }
    
    override func viewWillDisappear(animated: Bool) {
        // Because the settings controller might change the developer mode, and this controller
        // depends on the developer mode, we need to track it to see if it gets changed or not.
        developerModeAtTimeOfDisappear = UserPreferences().developerModeEnabled
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    // MARK: - Table view data source

    override func numberOfSectionsInTableView(tableView: UITableView) -> Int {
        return sections.count
    }

    override func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        switch sections[section] {
        case .Cars:
            return carRows.count
        case .Settings:
            return 1
        }
    }
    
    override func tableView(tableView: UITableView, titleForHeaderInSection section: Int) -> String? {
        switch sections[section] {
        case .Cars:
            return LocalizedString("MY_CARS")
        case .Settings:
            return nil
        }
    }
    
    private let carCell = TableViewCellReuseIdAndType<DefaultTableViewCell>(reuseId: "Car")
    private let addCarCell = TableViewCellReuseIdAndType<LabelTableViewCell>(reuseId: "AddCar")
    private let settingsCell = TableViewCellReuseIdAndType<DefaultTableViewCell>(reuseId: "Settings")

    override func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        
        switch sections[indexPath.section] {
        case .Cars:
            switch carRows[indexPath.row] {
            case .Car(let car):
                let cell = carCell.dequeueFromTableView(tableView)
                cell.textLabel?.text = car.nickname
                
                switch mode {
                case .CarPicker:
                    if car == carPickerCheckedCar {
                        cell.accessoryType = .Checkmark
                    } else {
                        cell.accessoryType = .None
                    }
                case .RootController:
                    cell.editingAccessoryType = .DetailButton
                    cell.accessoryType = .DisclosureIndicator
                    break
                }
                return cell
            case .AddCar:
                let cell = addCarCell.dequeueFromTableView(tableView)
                cell.label.text = LocalizedString("ADD_CAR")
                cell.label.textAlignment = .Center
                cell.label.textColor = tableView.tintColor
                let enabled = !tableView.editing
                cell.userInteractionEnabled = enabled
                cell.label.enabled = enabled
                return cell
            case .AddSampleData:
                let cell = addCarCell.dequeueFromTableView(tableView)
                cell.label.text = "Add Sample Data" // not localized
                cell.label.textAlignment = .Center
                cell.label.textColor = tableView.tintColor
                let enabled = !tableView.editing
                cell.userInteractionEnabled = enabled
                cell.label.enabled = enabled
                return cell
            }
        case .Settings:
            let cell = settingsCell.dequeueFromTableView(tableView)
            cell.textLabel?.text = LocalizedString("PREFERENCES")
            cell.accessoryType = .DisclosureIndicator
            let enabled = !tableView.editing
            cell.userInteractionEnabled = enabled
            cell.textLabel?.enabled = enabled
            return cell
        }
    }
    
    override func tableView(tableView: UITableView, canEditRowAtIndexPath indexPath: NSIndexPath) -> Bool {
        switch sections[indexPath.section] {
        case .Cars:
            switch carRows[indexPath.row] {
            case .Car(_):
                return true
            case .AddCar:
                return false
            case .AddSampleData:
                return false
            }
        case .Settings:
            return false
        }
    }
    
    override func tableView(tableView: UITableView, accessoryButtonTappedForRowWithIndexPath indexPath: NSIndexPath) {
        switch sections[indexPath.section] {
        case .Cars:
            showEditCarController(cars[indexPath.row], analyticsLabel: "edit")
        default:
            break
        }
    }

    // Override to support editing the table view.
    override func tableView(tableView: UITableView, commitEditingStyle editingStyle: UITableViewCellEditingStyle, forRowAtIndexPath indexPath: NSIndexPath) {
        
        switch editingStyle {
        case .Delete:
            
            let deleteAction = UIAlertAction(title: LocalizedString("DELETE_CAR_BUTTON_TITLE"), style: .Destructive) {
                (action) -> Void in
                
                Analytics.sharedInstance.eventWithCategory(.UIAction, action: "press_delete_car", label: "delete")
                
                tableView.beginUpdates()
                tableView.deleteRowsAtIndexPaths([indexPath], withRowAnimation: .Fade)
                let moc = AppDelegate.sharedInstance.managedObjectContext!
                moc.deleteObject(self.cars[indexPath.row])
                
                self.cars.removeAtIndex(indexPath.row)
                self.updateCarRows()
                
                tableView.endUpdates()
                
                if self.cars.count == 0 {
                    self.donePressed()
                    self.editButton.enabled = false
                }
                
                AppDelegate.sharedInstance.saveContext(self)
                
                self.navigationController?.popViewControllerAnimated(true)
            }
            
            let cancelAction = UIAlertAction(title: LocalizedString("CANCEL"), style: .Cancel, handler: nil)
            
            let style = UIDevice.currentDevice().userInterfaceIdiom == .Pad ? UIAlertControllerStyle.Alert : UIAlertControllerStyle.ActionSheet
            let alert = UIAlertController(title: LocalizedString("WARNING_CAR_DELETE_TITLE"), message: LocalizedString("WARNING_CAR_DELETE_MESSAGE"), preferredStyle: style)
            alert.addAction(deleteAction)
            alert.addAction(cancelAction)
            
            self.presentViewController(alert, animated: true, completion: nil)
            
        default:
            assert(false)
        }
    }
    
    // MARK: EditCarTableViewControllerDelegate
    func editCarControllerRequestsSave(controller: EditCarTableViewController) {
        var car = carBeingEdited
        if car == nil {
            let moc = AppDelegate.sharedInstance.managedObjectContext!
            car = NSEntityDescription.insertNewObjectForEntityForName("Car", inManagedObjectContext: moc) as? Car
        }
        
        car!.nickname = controller.nickname!
        car!.odometerUnit = controller.odometerUnit!.rawValue
        if let id = controller.epaVehicleId {
            car!.epaVehicleId = String(id)
        } else {
            car!.epaVehicleId = nil
        }
        
        AppDelegate.sharedInstance.saveContext(self)
        
        reload()
        navigationController?.popToViewController(self, animated: true)
    }

    // MARK: - UITableViewDelegate
    
    private var carBeingEdited : Car?

    private func showEditCarController(car : Car?, analyticsLabel: String) {
        carBeingEdited = car
        
        let edit = EditCarTableViewController.newController()
        edit.editCarDelegate = self
        
        if let c = car {
            edit.nickname = c.nickname
            edit.odometerUnit = DistanceUnit(rawValue: c.odometerUnit)
            edit.existing = true
            if let id = c.epaVehicleId?.toInt() {
                edit.epaVehicleId = id
            }
        }
        
        if car == nil{
            Analytics.sharedInstance.eventWithCategory(.UIAction, action: "press_add_car_button", label: analyticsLabel)
        }
        
        self.showViewController(edit)
    }
    
    override func tableView(tableView: UITableView, shouldHighlightRowAtIndexPath indexPath: NSIndexPath) -> Bool {
        switch sections[indexPath.section] {
        case .Cars:
            return true
        case .Settings:
            return true
        }
    }
    
    override func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath) {
        
        setEditing(false)
        
        switch sections[indexPath.section] {
        case .Cars:
            switch carRows[indexPath.row] {
            case .Car(let selectedCar):
                var previouslySelectedIndex : Int?
                if let pickedCar = carPickerCheckedCar {
                    previouslySelectedIndex = cars.indexOfFirstObjectPassingTest({$0 == pickedCar})
                }
                delegate?.garageViewControllerDidSelectCar(self, car: selectedCar)
                carPickerCheckedCar = selectedCar
                
                switch mode {
                case .CarPicker:
                    if let prevIndex = previouslySelectedIndex {
                        if let section = sections.indexOfFirstObjectPassingTest({ $0 == .Cars}) {
                            let prevIndexPath = NSIndexPath(forRow: prevIndex, inSection: section)
                            tableView.cellForRowAtIndexPath(prevIndexPath)?.accessoryType = .None
                        }
                    }
                    
                    tableView.cellForRowAtIndexPath(indexPath)?.accessoryType = .Checkmark
                    tableView.deselectRowAtIndexPath(indexPath, animated: true)
                    
                case .RootController:
                    break
                }
            case .AddCar:
                showEditCarController(nil, analyticsLabel: "row")
            case .AddSampleData:
                generateAppStoreSampleData(self)
                reload()
            }
            
        case .Settings:
            let vc = SettingsTableViewController(style: .Grouped)
            self.showViewController(vc)
        }
    }
    
    // MARK: - Miscellaneous
    
    private func setEditing(editing : Bool) {
        tableView.setEditing(editing, animated: true)
        if let section = sections.indexOfFirstObjectPassingTest({$0 == .Settings}) {
            tableView.reloadSections(NSIndexSet(index: section), withRowAnimation: .Automatic)
        }
        
        if let section = sections.indexOfFirstObjectPassingTest({$0 == .Cars}) {
            let possibleRows = carRows.indexesOfObjectsPassingTest({
                switch $0 {
                case .Car(_):
                    return false
                case .AddCar:
                    return true
                case .AddSampleData:
                    return true
                }
            })
            
            let paths = possibleRows.map { NSIndexPath(forRow: $0, inSection: section) }
            tableView.reloadRowsAtIndexPaths(paths, withRowAnimation: .Automatic)
        }
        addButton.enabled = !editing
    }
    
    @IBOutlet var editButton: UIBarButtonItem!
    @IBAction func editPressed() {
        setEditing(true)
        navigationItem.leftBarButtonItem = doneButton
    }
    
    @IBOutlet weak var doneButton: UIBarButtonItem!
    @IBAction func donePressed() {
        setEditing(false)
        navigationItem.leftBarButtonItem = editButton
    }
    
    @IBOutlet var addButton: UIBarButtonItem!
    @IBAction func addPressed() {
        showEditCarController(nil, analyticsLabel: "navigation")
    }
    
    var nav : UINavigationController?
    
    func settingsPressed() {
        let vc = SettingsTableViewController(style: tableView.style)
        vc.navigationItem.leftBarButtonItem = UIBarButtonItem(barButtonSystemItem: UIBarButtonSystemItem.Done, target: self, action: Selector("settingsDonePressed"))
        let nav = UINavigationController(rootViewController: vc)
        self.nav = nav
        self.presentViewController(nav, animated: true, completion: nil)
    }
    
    func settingsDonePressed() {
        nav?.dismissViewControllerAnimated(true, completion: nil)
        nav = nil
    }
}

private func fetchCars() -> [Car] {
    let moc = AppDelegate.sharedInstance.managedObjectContext!
    var error : NSError?
    let carRequest = NSFetchRequest(entityName: "Car")
    carRequest.sortDescriptors = [NSSortDescriptor(key: "nickname", ascending: true)]
    
    if let cars = moc.executeFetchRequest(carRequest, error: &error) as? [Car] {
        return cars
    } else {
        NSLog("Error requesting cars: %@", error!.localizedDescription)
        return []
    }
}
