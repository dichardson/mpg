//
//  BezierPathExtensions.swift
//  MyMPG
//
//  Created by Douglas Richardson on 3/24/15.
//  Copyright (c) 2015 True Labs, Inc. All rights reserved.
//

import UIKit

extension UIBezierPath {
    func moveToX(x : CGFloat, y : CGFloat) {
        moveToPoint(CGPointMake(x, y))
    }
    
    func addLineToX(x : CGFloat, y : CGFloat) {
        addLineToPoint(CGPointMake(x, y))
    }
}
