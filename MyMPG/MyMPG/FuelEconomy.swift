//
//  FuelEconomy.swift
//  MyMPG
//
//  Created by Douglas Richardson on 3/4/15.
//  Copyright (c) 2015 True Labs, Inc. All rights reserved.
//

import Foundation

public struct FuelEconomy {
    public let kilometers : Double
    public let liters : Double
    public let date : NSDate
    public let beginOdometer : NSDecimalNumber // in Car's distanceUnit
    public let endOdometer : NSDecimalNumber // in Car's distanceUnit
    public let cost : NSDecimalNumber?
    public let currencyCode : String?
    public let fillupCount : Int
    
    public init(kilometers: Double,
        liters: Double,
        date: NSDate,
        beginOdometer: NSDecimalNumber,
        endOdometer: NSDecimalNumber,
        cost: NSDecimalNumber?,
        currencyCode: String?,
        fillupCount: Int) {
            self.kilometers = kilometers
            self.liters = liters
            self.date = date
            self.beginOdometer = beginOdometer
            self.endOdometer = endOdometer
            self.cost = cost
            self.currencyCode = currencyCode
            self.fillupCount = fillupCount
    }

    public var fuelEfficiency : FuelEfficiency? {
        get {
            if let kmL = kmPerLiter() {
                return FuelEfficiency(value: kmL, unit: .kmL)
            } else {
                return nil
            }
        }
    }
    
    // Returns nil if an exception occurs.
    private func kmPerLiter() -> Double? {
        if liters.isZero {
            return nil
        }
        let d = kilometers / liters
        return d.isNaN ? nil : d
    }
}

// Given a odometer decending sorted array of Fillups, returns an array of FuelEconomys
// also in odometer decending order.
public func fuelEconomiesFromFillups(let fillups : [Fillup]) -> [FuelEconomy] {
    
    // Get the fuel economy data points for the chart.
    var fuelEconomies : [FuelEconomy]  = []
    
    // Convert fillups to fuel efficiency values.
    // To calculate a fuel efficiency value you need:
    // odometer_2 - odometer_1 / volume_2, if 2 not missed and 1/2 not partial
    let NaN = NSDecimalNumber.notANumber()
    var litersAddedAccumulator = 0.0
    var costAccumulator : NSDecimalNumber?
    var costAccumulatorCurrency : String?
    var endFillup : Fillup?
    var fillupCount = Int(0)
    var lastOdometer_forSortValidationOnly : NSDecimalNumber?
    
    // fillups is ordered newest to oldest
    // To compute a fuel economy, we need to know distance and volume. What we have
    // is a list of fillups, which may be partial fillups and which may have gaps between
    // fillups (i.e., the user forgot to record a fillup and selected the missed fillup option).
    //
    // To generate a list of fuel economies, we go from newest to oldest. Fuel economy data
    // points can only be added when two non-partial entries are found with no missed entries
    // in between.
    //
    // Simple case:
    // (odometer[i] - odometer[i+1]) / volume[i]
    //
    // Partial case:
    // (odometer[i] - odoemter[i+X]) / (volume[i] + volume[i+1] + ... + volume[i+X-1])
    //
    // A miss indicates a discontinuity in the values, meaning items after the missed
    // fillup cannot be used with values before the missed fillup.
    for fillup in fillups {
        if !fillup.isPartial.boolValue {
            if let endFill = endFillup {
                // have enough information to emit a fuel economy.
                // NOTE: the current fillup is used at this point only for it's odometer reading
                // to establish a distance travelled. If we have an endFillup, then we already know how much
                // fuel was needed to drive this distance... we do not need to add the current fillups volume or costs.
                let odometerUnit = DistanceUnit(rawValue: fillup.car.odometerUnit)!
                let distanceInUnits = endFill.odometer.decimalNumberBySubtracting(fillup.odometer, withBehavior: noRaiseBehavior)
                let distance = Distance(value: distanceInUnits.doubleValue, unit: odometerUnit)
                let fuelEco = FuelEconomy(kilometers: distance.changeUnit(.kilometer).value,
                    liters: litersAddedAccumulator,
                    date: endFill.date,
                    beginOdometer: fillup.odometer,
                    endOdometer: endFill.odometer,
                    cost: costAccumulator,
                    currencyCode: costAccumulatorCurrency,
                    fillupCount: fillupCount)
                fuelEconomies.append(fuelEco)
                endFillup = nil
                litersAddedAccumulator = 0.0
                costAccumulator = nil
                costAccumulatorCurrency = nil
                fillupCount = 0
            }
        }
        
        // Verify the input array is in decending order by odometer since the calculations
        // depend on it.
        if let last = lastOdometer_forSortValidationOnly {
            assert(last.compare(fillup.odometer) == NSComparisonResult.OrderedDescending)
        }
        lastOdometer_forSortValidationOnly = fillup.odometer
        
        if fillup.missedLastFillup.boolValue {
            // Discontinuity in fillups. Cannot use fillup information collected
            // so far with fillup information collected after this point.
            endFillup = nil
            litersAddedAccumulator = 0.0
            costAccumulator = nil
            costAccumulatorCurrency = nil
            fillupCount = 0
        } else if fillup.isPartial.boolValue {
            if endFillup != nil {
                // If endFillup is set, that means we've seen a non-partial fillup
                // in the left of the list, and we're collecting the volumes of gas added
                // until we find another non-partial fillup.
                let fuelVolume = fillup.fuelVolume
                litersAddedAccumulator += fuelVolume.changeUnit(.liter).value
                fillupCount++
                if let totalPrice = fillup.totalPrice {
                    costAccumulator = costAccumulator?.decimalNumberByAdding(totalPrice, withBehavior: noRaiseBehavior)
                } else {
                    // missing price info, have to invalidate the accumulated entries.
                    costAccumulator = nil
                    costAccumulatorCurrency = nil
                }
                
                if costAccumulatorCurrency != fillup.currency {
                    // Different currency used between fillups, have to invalidate accumulated entries.
                    costAccumulator = nil
                    costAccumulatorCurrency = nil
                }
            } else {
                // If endFillup is nil, that means this is a partial fill-up but
                // we have no non-partial fillup in the left of the fillup list, so we
                // can't use this data for anything.
            }
        } else {
            // This is a non-missed, non-partial fillup. That means it can be used as the endFillup
            // value.
            assert(endFillup == nil)
            endFillup = fillup
            litersAddedAccumulator = fillup.fuelVolume.changeUnit(.liter).value
            costAccumulator = fillup.totalPrice
            costAccumulatorCurrency = fillup.currency
            fillupCount++
        }
    }
 
    return fuelEconomies
}
