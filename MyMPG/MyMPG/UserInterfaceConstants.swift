//
//  TableViewExtensions.swift
//  MyMPG
//
//  Created by Douglas Richardson on 3/11/15.
//  Copyright (c) 2015 True Labs, Inc. All rights reserved.
//

import Foundation

let StandardTableViewRowHeight = CGFloat(44.0)
