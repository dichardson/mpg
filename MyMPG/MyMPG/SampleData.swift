//
//  SampleData.swift
//  MyMPG
//
//  Created by Douglas Richardson on 4/9/15.
//  Copyright (c) 2015 True Labs, Inc. All rights reserved.
//

import Foundation
import CoreData

func generateAppStoreSampleData(vc : UIViewController) {
    let moc = AppDelegate.sharedInstance.managedObjectContext!
    let car = NSEntityDescription.insertNewObjectForEntityForName("Car", inManagedObjectContext: moc) as! Car
    car.nickname = "Camry"
    car.odometerUnit = DistanceUnit.mile.rawValue
    car.epaVehicleId = "26423" // EPA vehicle database ID for 2010 Camry 4L.
    addSampleFillupsFor2010Camry(car)
    AppDelegate.sharedInstance.saveContext(vc)
}

// Values generated are reasonable for a 2010 Camry
private func addSampleFillupsFor2010Camry(car : Car) {
    // *****************************
    // Input values
    // *****************************
    let maxMPG = 38.23
    let minMPG = 17.5
    let maxGallons = 11.23
    let minGallons = 5.8
    let numberOfFillups = 23
    let odometerStart = 38258
    let maxPriceCents = UInt32(354)
    let minPriceCents = UInt32(248)
    
    let startDateComponents = NSDateComponents()
    startDateComponents.year = 2014
    startDateComponents.month = 1
    startDateComponents.day = 1
    startDateComponents.hour = 10
    startDateComponents.minute = 0
    startDateComponents.second = 0
    
    let calendar = NSCalendar.currentCalendar()
    let currency = LocaleUnits().isoCurrencyCode
    // *****************************
    
    
    let moc = car.managedObjectContext!
    
    let dayInSeconds = 60.0 * 60.0 * 24.0
    let weekInSeconds = 7.0 * dayInSeconds
    let fillupInterval = 1.5 * weekInSeconds
    
    
    let mpgRange = maxMPG - minMPG
    let gallonRange = maxGallons - minGallons
    let priceRangeCents = UInt32(maxPriceCents - minPriceCents)
    
    var odometer = odometerStart
    var date = calendar.dateFromComponents(startDateComponents)!
    
    let hundred = NSDecimalNumber(integer: 100)
    
    let thousandths = NSDecimalNumberHandler(roundingMode: NSRoundingMode.RoundPlain, scale: 3, raiseOnExactness: false, raiseOnOverflow: true, raiseOnUnderflow: true, raiseOnDivideByZero: true)
    
    for i in 1...numberOfFillups {
        let mpg = (Double(arc4random()) % mpgRange) + minMPG
        let gallons = (Double(arc4random()) % gallonRange) + minGallons
        let miles = Int(mpg * gallons)
        
        let priceInCents = arc4random() % priceRangeCents + minPriceCents
        
        odometer += miles
        date = date.dateByAddingTimeInterval(fillupInterval)
        
        let fillup = NSEntityDescription.insertNewObjectForEntityForName("Fillup", inManagedObjectContext: moc) as! Fillup
        fillup.odometer = NSDecimalNumber(integer: odometer)
        fillup.volume = NSDecimalNumber(double:gallons).decimalNumberByRoundingAccordingToBehavior(thousandths)
        fillup.volumeUnit = VolumeUnit.usGallon.rawValue
        fillup.date = date
        fillup.car = car
        fillup.pricePerVolumeUnit = NSDecimalNumber(integer: Int(priceInCents)).decimalNumberByDividingBy(hundred)
        fillup.currency = currency
    }
}