//
//  ViewControllerExtension.swift
//  MyMPG
//
//  Created by Douglas Richardson on 3/8/15.
//  Copyright (c) 2015 True Labs, Inc. All rights reserved.
//

import Foundation

extension UIViewController {
    func showViewController(viewController : UIViewController) {
        if (self.respondsToSelector(Selector("showViewController:sender:"))) {
            // iOS 8
            self.showViewController(viewController, sender: self)
        } else {
            // pre iOS 8
            self.navigationController?.pushViewController(viewController, animated: true)
        }
    }
}
