//
//  PickerTableViewCell.swift
//  MyMPG
//
//  Created by Douglas Richardson on 3/17/15.
//  Copyright (c) 2015 True Labs, Inc. All rights reserved.
//

import UIKit

class PickerTableViewCell: SingleViewCell {

    var picker : UIPickerView { get { return view as! UIPickerView } }
    
    override init(style: UITableViewCellStyle, reuseIdentifier: String?) {
        super.init(style: .Default, reuseIdentifier: reuseIdentifier)
        setSubview(UIPickerView())
    }

    required init(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

}
