//
//  LabelTextFieldTableViewCell.swift
//  MyMPG
//
//  Created by Douglas Richardson on 2/13/15.
//  Copyright (c) 2015 True Labs, Inc. All rights reserved.
//

import UIKit

class LabelTextFieldTableViewCell: UITableViewCell {
    @IBOutlet var label : UILabel!
    @IBOutlet var textField : UITextField!
}
