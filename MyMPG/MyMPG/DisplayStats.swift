//
//  DisplayStats.swift
//  MyMPG
//
//  Created by Douglas Richardson on 3/25/15.
//  Copyright (c) 2015 True Labs, Inc. All rights reserved.
//

import Foundation

struct LabeledStat {
    let label : String
    let value : String
}

struct SelfDescribingStat {
    let value : String
}

struct DisplayStats
{
    let bestFuelEfficiency : LabeledStat?
    let averageFuelEfficiency : LabeledStat?
    let lastFuelEfficiency : LabeledStat?
    let costPerDistance : SelfDescribingStat?
    let costPerFillup : SelfDescribingStat?
    let costPerVolume : SelfDescribingStat?
}

extension LabeledStat {
    
    var attributedDisplayString : NSAttributedString {
        get {
            let fontSize = UIFont.systemFontSize()
            let systemFontAttributes = [NSFontAttributeName : UIFont.systemFontOfSize(fontSize)]
            let boldSystemFontAttribute = [NSFontAttributeName : UIFont.boldSystemFontOfSize(fontSize)]
            let attrLabel = NSAttributedString(string: label, attributes: boldSystemFontAttribute)
            let attrValue = NSAttributedString(string: value, attributes: systemFontAttributes)
            let result = NSMutableAttributedString(attributedString: attrLabel)
            result.appendAttributedString(attrValue)
            return result
        }
    }
}

extension SelfDescribingStat {
    var attributedDisplayString : NSAttributedString {
        get {
            let fontSize = UIFont.systemFontSize()
            let boldSystemFontAttribute = [NSFontAttributeName : UIFont.boldSystemFontOfSize(fontSize)]
            return NSAttributedString(string: value, attributes: boldSystemFontAttribute)
        }
    }
}

extension DisplayStats {
    init(stats : Stats, mostRecent : FuelEconomy?, localeUnits : LocaleUnits) {
        let currentLocale = NSLocale.currentLocale()
        
        let fuelEfficiencyFormatter = makeFuelEfficiencyNumberFormatter(1)
        
        let makeFuelEfficiencyStat = { (key : String, value : NSNumber) -> LabeledStat in
            let label = LocalizedString(key)
            let strValue = fuelEfficiencyFormatter.stringFromNumber(value) ?? ""
            
            return LabeledStat(label: label.uppercaseStringWithLocale(currentLocale), value: strValue)
        }
        
        if let best = stats.bestFuelEfficiency?.changeUnit(localeUnits.fuelEfficiencyUnits)?.value {
            self.bestFuelEfficiency = makeFuelEfficiencyStat("STAT_LABEL_BEST", best)
        } else {
            self.bestFuelEfficiency = nil
        }
        
        if let avg = stats.averageFuelEfficiency?.changeUnit(localeUnits.fuelEfficiencyUnits)?.value {
            self.averageFuelEfficiency = makeFuelEfficiencyStat("STAT_LABEL_AVERAGE", avg)
        } else {
            self.averageFuelEfficiency = nil
        }
        
        if let last = mostRecent?.fuelEfficiency?.changeUnit(localeUnits.fuelEfficiencyUnits)?.value {
            self.lastFuelEfficiency = makeFuelEfficiencyStat("STAT_LABEL_LAST", last)
        } else {
            self.lastFuelEfficiency = nil
        }
        
        let currencyFormatter = NSNumberFormatter()
        currencyFormatter.numberStyle = .CurrencyStyle
        if let code = stats.currencyCode {
            currencyFormatter.currencyCode = code
        }
        
        if let costPerFillup = stats.costPerFillup {
            let costPerFillupStringValue = currencyFormatter.stringFromNumber(costPerFillup) ?? ""
            self.costPerFillup = SelfDescribingStat(value: NSString(format: LocalizedString("STAT_LABEL_COST_PER_FILLUP_FORMAT"), costPerFillupStringValue) as String)
        } else {
            self.costPerFillup = nil
        }
        
        if let costPerVU = stats.costPerVolume?.changeUnit(localeUnits.volumeUnit).value {
            let costPerVolumeStringValue = currencyFormatter.stringFromNumber(costPerVU) ?? ""
            self.costPerVolume = SelfDescribingStat(value: NSString(format: LocalizedString("STAT_LABEL_COST_PER_VOLUME_LABEL"), costPerVolumeStringValue, localeUnits.abbreviatedVolumeUnitDisplayString) as String)
        } else {
            self.costPerVolume = nil
        }
        
        if let costPerDU = stats.costPerDistance?.changeUnit(localeUnits.distanceUnit).value {
            let costPerDistanceStringValue = currencyFormatter.stringFromNumber(costPerDU) ?? ""
            self.costPerDistance = SelfDescribingStat(value: NSString(format: LocalizedString("STAT_LABEL_COST_PER_DISTANCE_FORMAT"), costPerDistanceStringValue, localeUnits.distanceUnitDisplayString) as String)
        } else {
            self.costPerDistance = nil
        }
    }
}
