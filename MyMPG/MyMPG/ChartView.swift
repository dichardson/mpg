//
//  ChartView.swift
//  MyMPG
//
//  Created by Douglas Richardson on 3/23/15.
//  Copyright (c) 2015 True Labs, Inc. All rights reserved.
//

import UIKit

private let YAxisLeftPad = CGFloat(2.0)
private let XAxisLeftPad = YAxisLeftPad
private let XAxisHeight = CGFloat(36.0)
private let TitleHeight = CGFloat(30.0)
private let TitleFontSize = CGFloat(12.0)
private let LabelFontSize = CGFloat(10.0)
private let AxisTickLength = CGFloat(2)
private let AxisLineWidth = CGFloat(1)

class ChartView: UIView {
    let chart : JBLineChartView
    let xAxis : XAxisView
    let yAxis : YAxisView
    let title : TitleView
    
    
    // scale is the values that can appear in the chart. Any points that fall outside the scale
    // will not be displayed.
    var scale = CGRectMake(0, 0, 10, 10)
    
    override init(frame: CGRect) {
        chart = JBLineChartView()
        xAxis = XAxisView(frame:CGRectZero)
        yAxis = YAxisView(frame:CGRectZero)
        title = TitleView(frame:CGRectZero)
        
        super.init(frame: frame)
        
        addSubview(chart)
        addSubview(yAxis)
    }
    
    override var backgroundColor: UIColor? {
        didSet {
            xAxis.backgroundColor = backgroundColor
            yAxis.backgroundColor = backgroundColor
            title.backgroundColor = backgroundColor
        }
    }
    
    required init(coder aDecoder: NSCoder) {
        fatalError("Not implemented")
    }
    
    override func layoutSubviews() {
        super.layoutSubviews()
        
        let yAxisForcedHeight = bounds.height - XAxisHeight - TitleHeight
        let yAxisSize = yAxis.sizeThatFits(CGSizeMake(bounds.width - YAxisLeftPad, yAxisForcedHeight))
        yAxis.frame = CGRectMake(CGRectGetMaxX(bounds) - yAxisSize.width, TitleHeight, yAxisSize.width, yAxisForcedHeight)
        
        let chartPad = YAxisLeftPad + yAxisSize.width
        chart.frame = CGRectMake(chartPad, 0, bounds.width - chartPad * 2, bounds.height)
        chart.reloadData()
        
        if xAxis.superview == nil {
            xAxis.frame = CGRectMake(0, 0, 0, XAxisHeight)
            chart.footerPadding = XAxisLeftPad
            chart.footerView = xAxis
        }
        if title.superview == nil {
            title.frame = CGRectMake(0, 0, 0, TitleHeight)
            chart.headerView = title
        }
    }
}

private func chartLabel(alignment : NSTextAlignment = .Left) -> UILabel {
    let label = UILabel()
    label.font = UIFont.systemFontOfSize(LabelFontSize)
    label.textAlignment = alignment
    label.textColor = UIColor.whiteColor()
    return label
}

class XAxisView : UIView {
    var strokeColor = UIColor.whiteColor()
    let leftLabel = chartLabel()
    let rightLabel = chartLabel(alignment: .Right)
    let topLegend = LegendView(frame: CGRectZero)
    let midLegend = LegendView(frame: CGRectZero)
    let bottomLegend = LegendView(frame: CGRectZero)
    
    override init(frame: CGRect) {
        leftLabel.numberOfLines = 2
        rightLabel.numberOfLines = 2
        
        super.init(frame: frame)
        
        addSubview(leftLabel)
        addSubview(rightLabel)
        addSubview(topLegend)
        addSubview(midLegend)
        addSubview(bottomLegend)
        
        self.contentMode = .Redraw
    }
    
    required init(coder aDecoder: NSCoder) {
        fatalError("not implemented")
    }
    
    
    override func drawRect(rect: CGRect) {
        // When stroking a path, the line is drawn half above and half below the line. Adjusting
        // by half the line width creates a pixel aligned line.
        let pixelAlignAdjust = AxisLineWidth * 0.5
        let topY = CGRectGetMinY(bounds) + pixelAlignAdjust
        let bottomTickY = topY + AxisTickLength
        let minX = CGRectGetMinX(bounds) + pixelAlignAdjust
        let maxX = CGRectGetMaxX(bounds) - pixelAlignAdjust
        
        let path = UIBezierPath()
        path.moveToX(minX, y: bottomTickY)
        path.addLineToX(minX, y: topY)
        path.addLineToX(maxX, y: topY)
        path.addLineToX(maxX, y: bottomTickY)
        path.lineWidth = AxisLineWidth
        
        strokeColor.setStroke()
        path.stroke()
    }
    
    override func layoutSubviews() {
        super.layoutSubviews()
        let labelWidth = CGFloat(80)
        let labelHeight = CGRectGetHeight(bounds) - AxisTickLength
        let leftSize = leftLabel.sizeThatFits(CGSizeMake(labelWidth, labelHeight))
        leftLabel.frame = CGRectMake(AxisLineWidth, AxisTickLength, leftSize.width, leftSize.height)
        
        let rightSize = rightLabel.sizeThatFits(CGSizeMake(labelWidth, labelHeight))
        rightLabel.frame = CGRectMake(CGRectGetMaxX(bounds) - rightSize.width - AxisTickLength, AxisTickLength, rightSize.width, rightSize.height)

        let legendWidth = CGFloat(120)
        let legendHeight = labelHeight * 0.33
        topLegend.frame = CGRectMake(CGRectGetMidX(bounds) - legendWidth*0.5, AxisTickLength, legendWidth, legendHeight)
        midLegend.frame = CGRectMake(CGRectGetMinX(topLegend.frame), CGRectGetMaxY(topLegend.frame), legendWidth, legendHeight)
        bottomLegend.frame = CGRectMake(CGRectGetMinX(midLegend.frame), CGRectGetMaxY(midLegend.frame), legendWidth, legendHeight)
    }
}

class YAxisView : UIView {
    var strokeColor = UIColor.whiteColor()
    let topLabel = chartLabel()
    let bottomLabel = chartLabel()
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        addSubview(topLabel)
        addSubview(bottomLabel)
        self.contentMode = .Redraw
    }

    required init(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override func drawRect(rect: CGRect) {
        let pixelAlignAdjust = AxisLineWidth * 0.5
        let leftX = pixelAlignAdjust
        let tickX = AxisTickLength + leftX
        let bottom = CGRectGetMaxY(bounds) - pixelAlignAdjust
        let top = CGRectGetMinY(bounds) + pixelAlignAdjust
        
        let path = UIBezierPath()
        path.moveToX(tickX, y: bottom)
        path.addLineToX(tickX, y: bottom)
        path.addLineToX(leftX, y: bottom)
        path.addLineToX(leftX, y: top)
        path.addLineToX(tickX, y: top)
        path.lineWidth = AxisLineWidth
        
        strokeColor.setStroke()
        path.stroke()
    }
    
    override func layoutSubviews() {
        super.layoutSubviews()
        let labelHeight = CGFloat(20)
        let width = CGRectGetWidth(bounds) - AxisTickLength
        
        let topSize = topLabel.sizeThatFits(CGSizeMake(width, labelHeight))
        topLabel.frame = CGRectMake(AxisTickLength, AxisLineWidth, topSize.width, topSize.height)
        
        let bottomSize = bottomLabel.sizeThatFits(CGSizeMake(width, labelHeight))
        bottomLabel.frame = CGRectMake(AxisTickLength, CGRectGetMaxY(bounds)-AxisLineWidth-bottomSize.height, bottomSize.width, bottomSize.height)
    }
    
    override func sizeThatFits(size: CGSize) -> CGSize {
        let topLabelSize = topLabel.sizeThatFits(size)
        let bottomLabelSize = bottomLabel.sizeThatFits(size)
        let maxLabelWidth = max(topLabelSize.width, bottomLabelSize.width)
        let width = maxLabelWidth + AxisTickLength
        let height = AxisLineWidth + topLabelSize.height + bottomLabelSize.height + AxisLineWidth
        return CGSizeMake(width, height)
    }
}

class TitleView : UIView {
    let centerLabel = chartLabel(alignment: .Center)
    override init(frame: CGRect) {
        centerLabel.font = UIFont.boldSystemFontOfSize(TitleFontSize)
        super.init(frame: frame)
        addSubview(centerLabel)
    }

    required init(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override func layoutSubviews() {
        centerLabel.frame = bounds
    }
}

class LegendView : UIView {
    let label = chartLabel()
    let dashView = DashView(frame:CGRectZero)
    
    override init(frame: CGRect) {
        super.init(frame:frame)
        addSubview(label)
        addSubview(dashView)
    }

    required init(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override func layoutSubviews() {
        super.layoutSubviews()
        
        let h = CGRectGetHeight(bounds)
        let w = CGRectGetWidth(bounds)
        let padding = CGFloat(2)
        dashView.frame = CGRectMake(0, 0, 20, h)
        label.frame = CGRectMake(CGRectGetMaxX(dashView.frame)+padding, 0, w - CGRectGetWidth(dashView.frame)-padding, h)
    }
}

class DashView : UIView {
    var dashColor = UIColor.whiteColor()
    var lineWidth = CGFloat(1)
    
    override init(frame: CGRect) {
        super.init(frame:frame)
        self.opaque = false
    }

    required init(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override func drawRect(rect: CGRect) {
        let midY = CGRectGetMidY(bounds)
        
        let p = UIBezierPath()
        p.moveToPoint(CGPointMake(CGRectGetMinX(bounds), midY))
        p.addLineToPoint(CGPointMake(CGRectGetMaxX(bounds), midY))
        p.lineWidth = lineWidth
        let pattern : [CGFloat] = [3,2] // same pattern JBLineChartView uses
        p.setLineDash(pattern, count: pattern.count, phase: 0)
        
        dashColor.setStroke()
        p.stroke()
    }
}
