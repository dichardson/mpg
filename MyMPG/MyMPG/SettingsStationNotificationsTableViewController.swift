//
//  SettingsStationNotificationsTableViewController.swift
//  MyMPG
//
//  Created by Douglas Richardson on 3/31/15.
//  Copyright (c) 2015 True Labs, Inc. All rights reserved.
//

import UIKit
import CoreData
import CoreLocation

class SettingsStationNotificationsTableViewController: UITableViewController {
    
    private enum Sections {
        case GlobalOptions
        case Stations
    }
    
    private let sections : [Sections] = [.GlobalOptions, .Stations]
    private var stations : [Station] = []
    private let userPrefs = UserPreferences()
    
    private enum GlobalOptionRows {
        case UseReminders
        case OpeniOSSettings
    }
    
    private let globalOptionRows : [GlobalOptionRows] = [.UseReminders, .OpeniOSSettings]

    override func viewDidLoad() {
        super.viewDidLoad()
        navigationItem.title = LocalizedString("FILL_UP_REMINDERS")
        
        Analytics.sharedInstance.screenView("Settings: Fillup Reminder")
        
        reload()
    }
    
    private func reload() {
        fetchStations()
        tableView.reloadData()
    }
    
    private func fetchStations() {
        
        // 20 is the Apple published maximum number of regions an app can monitor at once. We show no more
        // than 20 so that it is not possible for the user to attempt to enable more than 20, which wouldn't
        // actually work due to Apple's 20 limit.
        // https://developer.apple.com/library/ios/documentation/CoreLocation/Reference/CLLocationManager_Class/index.html?hl=p#//apple_ref/occ/instm/CLLocationManager/startMonitoringForRegion:
        let maxMonitoredRegions = 20
        
        // Build up a list of 20 stations, starting with the currently monitored stations.
        // If there are <20 monitored stations then add unmonitoried stations, starting with the
        // most recently used non-monitored stations.
        
        let monitoredRegions = AppDelegate.sharedInstance.gasStationLocationMonitor.monitoredRegions
        let regionIDs = monitoredRegions.map{NSURL(string: $0.identifier)}.filter{$0 != nil}
        
        let moc = AppDelegate.sharedInstance.managedObjectContext!
        let objectIDs = regionIDs.map{moc.persistentStoreCoordinator!.managedObjectIDForURIRepresentation($0!)}.filter{$0 != nil}.map{$0!}
        
        var newStations : [Station] = []
        
        // Add enabled stations (no fetch limit, we want to show all enabled stations even if Apple ups the 20 monitored station limit).
        var error : NSError?
        let enabledObjectsPredicate = NSPredicate(format: "self IN %@", argumentArray: [objectIDs])
        
        let req = NSFetchRequest(entityName: "Station")
        req.predicate = enabledObjectsPredicate
        
        if let stations = moc.executeFetchRequest(req, error: &error) as? [Station] {
            newStations = stations
        } else {
            NSLog("Error requesting stations (1): %@", error!.localizedDescription)
        }
        
        // Now add up to 20 non-enabled stations, starting with the most recently used.
        if newStations.count < maxMonitoredRegions {
            println("Don't have 20 monitired regions, looking for more")
            let req = NSFetchRequest(entityName: "Station")
            let disabledObjectsPredicate = NSCompoundPredicate(type: NSCompoundPredicateType.NotPredicateType, subpredicates: [enabledObjectsPredicate])
            req.sortDescriptors = [NSSortDescriptor(key: "lastUsed", ascending: false)]
            req.fetchLimit = maxMonitoredRegions - newStations.count
            req.predicate = disabledObjectsPredicate
            error = nil
            if let stations = moc.executeFetchRequest(req, error: &error) as? [Station] {
                newStations.splice(stations, atIndex: newStations.count)
            }
        } else {
            println("Already have 20 monitored regions")
        }
        
        self.stations = newStations
    }

    // MARK: - Table view data source

    override func numberOfSectionsInTableView(tableView: UITableView) -> Int {
        return sections.count
    }

    override func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        switch sections[section] {
        case .GlobalOptions:
            return globalOptionRows.count
        case .Stations:
            return stations.count
        }
    }

    override func tableView(tableView: UITableView, titleForFooterInSection section: Int) -> String? {
        switch sections[section] {
        case .GlobalOptions:
            return LocalizedString("REMINDER_SETTINGS_TEXT")
        case .Stations:
            return nil
        }
    }
    
    private let enableRemindersCell = TableViewCellReuseIdAndType<LabelSwitchTableViewCell>(reuseId: "EnableReminders")
    private let stationCell = TableViewCellReuseIdAndType<SubtitleSwitchTableViewCell>(reuseId: "Station")
    private let buttonCell = TableViewCellReuseIdAndType<Value1TableViewCell>(reuseId: "ButtonCell")

    override func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        switch sections[indexPath.section] {
        case .GlobalOptions:
            switch globalOptionRows[indexPath.row] {
            case .UseReminders:
                let cell = enableRemindersCell.dequeueFromTableView(tableView)
                cell.label.text = LocalizedString("USE_FILL_UP_REMINDERS")
                cell.switchView.on = !userPrefs.fillupRemindersDisabled
                cell.switchView.removeTarget(nil, action: nil, forControlEvents: .AllEvents)
                cell.switchView.addTarget(self, action: Selector("enableFillupRemindersChanged:"), forControlEvents: .ValueChanged)
                return cell
            case .OpeniOSSettings:
                let cell = buttonCell.dequeueFromTableView(tableView)
                cell.textLabel?.text = LocalizedString("OPEN_SETTINGS")
                cell.textLabel?.textColor = tableView.tintColor
                cell.accessoryType = .None
                return cell
            }
        case .Stations:
            let monitoredRegions = AppDelegate.sharedInstance.gasStationLocationMonitor.monitoredRegions
            let station = stations[indexPath.row]
            let stationID = station.objectID.URIRepresentation().absoluteString!
            
            let cell = stationCell.dequeueFromTableView(tableView)
            cell.label.text = station.name
            cell.subtitle.text = station.address
            cell.switchView.on = monitoredRegions.filter{$0.identifier == stationID}.count > 0
            cell.switchView.removeTarget(nil, action: nil, forControlEvents: .AllEvents)
            cell.switchView.addTarget(self, action: Selector("enableStationChanged:"), forControlEvents: .ValueChanged)
            cell.switchView.tag = indexPath.row
            let enabled = !userPrefs.fillupRemindersDisabled
            cell.switchView.enabled = enabled
            cell.label.enabled = enabled
            cell.subtitle.enabled = enabled
            return cell
        }
    }
    
    override func tableView(tableView: UITableView, shouldHighlightRowAtIndexPath indexPath: NSIndexPath) -> Bool {
        switch sections[indexPath.section] {
        case .GlobalOptions:
            return globalOptionRows[indexPath.row] == .OpeniOSSettings
        case .Stations:
            return false
        }
    }
    
    override func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath) {
        switch sections[indexPath.section] {
        case .GlobalOptions:
            switch globalOptionRows[indexPath.row] {
            case .OpeniOSSettings:
                if let url = NSURL(string: UIApplicationOpenSettingsURLString) {
                    UIApplication.sharedApplication().openURL(url)
                }
                tableView.deselectRowAtIndexPath(indexPath, animated: true)
            case .UseReminders:
                break
            }
        default:
            break
        }
    }
    
    func enableFillupRemindersChanged(sender : UISwitch) {
        userPrefs.fillupRemindersDisabled = !sender.on
        if let section = sections.indexOfFirstObjectPassingTest({$0 == .Stations}) {
            let indexSet = NSIndexSet(index: section)
            tableView.reloadSections(indexSet, withRowAnimation: .Fade)
        }
        var label = userPrefs.fillupRemindersDisabled ? "Off" : "On"
        Analytics.sharedInstance.eventWithCategory(.UIAction, action: "change_global_station_reminders", label: label)
    }
    
    override func viewWillDisappear(animated: Bool) {
        let label = userPrefs.fillupRemindersDisabled ? "Off" : "On"
        Analytics.sharedInstance.eventWithCategory(.Settings, action: "set_fillup_reminders", label: label)
        super.viewWillDisappear(animated)
    }
    
    func enableStationChanged(sender : UISwitch) {
        let station = stations[sender.tag]
        println("Enable Station Changed: \(sender.on) for station \(station.name)")
        
        Analytics.sharedInstance.eventWithCategory(.UIAction, action: "change_individual_station_reminders", label: sender.on ? "On" : "Off")
        
        let mon = AppDelegate.sharedInstance.gasStationLocationMonitor!
        
        station.fillupRemindersDisabled = !sender.on
        AppDelegate.sharedInstance.saveContext(self)
        
        if sender.on {
            let loc = CLLocation(latitude: station.latitude.doubleValue, longitude: station.longitude.doubleValue)
            mon.startMonitoringStation(station, callback: { (ok, alert) -> () in
                if ok {
                    println("Added station with region id \(station.objectID)")
                } else if let vc = alert {
                    self.presentViewController(vc, animated: true, completion: nil)
                } else {
                    // should never get here
                    NSLog("Unexpected ok=false alert=nil result from startMonitoringStation")
                }
            })
        } else {
            mon.stopMonitoringStation(station)
        }
    }
}

