//
//  AddFuelTableViewController.swift
//  MyMPG
//
//  Created by Douglas Richardson on 2/11/15.
//  Copyright (c) 2015 True Labs, Inc. All rights reserved.
//

import UIKit
import CoreData
import CoreLocation

protocol AddFuelTableViewControllerDelegate {
    func addFuelControllerDidAddFuel(controller : AddFuelTableViewController)
}

class AddFuelTableViewController: UITableViewController, GarageViewControllerDelegate, DriveStyleViewControllerDelegate, GasStationMapViewControllerDelegate, UITextFieldDelegate {
    
    var delegate : AddFuelTableViewControllerDelegate?
    let localeUnits = LocaleUnits()
    lazy var currencyFormatter : NSNumberFormatter = {
        let f = NSNumberFormatter()
        f.numberStyle = .CurrencyStyle
        return f
    }()
    
    // raw value is reuse identifier
    private enum CellMeta : String {
        case Car = "Car"
        case Date = "Date"
        case DatePicker = "DatePicker"
        case Odometer = "Odometer"
        case Volume = "Volume"
        case PricePerVolume = "PricePerVolume"
        case PartialFillup = "PartialFillup"
        case MissedFillup = "MissedFillup"
        case Style = "Style"
        case Octane = "Octane"
        case Station = "Station"
        case Delete = "Delete"
    }
    
    class func storyboard() -> UIStoryboard {
        return UIStoryboard(name: "AddFuel", bundle: nil)
    }
    
    class func newTableControllerWithCar(car : Car) -> AddFuelTableViewController {
        let vc = storyboard().instantiateInitialViewController() as! AddFuelTableViewController
        vc.car = car
        return vc
    }
    
    // cells defines the order of the table view cells by reuse identifier.
    //var cells : [CellMeta] = [.Odometer, .Volume, .PricePerVolume, .Station, .Date, .Car, .PartialFillup]
    
    private var cells : [[CellMeta]] = [[.Odometer, .Volume, .PricePerVolume, .Station], [.Date, .Car, .Style], [.PartialFillup, .MissedFillup]]
    
    private func cellMetaAtIndexPath(indexPath : NSIndexPath) -> CellMeta {
        return cells[indexPath.section][indexPath.row]
    }
    
    private func cellMetaIndexPath(cell : CellMeta) -> NSIndexPath? {
        return indexPathOfFirstObjectPassingTest(cells, { $0 == cell })
    }
    
    private func tableViewCellFromCellMeta(cell : CellMeta) -> UITableViewCell? {
        if let i = cellMetaIndexPath(cell) {
            return tableView.cellForRowAtIndexPath(i)
        }
        return nil
    }
    
    // Creating my own outlet because setting the Table View Row Height on the
    // Table View or Table View Cells does nothing.
    @IBInspectable var normalRowHeight = StandardTableViewRowHeight // set here or in Interface Builder
    var datePickerRowHeight : CGFloat? // set in viewDidLoad
    
    var existing = false
    
    var fillUpDate = NSDate()
    var isPartialFillUp = false
    var isMissedFillUp = false
    
    var octane : String?
    var dstyle : Float?
    
    var car : Car!
    // TODO: remove?
    var changeInOdometerRange = 0
    
    @IBOutlet var numberFormatter : NSNumberFormatter?
    
    @IBOutlet var saveButton: UIBarButtonItem? {
        didSet {
            updateSaveEnabled()
        }
    }
    
    var enteredVolume : String? {
        didSet {
            updateSaveEnabled()
            updateTotal()
        }
    }
    
    var enteredVolumeUnit = VolumeUnit.liter
    
    var enteredOdometer : String? {
        didSet {
            updateSaveEnabled()
        }
    }
    
    var enteredPricePerUnitVolume : String? {
        didSet {
            updateTotal()
        }
    }
    
    var totalPrice : NSDecimalNumber = NSDecimalNumber(int:0) {
        didSet {
            if let cell = tableViewCellFromCellMeta(.PricePerVolume) as? LabelSubtitleTextFieldTableViewCell {
                let totalStr = currencyFormatter.stringFromNumber(totalPrice) ?? "0"
                cell.subtitleLabel.text = NSString(format: LocalizedString("TOTAL_PRICE_FORMAT"), totalStr) as String
            }
        }
    }
    
    private func updateTotal() {
        var total = NSDecimalNumber(int: 0)
        if let volume = numberFormatter?.numberFromString(enteredVolume ?? "0") as? NSDecimalNumber {
            if let ppuv = numberFormatter?.numberFromString(enteredPricePerUnitVolume ?? "0") as? NSDecimalNumber {
                total = volume.decimalNumberByMultiplyingBy(ppuv)
            }
        }
        totalPrice = total
    }
    
    var initialSelectedGasStation : Station?
    var selectedGasStation : GasStationMapViewController.Station?
    var fillupToEdit : Fillup?
    
    func updateSaveEnabled() {
        var enabled = false
        if let v = enteredVolume {
            if let o = enteredOdometer {
                enabled = !(v.isEmpty || o.isEmpty)
            }
        }
        saveButton?.enabled = enabled
    }
    
    @IBAction func volumeTextFieldChanged(sender: UITextField) {
        enteredVolume = sender.text
    }
    @IBAction func odometerTextFieldChanged(sender: UITextField) {
        enteredOdometer = sender.text
    }
    @IBAction func pricePerUnitVolumeChanged(sender: UITextField) {
        enteredPricePerUnitVolume = sender.text
    }

    @IBAction func savePressed(sender: UIBarButtonItem) {
        
        let (ok, station, newStationAdded) = saveFuelEntryToDatabase()
        if !ok {
            // error adding fuel. addFuelEntryToDatabase will have put up an alert already so
            // don't put another one up here.
            return
        }
        
        let label = existing ? "edit" : "new"
        Analytics.sharedInstance.eventWithCategory(.UIAction, action: "press_save_fillup", label: label)
        
        if let s = station {
            if !s.fillupRemindersDisabled {
                AppDelegate.sharedInstance.gasStationLocationMonitor.startMonitoringStation(s, callback: { (ok, alert) -> () in
                    
                    if ok {
                        
                        if newStationAdded {
                            // A station was selected for this fill up and there was no previously selected station either
                            // because this is a new fillup (e.g., fillupToEdit == nil) or because the existing fillup
                            // has no station.
                            Analytics.sharedInstance.eventWithCategory(.UINotification, action: "add_station_monitoring", label: label, value: 1)
                        }
                        
                        self.delegate?.addFuelControllerDidAddFuel(self)
                    } else if let vc = alert {
                        self.presentViewController(vc, animated: true, completion: nil)
                    } else {
                        // should never get here
                        println("\(__FUNCTION__): ok is false and alert is nil.")
                    }
                })
            } else {
                delegate?.addFuelControllerDidAddFuel(self)
            }
        } else {
            delegate?.addFuelControllerDidAddFuel(self)
        }
    }
    
    @IBAction func isPartialFillUpChanged(sender: UISwitch) {
        isPartialFillUp = sender.on
    }
    
    @IBAction func isMissedFillUpChanged(sender: UISwitch) {
        isMissedFillUp = sender.on
    }
    
    func errorSavingEntry(message: String) {
        NSLog("Error Saving Fill-up: %@", message)
        let alert = UIAlertController(title: LocalizedString("ERROR_SAVING_FILLUP_TITLE"),
            message: message, preferredStyle: .Alert)
        let action = UIAlertAction(title: LocalizedString("OK_ALERT_BUTTON_TITLE"), style: .Default, handler: nil)
        alert.addAction(action)
        self.presentViewController(alert, animated: true, completion: nil)
    }
    
    func saveFuelEntryToDatabase() -> (Bool, MyMPG.Station?, Bool) {
        let moc = AppDelegate.sharedInstance.managedObjectContext!
        
        let odometer : NSDecimalNumber! = numberFormatter?.numberFromString(enteredOdometer ?? "") as? NSDecimalNumber
        let volume : NSDecimalNumber! = numberFormatter?.numberFromString(enteredVolume ?? "") as? NSDecimalNumber
        let fail : (Bool, MyMPG.Station?, Bool) = (false, nil, false)
        
        if odometer == nil {
            // should not happen, but just in case
            errorSavingEntry(LocalizedString("ODOMETER_NOT_SET"))
            return fail
        }
        
        if volume == nil {
            // should not happen, but just in case
            errorSavingEntry(LocalizedString("VOLUME_NOT_SET"))
            return fail
        }
        
        // Verify the odometer value is unique for the current car.
        var predicate = NSPredicate(format: "self.car == %@ AND self.odometer == %@", argumentArray: [car, odometer])
        if let f = fillupToEdit {
            // If we're editing a fillup, it's okay if the odometer value is the same as it used to be.
            let p2 = NSPredicate(format: "self != %@", argumentArray: [f])
            predicate = NSCompoundPredicate(type: .AndPredicateType, subpredicates: [predicate, p2])
        }
        let req = NSFetchRequest(entityName: "Fillup")
        req.predicate = predicate
        var error : NSError?
        if let fillupsWithSameOdometer = moc.executeFetchRequest(req, error: &error) as? [Fillup] {
            if fillupsWithSameOdometer.count > 0 {
                errorSavingEntry(LocalizedString("ODOMETER_NOT_UNIQUE"))
                return fail
            }
        } else {
            NSLog("Error looking for duplicate odometer entry. %@", error!)
            errorSavingEntry(LocalizedString("ERROR_SAVING_FILLUP"))
            return fail
        }
        
        assert(numberFormatter != nil, "numberFormatter outlet not set")
        
        let oldOdometerRange = odometerRangeForCar(car)
        
        if let fillup = fillupToEdit ?? NSEntityDescription.insertNewObjectForEntityForName("Fillup", inManagedObjectContext: moc) as? Fillup {
            fillup.date = fillUpDate
            fillup.odometer = odometer
            fillup.volume = volume
            fillup.volumeUnit = localeUnits.volumeUnit.rawValue
            fillup.isPartial = isPartialFillUp
            fillup.missedLastFillup = isMissedFillUp
            //Driving style set
            if let ds = dstyle {
                fillup.drivingStyle = NSNumber(float: ds)
                let label = existing ? "edit" : "new"
                Analytics.sharedInstance.eventWithCategory(.UIAction, action: "select_drive_style_add_fuel", label: label)
                
            }else{
                fillup.drivingStyle = nil
            }
            fillup.currency = localeUnits.isoCurrencyCode
            if fillup.car != car && existing {
                Analytics.sharedInstance.eventWithCategory(.UIAction, action: "select_different_car_for_fillup", label: "edit")
            }
            fillup.car = car
            
            var newStationAdded = false
            if let selectedStation = selectedGasStation {
                
                // Look for a station that matches the name and location within 1 meter.
                let req = NSFetchRequest(entityName: "Station")
                req.predicate = NSPredicate(format: "self.name = %@", argumentArray: [selectedStation.name])
                var error : NSError?
                var existingStation : Station?
                if let stations = moc.executeFetchRequest(req, error: &error) as? [Station] {
                    for station in stations {
                        let stationLoc = CLLocation(latitude: station.latitude.doubleValue, longitude: station.longitude.doubleValue)
                        if selectedStation.location.distanceFromLocation(stationLoc) < 1.0 {
                            existingStation = station
                            break
                        }
                    }
                }
                
                if let station = existingStation {
                    fillup.station = station
                } else if let station = NSEntityDescription.insertNewObjectForEntityForName("Station", inManagedObjectContext: moc) as? Station {
                    station.name = selectedStation.name
                    station.address = selectedStation.address
                    let coords = selectedStation.location.coordinate
                    station.latitude = coords.latitude
                    station.longitude = coords.longitude
                    fillup.station = station
                    
                    newStationAdded = true
                }
                
                fillup.station?.lastUsed = NSDate()
            }
            if let ppuvStr = enteredPricePerUnitVolume {
                if let ppvu = numberFormatter?.numberFromString(ppuvStr) as? NSDecimalNumber {
                    fillup.pricePerVolumeUnit = ppvu
                }
            }
            
            let ok = AppDelegate.sharedInstance.saveContext(self)
            
            if ok {
                let newOdometerRange = odometerRangeForCar(car)
                analyticsTrackOdometerRangeChangeWithOldRange(oldOdometerRange, newOdometerRange: newOdometerRange, car: car)
            }
            
            return (true, fillup.station, newStationAdded)
        } else {
            errorSavingEntry(LocalizedString("ERROR_SAVING_FILLUP"))
            return fail
        }
    }
    
    func reloadCarRow() {
        reloadCell(.Car)
    }
    
    override func viewWillAppear(animated: Bool) {
        super.viewWillAppear(animated)
        
        self.reloadCarRow()
        
        navigationItem.title = existing ? LocalizedString("EDIT_FUEL") : LocalizedString("ADD_FUEL")
        
        Analytics.sharedInstance.screenView(existing ? "Edit Fuel" : "Add Fuel")
    }
    
    override func viewDidLoad() {
        
        car = fillupToEdit?.car ?? car
        fillUpDate = fillupToEdit?.date ?? NSDate()
        enteredOdometer = fillupToEdit?.odometer.stringValue
        
        enteredVolume = nil
        enteredVolumeUnit = localeUnits.volumeUnit
        enteredPricePerUnitVolume = nil
        
        
        if let f = fillupToEdit {
            enteredVolume = numberFormatter!.stringFromNumber(f.volume)
            if let ppl = f.pricePerVolumeUnit {
                enteredPricePerUnitVolume = numberFormatter!.stringFromNumber(ppl)
            }
            if let unit = VolumeUnit(rawValue: f.volumeUnit) {
                enteredVolumeUnit = unit
            }
        }
        
        isPartialFillUp = fillupToEdit?.isPartial.boolValue ?? false
        isMissedFillUp = fillupToEdit?.missedLastFillup.boolValue ?? false
        octane = fillupToEdit?.octane.stringValue
        dstyle = fillupToEdit?.drivingStyle?.floatValue
        if let station = fillupToEdit?.station {
            selectedGasStation = GasStationMapViewController.Station(location: CLLocation(latitude: station.latitude.doubleValue, longitude: station.longitude.doubleValue), name:station.name ?? "", address: station.address ?? "")
        } else {
            selectedGasStation = nil
        }
        
        let indexPath = cellMetaIndexPath(.Delete)
        
        if fillupToEdit != nil && indexPath == nil {
            // Add delete section at the end.
            cells.append([.Delete])
            existing = true
        } else if fillupToEdit == nil && indexPath != nil {
            cells.removeAtIndex(indexPath!.section)
        }

        if let s = initialSelectedGasStation {
            let loc = CLLocation(latitude: s.latitude.doubleValue, longitude: s.longitude.doubleValue)
            selectedGasStation = GasStationMapViewController.Station(location: loc, name: s.name, address: s.address ?? "")
        }
        
        if !GasStationLocationMonitor.capableOfShowingNearbyStations() {
            if let indexPath = cellMetaIndexPath(.Station) {
                cells[indexPath.section].removeAtIndex(indexPath.row)
            }
        }
        
        super.viewDidLoad()
        
        let datePickerCell = tableView.dequeueReusableCellWithIdentifier(CellMeta.DatePicker.rawValue) as! DatePickerTableViewCell
        datePickerRowHeight = datePickerCell.datePicker.intrinsicContentSize().height
    }
    
    var shouldSetInitialFirstResponder = false
    private var hasSetFirstResponder = false
    
    override func viewDidAppear(animated: Bool) {
        super.viewDidAppear(animated)
        
        if shouldSetInitialFirstResponder && !hasSetFirstResponder {
            hasSetFirstResponder = true
            if let odometerCell = tableViewCellFromCellMeta(.Odometer) as? LabelTextFieldTableViewCell {
                odometerCell.textField.becomeFirstResponder()
            }
        }
    }
    
    override func tableView(tableView: UITableView, heightForRowAtIndexPath indexPath: NSIndexPath) -> CGFloat {
        
        // -------------------------------------------------------------------------------------------
        // Setting the row height because Table View Row Height on Dynamic Cells in
        // Interface Builder are not respected. Filed Radar 19836969.
        // Also, without fix, I get:
        // Warning once only: Detected a case where constraints ambiguously suggest a height of zero
        // for a tableview cell's content view. We're considering the collapse unintentional and using
        // standard height instead.
        // -------------------------------------------------------------------------------------------
        
        switch cellMetaAtIndexPath(indexPath) {
        case .DatePicker:
            return datePickerRowHeight!
        default:
            return normalRowHeight
        }
    }
    
    override func numberOfSectionsInTableView(tableView: UITableView) -> Int {
        return cells.count
    }
    
    override func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return cells[section].count
    }
    
    override func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        let cellMeta = cellMetaAtIndexPath(indexPath)
        let cellAny : AnyObject? = tableView.dequeueReusableCellWithIdentifier(cells[indexPath.section][indexPath.row].rawValue)
        assert(cellAny != nil, "Not able to create cell with reuse identifier \(cellMeta.rawValue)")
        let cell = cellAny as! UITableViewCell
        
        switch cellMeta {
        case .Car:
            let c = cell as! LabelDetailTableViewCell
            c.detailLabel.text = car.nickname
        case .Date:
            let c = cell as! DateTableViewCell
            c.dateFormatter.locale = localeUnits.locale
            c.date = fillUpDate
            c.dateLabel.textColor = cellMetaIndexPath(.DatePicker) != nil ? c.selectedColor : nil
        case .Odometer:
            let c = cell as! LabelTextFieldTableViewCell
            let locKeys = DistanceUnitLocalizationKeys(distanceUnit: DistanceUnit(rawValue: car.odometerUnit)!)
            let unit = LocalizedString(locKeys.distanceUnitAbbreviationKey)
            c.label.text = NSString(format: LocalizedString("ODOMETER_WITH_UNIT_FORMAT"), unit) as String
            c.textField.text = enteredOdometer
        case .Volume:
            let c = cell as! LabelTextFieldTableViewCell
            let locKeys = VolumeUnitLocalizationKeys(volumeUnit: enteredVolumeUnit)
            c.label.text = LocalizedString(locKeys.volumeUnitKey)
            c.textField.text = enteredVolume
        case .PricePerVolume:
            let c = cell as! LabelSubtitleTextFieldTableViewCell
            let locKeys = VolumeUnitLocalizationKeys(volumeUnit: enteredVolumeUnit)
            let pricePerVolumeFormatString = LocalizedString(locKeys.pricePerVolumeFormatKey)
            c.label.text = NSString(format: pricePerVolumeFormatString, currencyFormatter.currencySymbol ?? "") as String
            c.textField.text = enteredPricePerUnitVolume
            c.subtitleLabel.text = NSString(format: LocalizedString("TOTAL_PRICE_FORMAT"), currencyFormatter.stringFromNumber(totalPrice) ?? "0") as String
        case .PartialFillup:
            let c = cell as! SwitchTableViewCell
            c.label.text = LocalizedString("PARTIAL_FILLUP")
            c.switchView.on = isPartialFillUp
        case .MissedFillup:
            let c = cell as! SwitchTableViewCell
            c.label.text = LocalizedString("MISSED_FILLUP")
            c.switchView.on = isMissedFillUp
        case .Style:
            let c = cell as! LabelDetailTableViewCell
            c.label.text = LocalizedString("DRIVING_STYLE")
            if let ds = dstyle {
                c.detailLabel.text = NSString(format: LocalizedString("DRIVING_STYLE_DISPLAY"), numberFormatter?.stringFromNumber(Int(NSNumber(float: ds))) ?? "") as String
            }else{
                c.detailLabel.text = LocalizedString("NOT_SET")
            }
        case .Octane:
            let c = cell as! LabelDetailTableViewCell
        case .Station:
            let c = cell as! StationTableViewCell
            if let station = selectedGasStation {
                let str = NSString(format: LocalizedString("STATION_WITH_ADDRESS_FORMAT"), station.name, station.address)
                c.detailTextLabel?.text = str as String
            }else{
                c.detailTextLabel?.text = LocalizedString("NOT_SET")
            }
        case .DatePicker:
            let c = cell as! DatePickerTableViewCell
            c.datePicker.date = fillUpDate
        case .Delete:
            break
        }
        
        return cell
    }

    // MARK: - Navigation
    
    override func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath) {
        let cell = cellMetaAtIndexPath(indexPath)
        
        view.endEditing(true)
        tableView.deselectRowAtIndexPath(indexPath, animated: true)
        
        let datePickerIndex = cellMetaIndexPath(.DatePicker)
        if let pickerIndex = datePickerIndex {
            cells[pickerIndex.section].removeAtIndex(pickerIndex.row)
            
            tableView.beginUpdates()
            tableView.deleteRowsAtIndexPaths([pickerIndex], withRowAnimation: .Fade)
            tableView.endUpdates()
            
            if let c = tableViewCellFromCellMeta(.Date) as? DateTableViewCell {
                c.dateLabel.textColor = nil
            }
        }
        
        switch cell {
        case .Car:
            let vc = GarageViewController.newTableController()
            vc.mode = .CarPicker
            vc.carPickerCheckedCar = car
            vc.delegate = self
            self.showViewController(vc)
        case .Date:
            if datePickerIndex == nil {
                cells[indexPath.section].insert(.DatePicker, atIndex: indexPath.row+1)
                tableView.beginUpdates()
                tableView.insertRowsAtIndexPaths([NSIndexPath(forRow: indexPath.row+1, inSection: indexPath.section)], withRowAnimation: .Fade)
                tableView.endUpdates()
                
                if let c = tableViewCellFromCellMeta(.Date) as? DateTableViewCell {
                    c.dateLabel.textColor = c.selectedColor
                }
            }
        case .Odometer, .Volume:
            if let c = tableView.cellForRowAtIndexPath(indexPath) as? LabelTextFieldTableViewCell {
                c.textField.becomeFirstResponder()
            }
        case .PartialFillup:
            break
        case .MissedFillup:
            break
        case .DatePicker:
            break
        case .Style:
            let vc = DriveStyleViewController.newController()
            vc.delegate = self
            vc.drivestyle = dstyle
            self.showViewController(vc)            
        case .Octane:
            break
        case .PricePerVolume:
            if let c = tableView.cellForRowAtIndexPath(indexPath) as? LabelSubtitleTextFieldTableViewCell {
                c.textField.becomeFirstResponder()
            }
        case .Station:
//            let vc = GasStationTableViewController.newController()
//            vc.delegate = self
//            self.showViewController(vc)
            let vc = GasStationMapViewController()
            vc.delegate = self
            self.showViewController(vc, sender: self)
        case .Delete:
            confirmDeleteEntry()
        }
    }
    
    private func reloadCell(c : CellMeta) {
        if let indexPath = cellMetaIndexPath(c) {
            tableView.reloadRowsAtIndexPaths([indexPath], withRowAnimation: .None)
        } else {
            println("Didn't find \(c) in cells.")
        }
    }

    @IBAction func dateChanged(sender: UIDatePicker) {
        fillUpDate = sender.date
        reloadCell(.Date)
    }
    
    func confirmDeleteEntry() {
        let deleteAction = UIAlertAction(title: LocalizedString("DELETE_FILLUP_BUTTON_TITLE"), style: .Destructive) {
            (action) -> Void in
            
            if let fillup = self.fillupToEdit {
                
                if let moc = AppDelegate.sharedInstance.managedObjectContext {
                    
                    Analytics.sharedInstance.eventWithCategory(.UIAction, action: "press_delete_fillup", label: "delete")
                    
                    let oldOdometerRange = odometerRangeForCar(self.car)
                    moc.deleteObject(fillup)
                    let ok = AppDelegate.sharedInstance.saveContext(self)
                    
                    if ok {
                        let newOdometerRange = odometerRangeForCar(self.car)
                        analyticsTrackOdometerRangeChangeWithOldRange(oldOdometerRange, newOdometerRange: newOdometerRange, car: self.car)
                    }
                }
            }
            self.navigationController?.popViewControllerAnimated(true)
        }
        
        let cancelAction = UIAlertAction(title: LocalizedString("CANCEL"), style: .Cancel, handler: nil)
        
        let style = UIDevice.currentDevice().userInterfaceIdiom == .Pad ? UIAlertControllerStyle.Alert : UIAlertControllerStyle.ActionSheet
        let alert = UIAlertController(title: nil, message: nil, preferredStyle: style)
        alert.addAction(deleteAction)
        alert.addAction(cancelAction)
        
        self.presentViewController(alert, animated: true, completion: nil)
    }
    
    // MARK: - GarageViewControllerDelegate
    func garageViewControllerDidSelectCar(controller: GarageViewController, car: Car) {
        self.car = car
        reloadCell(.Car)
    }
    
    // MARK: - DriveStyleViewControllerDelegate
    func driveStyleViewControllerDidSelectStyle(controller: DriveStyleViewController, drivestyle: Float?) {
        self.dstyle = drivestyle
        reloadCell(.Style)
    }

    // MARK: - GasStationMapViewControllerDelegate
    func gasStationMapViewControllerDidSelectStation(controller: GasStationMapViewController, station: GasStationMapViewController.Station) {
        selectedGasStation = station
        reloadCell(.Station)
        self.navigationController?.popToViewController(self, animated: true)
    }
}

private func odometerRangeForCar(car : Car) -> Int {
    //Figure out distance tracked changed since last fill-up entry for analytics tracking
    var maxOdometer = 0
    var minOdometer = 0
    let req = NSFetchRequest(entityName: "Fillup")
    req.predicate = NSPredicate(format: "self.car==%@", argumentArray: [car])
    req.sortDescriptors = [NSSortDescriptor(key: "odometer", ascending: false)]
    req.fetchLimit = 1
    let moc = car.managedObjectContext!
    var error : NSError?
    if let maxFillup = (moc.executeFetchRequest(req, error: &error))?.first as? Fillup {
        maxOdometer = maxFillup.odometer.integerValue
        req.sortDescriptors = [NSSortDescriptor(key: "odometer", ascending: true)]
        req.fetchLimit = 1
        if let minFillup = (moc.executeFetchRequest(req, error: &error))?.last as? Fillup {
            minOdometer = minFillup.odometer.integerValue
        }
    } else {
        println("Error quering max/min odometer: \(error)")
    }
    return maxOdometer - minOdometer
}

private func analyticsTrackOdometerRangeChangeWithOldRange(oldOdometerRange : Int, #newOdometerRange: Int, #car : Car) {
    let odometerRangeDelta = newOdometerRange - oldOdometerRange
    if odometerRangeDelta != 0 {
        var action = "UUID-" + AppDelegate.sharedInstance.analyticsUUID.UUIDString + "-carID-" + car.objectID.URIRepresentation().description
        Analytics.sharedInstance.eventWithCategory(.DistanceTracked, action: action, label: car.odometerUnit, value: odometerRangeDelta)
    }
}
