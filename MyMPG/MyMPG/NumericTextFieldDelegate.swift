//
//  NumericTextFieldDelegate.swift
//  MyMPG
//
//  Created by Douglas Richardson on 2/9/15.
//  Copyright (c) 2015 True Labs, Inc. All rights reserved.
//

import UIKit

@objc class NumericTextFieldDelegate : ChainedTextFieldDelegate
{
    @IBOutlet var formatter : NSNumberFormatter!
    
    override func awakeFromNib() {
        assert(formatter != nil, "NumericTextFieldDelegate requires a formatter to be set")
    }
    
    override func textField(textField: UITextField,
        shouldChangeCharactersInRange range: NSRange,
        replacementString string: String) -> Bool {
            
            // Allow backspace
            if string.isEmpty { return true }
            
            // Make sure the new string is less than the max allowed string length
            let r = range.location..<(range.location+range.length)
            let newString = (textField.text as NSString).stringByReplacingCharactersInRange(range, withString: string)
            
            if formatter.numberFromString(newString) == nil {
                return false
            }
            
            return super.textField(textField, shouldChangeCharactersInRange: range, replacementString: string)
    }
}
