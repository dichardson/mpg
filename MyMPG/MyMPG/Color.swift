//
//  Color.swift
//  MyMPG
//
//  Created by Douglas Richardson on 3/5/15.
//  Copyright (c) 2015 True Labs, Inc. All rights reserved.
//

import Foundation

extension UIColor {
    convenience init(photoshopR: Int, g: Int, b: Int) {
        self.init(red: CGFloat(photoshopR) / 255.0, green: CGFloat(g) / 255.0, blue: CGFloat(b) / 255.0, alpha: 1.0 )
    }
    
    func colorByScalingSaturation(scale : CGFloat) -> UIColor {
        var hue = CGFloat(0)
        var saturation = CGFloat(0)
        var brightness = CGFloat(0)
        var alpha = CGFloat(0)
        getHue(&hue, saturation: &saturation, brightness: &brightness, alpha: &alpha)
        return UIColor(hue: hue, saturation: saturation*scale, brightness: brightness, alpha: alpha)
    }
}
