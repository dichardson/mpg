//
//  NextTextFieldDelegate.swift
//  MyMPG
//
//  Created by Douglas Richardson on 2/12/15.
//  Copyright (c) 2015 True Labs, Inc. All rights reserved.
//

import UIKit

@objc class NextTextFieldDelegate : ChainedTextFieldDelegate {
    
    @IBOutlet var textFields : [UITextField]?
    
    override func textFieldShouldReturn(textField: UITextField) -> Bool {
        if let tf = textFields {
            if let i = tf.indexOfFirstObjectPassingTest({ $0 === textField }) {
                if i+1 < tf.count {
                    tf[i+1].becomeFirstResponder()
                }
            }
        }
        return false
    }
}


