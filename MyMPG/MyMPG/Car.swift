//
//  Car.swift
//  MyMPG
//
//  Created by Douglas Richardson on 2/12/15.
//  Copyright (c) 2015 True Labs, Inc. All rights reserved.
//

import Foundation
import CoreData

public class Car: NSManagedObject {

    @NSManaged public var nickname: String
    @NSManaged public var odometerUnit: String
    @NSManaged public var defaultDrivingStyle: NSNumber?
    @NSManaged public var fillups: NSSet
    @NSManaged public var epaVehicleId : String?

}
