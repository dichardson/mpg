//
//  DecimalNumbers.swift
//  MyMPG
//
//  Created by Douglas Richardson on 3/5/15.
//  Copyright (c) 2015 True Labs, Inc. All rights reserved.
//

import Foundation

let noRaiseBehavior = NSDecimalNumberHandler(
    roundingMode: NSRoundingMode.RoundPlain,
    scale: Int16(NSDecimalNoScale),
    raiseOnExactness: false,
    raiseOnOverflow: false,
    raiseOnUnderflow: false,
    raiseOnDivideByZero: false)


