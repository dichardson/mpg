//
//  EditCarTableViewController.swift
//  MyMPG
//
//  Created by Douglas Richardson on 2/7/15.
//  Copyright (c) 2015 True Labs, Inc. All rights reserved.
//

import UIKit

protocol EditCarTableViewControllerDelegate {
    func editCarControllerRequestsSave(controller : EditCarTableViewController)
}

class EditCarTableViewController: UITableViewController, DistanceUnitSelectionControllerDelegate, EPAVehiclePickerTableViewControllerDelegate {
    
    var editCarDelegate : EditCarTableViewControllerDelegate?
    
    class func newController() -> EditCarTableViewController {
        let sb = UIStoryboard(name: "EditCar", bundle: nil)
        return sb.instantiateInitialViewController() as! EditCarTableViewController
    }
    
    // MARK: Input/output values
    var nickname : String?
    var dstyle: Float?
    var odometerUnit : DistanceUnit?
    var epaVehicleId : Int?
    
    private var epaVehicleDatabase = EPAVehicleDatabase()
    
    var existing = false
    var hasSetInitialFirstResponder = false
    
    private enum Row {
        case Nickname
        case OdometerUnits
        case DrivingStyle
        case EPAVehicle
    }
    
    private var localeUnits : LocaleUnits!
    
    private var sections : [[Row]] = [
        [.Nickname],
        [.EPAVehicle],
        [.OdometerUnits]
    ]
    
    // MARK: Outlets
    @IBOutlet var saveButton : UIBarButtonItem!
    
    override func viewDidLoad() {
        
        localeUnits = LocaleUnits()
        
        if existing {
            navigationItem.title = LocalizedString("EDIT_CAR")
        } else {
            navigationItem.title = LocalizedString("ADD_CAR")
        }
        
        odometerUnit = odometerUnit ?? localeUnits.distanceUnit
        
//        if let ds = dstyle{
//            drivingStyleTextLabel.text = (numberFormatter?.stringFromNumber(Int(NSNumber(float: ds))) ?? "") + "% " + LocalizedString("HIGHWAY")
//        }else{
//            drivingStyleTextLabel.text = LocalizedString("NOT_SET")
//        }
        
        updateSaveEnabled()
        
        tableView.rowHeight = StandardTableViewRowHeight
        
        carCell.registerNib(tableView)
        
        Analytics.sharedInstance.screenView(existing ? "Edit Car" : "Add Car")
    }
    
    private func makeNicknameTextFieldFirstResponder() {
        // This is a new Car, so make nickname first responder, since it's the only required field.
        if let path = indexPathOfFirstObjectPassingTest(sections, { $0 == .Nickname }) {
            if let cell = tableView.cellForRowAtIndexPath(path) as? LabelTextFieldTableViewCell {
                cell.textField.becomeFirstResponder()
            }
        }
    }
    
    override func viewDidAppear(animated: Bool) {
        if !existing && !hasSetInitialFirstResponder {
            // This is a new Car, so make nickname first responder, since it's the only required field.
            hasSetInitialFirstResponder = true
            makeNicknameTextFieldFirstResponder()
        }
    }
    
    @IBAction func savePressed(sender: UIBarButtonItem) {
        
        Analytics.sharedInstance.eventWithCategory(.UIAction, action: "press_save_car", label: existing ? "edit" : "new")
        
        //Driving style set
        if let ds = dstyle {
            // dstyle = NSNumber(float: ds)
            Analytics.sharedInstance.eventWithCategory(.UIAction, action: "select_drive_style_car_default", label: "select")
        }else{
            // drivingStyleTextLabel = nil
        }
        
        editCarDelegate?.editCarControllerRequestsSave(self)
    }
    
    private func updateSaveEnabled() {
        saveButton.enabled = !(nickname?.isEmpty ?? true)
    }
    
    // MARK: UITableViewDataSource
    
    override func numberOfSectionsInTableView(tableView: UITableView) -> Int {
        return sections.count
    }
    
    override func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return sections[section].count
    }
    
    private let textFieldCell = TableViewCellReuseIdAndType<LabelTextFieldTableViewCell>(reuseId: "TextField")
    private let labelSubtitleCell = TableViewCellReuseIdAndType<LabelDetailTableViewCell>(reuseId: "TitleDetail")
    private let carCell = NibTableViewCellReuseIdAndType<CarDetailTableViewCell>(reuseId: "CarDetail")
    
    override func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        switch sections[indexPath.section][indexPath.row] {
        case .Nickname:
            let c = textFieldCell.dequeueFromTableView(tableView)
            c.label.text = LocalizedString("NICKNAME")
            c.textField.placeholder = LocalizedString("NICKNAME_PLACEHOLDER")
            c.textField.text = nickname
            c.textField.removeTarget(self, action: nil, forControlEvents: .AllEvents)
            c.textField.addTarget(self, action: Selector("nicknameChanged:"), forControlEvents: .EditingChanged)
            c.textField.addTarget(self, action: Selector("donePressed:"), forControlEvents: .EditingDidEndOnExit)
            c.accessoryType = .None
            return c
        case .DrivingStyle:
            let c = labelSubtitleCell.dequeueFromTableView(tableView)
            c.label.text = LocalizedString("DRIVING_STYLE")
            c.detailLabel.text = LocalizedString("NOT_SET")
            c.accessoryType = .DisclosureIndicator
            return c
        case .OdometerUnits:
            let locKeys = DistanceUnitLocalizationKeys(distanceUnit: odometerUnit!)
            let c = labelSubtitleCell.dequeueFromTableView(tableView)
            c.label.text = LocalizedString("ODOMETER_UNITS")
            c.label.enabled = true
            c.userInteractionEnabled = true
            c.detailLabel.text = LocalizedString(locKeys.distanceUnitKey)
            c.accessoryType = .DisclosureIndicator
            return c
        case .EPAVehicle:
            let c = carCell.dequeueFromTableView(tableView)
            var vehicle : EPAVehicle?
            if let id = epaVehicleId {
                vehicle = epaVehicleDatabase.epaVehicleForId(id)
            }
            c.epaVehicle = vehicle
            c.accessoryType = .DisclosureIndicator
            return c
        }
    }
    
    // MARK: UITableViewDelegate
    
    var epaPickerNavController : UINavigationController?
    
    override func tableView(tableView: UITableView, titleForFooterInSection section: Int) -> String? {
        switch sections[section][0] {
        case .Nickname:
            return nil
        case .OdometerUnits:
            return nil
        case .EPAVehicle:
            return LocalizedString("EPA_DESCRIPTION")
        default:
            return nil
        }
    }
    
    override func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath) {
        
        var shouldEndEditing = true
        
        switch sections[indexPath.section][indexPath.row] {
        case .Nickname:
            makeNicknameTextFieldFirstResponder()
            shouldEndEditing = false
        case .DrivingStyle:
            println("Driving Style")
        case .OdometerUnits:
            let vc = DistanceUnitSelectionController(style: tableView.style)
            vc.distanceUnitDelegate = self
            if let unit = odometerUnit {
                vc.selectedUnit = unit
            }
            vc.navigationItem.title = LocalizedString("ODOMETER_UNITS")
            self.showViewController(vc)
        case .EPAVehicle:
            let vc = EPAVehiclePickerTableViewController(style: .Plain, vehicleDatabase: epaVehicleDatabase)
            vc.epaVehicleDatabase = epaVehicleDatabase
            vc.pickerDelegate = self
            epaPickerNavController = UINavigationController(rootViewController: vc)
            self.presentViewController(epaPickerNavController!, animated: true, completion: nil)
        }
        
        if shouldEndEditing {
            view.endEditing(true)
        }
    }
    
    override func tableView(tableView: UITableView, heightForRowAtIndexPath indexPath: NSIndexPath) -> CGFloat {
        switch sections[indexPath.section][indexPath.row] {
        case .EPAVehicle:
            return 57
        default:
            return StandardTableViewRowHeight
        }
    }
    
    // MARK: DistanceUnitSelectionControllerDelegate
    func distanceUnitSelectionController(controller: DistanceUnitSelectionController, didSelectUnit unit: DistanceUnit) {
        odometerUnit = unit
        tableView.reloadData()
    }
    
    // MARK: Misc
    
    func nicknameChanged(sender : UITextField) {
        nickname = sender.text.stringByTrimmingCharactersInSet(NSCharacterSet.whitespaceAndNewlineCharacterSet())
        updateSaveEnabled()
    }
    
    func donePressed(sender : UITextField) {
        sender.resignFirstResponder()
    }
    
    private func reloadRows(rows : [Row], withRowAnimation rowAnimation: UITableViewRowAnimation) {
        var indexPaths = indexPathsOfObjectsPassingTest(sections) { contains(rows, $0) }
        tableView.reloadRowsAtIndexPaths(indexPaths, withRowAnimation: rowAnimation)
    }
    
    // MARK: EPAVehiclePickerTableViewControllerDelegate
    
    func epaVehiclePickerDidSelectVehicle(vehicle: EPAVehicle) {
        epaVehicleId = vehicle.id
        reloadRows([.EPAVehicle], withRowAnimation: .None)
        epaPickerNavController?.dismissViewControllerAnimated(true, completion: nil)
        epaPickerNavController = nil
    }
    
    func epaVehiclePickerDidCancel() {
        epaPickerNavController?.dismissViewControllerAnimated(true, completion: nil)
        epaPickerNavController = nil
    }
    
    func epaVehiclePickerDidClear() {
        if epaVehicleId != nil {
            Analytics.sharedInstance.eventWithCategory(.UIAction, action: "press_clear_epa_car_button", label: "clear")
        }
        epaVehicleId = nil
        reloadRows([.EPAVehicle], withRowAnimation: .None)
        epaPickerNavController?.dismissViewControllerAnimated(true, completion: nil)
        epaPickerNavController = nil
    }
}

