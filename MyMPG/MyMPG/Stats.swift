//
//  Stats.swift
//  MyMPG
//
//  Created by Douglas Richardson on 3/5/15.
//  Copyright (c) 2015 True Labs, Inc. All rights reserved.
//

import Foundation

public struct Stats
{
    public let bestFuelEfficiency : FuelEfficiency?
    public let averageFuelEfficiency : FuelEfficiency?
    public let costPerDistance : ValuePerDistanceUnit?
    public let costPerFillup : NSDecimalNumber?
    public let costPerVolume : ValuePerVolumeUnit?
    public let currencyCode : String? // currency used for all cost based calculations
}

public func statsFromEconomies(economies : [FuelEconomy]) -> Stats {
    
    if economies.count == 0 {
        return Stats(bestFuelEfficiency: nil, averageFuelEfficiency: nil, costPerDistance: nil, costPerFillup: nil, costPerVolume: nil, currencyCode: nil)
    }
    
    // Best
    let maxKmL = economies.reduce(0.0) {
        if $1.liters.isZero {
            return $0
        }
        let kmL = $1.kilometers / $1.liters
        return kmL.isNaN ? $0 : max($0, kmL)
    }
    
    let maxFuelEfficiency = FuelEfficiency(value: maxKmL, unit: .kmL)
    
    // Average
    let totalKilometers = economies.reduce(0.0) {
        let r = $0 + $1.kilometers
        return r.isNaN ? $0 : r
    }
    let totalLiters = economies.reduce(0.0) {
        let r = $0 + $1.liters
        return r.isNaN ? $0 : r
    }
    let avgKmL = totalKilometers / totalLiters
    let avgFuelEfficiency = FuelEfficiency(value: avgKmL, unit: .kmL)
    
    // Average Cost per Distance
    let economiesWithCostsAllCurrencies = economies.filter { $0.cost != nil && $0.currencyCode != nil }
    
    // Find the currency associated with the most fuel volume
    var volumeByCurrency = [String : Double]()
    for e in economiesWithCostsAllCurrencies {
        if let code = e.currencyCode {
            volumeByCurrency[code] = (volumeByCurrency[code] ?? 0.0) + e.liters
        }
    }
    var maxCurrencyCode : String?
    var maxCurrencyValue = Double(0)
    for (k, v) in volumeByCurrency {
        if v > maxCurrencyValue {
            maxCurrencyValue = v
            maxCurrencyCode = k
        }
    }
    
    
    var costPerFillup : NSDecimalNumber?
    var costPerDistance : ValuePerDistanceUnit?
    var costPerVolume : ValuePerVolumeUnit?
    
    let economiesWithCosts = economiesWithCostsAllCurrencies.filter { $0.currencyCode == maxCurrencyCode }
    if economiesWithCosts.count > 0 {
        let totalCosts = economiesWithCosts.reduce(NSDecimalNumber.zero()) {
            $1.cost!.decimalNumberByAdding($0, withBehavior: noRaiseBehavior)
        }
        let totalKilometersWithCosts = economies.reduce(0.0) { $1.kilometers + $0 }
        
        let costPerKm = totalCosts.doubleValue / totalKilometersWithCosts
        costPerDistance = ValuePerDistanceUnit(value: costPerKm, perUnit: .kilometer)
        
        // Average Cost per Fill-up
        let totalFillupsWithCosts = economiesWithCosts.reduce(Int(0)) { $0 + $1.fillupCount }
        costPerFillup = totalCosts.decimalNumberByDividingBy(NSDecimalNumber(integer:totalFillupsWithCosts))
        
        // Average Cost per Liter
        let totalLitersWithCosts = economiesWithCosts.reduce(0.0) { $0 + $1.liters }
        let costPerLiter = totalCosts.doubleValue / totalLitersWithCosts
        costPerVolume = ValuePerVolumeUnit(value: costPerLiter, perUnit: .liter)
    }

    return Stats(
        bestFuelEfficiency: maxFuelEfficiency,
        averageFuelEfficiency: avgFuelEfficiency,
        costPerDistance: costPerDistance,
        costPerFillup: costPerFillup,
        costPerVolume: costPerVolume,
        currencyCode: maxCurrencyCode)
}
