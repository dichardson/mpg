//
//  EPAVehiclePickerTableViewController.swift
//  MyMPG
//
//  Created by Douglas Richardson on 3/18/15.
//  Copyright (c) 2015 True Labs, Inc. All rights reserved.
//

import UIKit

protocol EPAVehiclePickerTableViewControllerDelegate {
    func epaVehiclePickerDidSelectVehicle(vehicle : EPAVehicle)
    func epaVehiclePickerDidCancel()
    func epaVehiclePickerDidClear()
}

class EPAVehiclePickerTableViewController: UITableViewController, EPAVehiclePickerTableViewControllerDelegate {
    
    enum Mode {
        case Year([Int])
        case Make(Int, [String])
        case Model(Int, String, [String])
        case Vehicle([EPAVehicle])
    }
    
    var mode = Mode.Year([])
    var epaVehicleDatabase : EPAVehicleDatabase!
    var pickerDelegate : EPAVehiclePickerTableViewControllerDelegate?
    
    private let yearFormatter = EPAYearFormatter()
    private let numberFormatter = NSNumberFormatter()
    
    convenience init(style: UITableViewStyle, vehicleDatabase : EPAVehicleDatabase) {
        self.init(style: style)
        mode = .Year(vehicleDatabase.years(order: .Descending))
        epaVehicleDatabase = vehicleDatabase
    }

    override func viewDidLoad() {
        super.viewDidLoad()
        
        let titleLocKey : String
        let modeString : String
        
        switch mode {
        case .Year:
            titleLocKey = "CAR_YEAR"
            let clear = UIBarButtonItem(title: LocalizedString("RESET_CAR"), style: .Plain, target: self, action: Selector("clearPressed"))
            self.navigationItem.leftBarButtonItem = clear
            modeString = "Year"
        case .Make:
            titleLocKey = "CAR_MAKE"
            modeString = "Make"
        case .Model:
            titleLocKey = "CAR_MODEL"
            modeString = "Model"
        case .Vehicle:
            titleLocKey = "CAR_OPTIONS"
            modeString = "Options"
        }
        
        self.navigationItem.title = LocalizedString(titleLocKey)
        
        let cancel = UIBarButtonItem(barButtonSystemItem: UIBarButtonSystemItem.Cancel, target: self, action: Selector("cancelPressed"))
        self.navigationItem.rightBarButtonItem = cancel
        
        let screenName = "EPA Car Selection: \(modeString)"
        Analytics.sharedInstance.screenView(screenName)
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    // MARK: - Table view data source

    override func numberOfSectionsInTableView(tableView: UITableView) -> Int {
        return 1
    }

    override func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        switch mode {
        case .Year(let years):
            return years.count
        case .Make(let year, let makes):
            return makes.count
        case .Model(let year, let make, let models):
            return models.count
        case .Vehicle(let vehicles):
            return vehicles.count
        }
    }
    
    private let defaultCell = TableViewCellReuseIdAndType<DefaultTableViewCell>(reuseId: "Default")

    override func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        let cell = defaultCell.dequeueFromTableView(tableView)
        
        cell.textLabel?.adjustsFontSizeToFitWidth = true
        cell.textLabel?.minimumScaleFactor = 0.5
        
        switch mode {
        case .Year(let years):
            cell.textLabel?.text = yearFormatter.displayYear(years[indexPath.row])
            cell.accessoryType = .DisclosureIndicator
        case .Make(let year, let makes):
            cell.textLabel?.text = makes[indexPath.row]
            cell.accessoryType = .DisclosureIndicator
        case .Model(let year, let make, let models):
            cell.textLabel?.text = models[indexPath.row]
            cell.accessoryType = .DisclosureIndicator
        case .Vehicle(let vehicles):
            cell.textLabel?.text = vehicleOptionsDisplayString(vehicles[indexPath.row])
            cell.textLabel?.numberOfLines = 2
            cell.accessoryType = .None
        }
        
        return cell
    }
    
    override func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath) {
        var nextMode : Mode?
        
        switch mode {
        case .Year(let years):
            let year = years[indexPath.row]
            nextMode = .Make(year, epaVehicleDatabase.makesForYear(year))
        case .Make(let year, let makes):
            let make = makes[indexPath.row]
            nextMode = .Model(year, make, epaVehicleDatabase.modelsForYear(year, make: make))
        case .Model(let year, let make, let models):
            let model = models[indexPath.row]
            nextMode = .Vehicle(epaVehicleDatabase.epaVehiclesForYear(year, make: make, model: model))
        case .Vehicle(let vehicles):
            var label = vehicles[indexPath.row].year.description + "-" + vehicles[indexPath.row].make + "-" + vehicles[indexPath.row].model
            Analytics.sharedInstance.eventWithCategory(.UIAction, action: "select_epa_car", label: label)
            
            pickerDelegate?.epaVehiclePickerDidSelectVehicle(vehicles[indexPath.row])
            return
        }
        
        if let m = nextMode {
            let vc = EPAVehiclePickerTableViewController(style: tableView.style)
            vc.mode = m
            vc.epaVehicleDatabase = epaVehicleDatabase
            vc.pickerDelegate = self
            self.showViewController(vc)
            return
        }
    }
    
    func epaVehiclePickerDidSelectVehicle(vehicle: EPAVehicle) {
        // forward to next delegate
        self.pickerDelegate?.epaVehiclePickerDidSelectVehicle(vehicle)
    }
    
    func epaVehiclePickerDidCancel() {
        self.pickerDelegate?.epaVehiclePickerDidCancel()
    }
    
    func epaVehiclePickerDidClear() {
        self.pickerDelegate?.epaVehiclePickerDidClear()
    }

    // MARK: Misc
    private func vehicleOptionsDisplayString(vehicle : EPAVehicle) -> String {
        let options = vehicle.displayOptionsWithNumberFormatter(numberFormatter)
        let separator = LocalizedString("CAR_OPTIONS_SEPARATOR")
        return separator.join(options)
    }
    
    func cancelPressed() {
        self.pickerDelegate?.epaVehiclePickerDidCancel()
    }
    
    func clearPressed() {
        self.pickerDelegate?.epaVehiclePickerDidClear()
    }
}
