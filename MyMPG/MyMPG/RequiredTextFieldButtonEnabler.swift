//
//  RequiredTextFieldButtonEnabler.swift
//  MyMPG
//
//  Created by Douglas Richardson on 2/14/15.
//  Copyright (c) 2015 True Labs, Inc. All rights reserved.
//

import UIKit

class RequiredTextFieldButtonEnabler : NSObject {
    
    // The text fields required to have content to enable the bar items
    @IBOutlet var requiredTextFields : [UITextField]? {
        didSet {
            updateEnabled()
        }
    }
    
    // The bar items enabled/disabled depending on the context of the required
    // text fields.
    @IBOutlet var barItems : [UIBarItem]? {
        didSet {
            updateEnabled()
        }
    }
    
    @IBAction func textFieldDidChange(sender : UITextField) {
        updateEnabled()
    }
    
    func updateEnabled() {
        var enable = true
        if let tf = requiredTextFields {
            if let items = barItems {
                for t in tf {
                    if count(t.text) == 0 {
                        enable = false
                        break
                    }
                }
                for b in items {
                    b.enabled = enable
                }
            }
        }
    }
}

