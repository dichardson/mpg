//
//  Fillup.swift
//  MyMPG
//
//  Created by Douglas Richardson on 2/12/15.
//  Copyright (c) 2015 True Labs, Inc. All rights reserved.
//

import Foundation
import CoreData

// Fillups normalize volume into liters, because it is the only unit that can represent the
// other units without loss of fidelity (that is, without losing information the user entered
// due to rounding). An alternate approach would be to store a raw volume and unit in separate
// columns.

public class Fillup: NSManagedObject {

    // required
    @NSManaged public var date: NSDate
    @NSManaged public var odometer: NSDecimalNumber
    @NSManaged public var volume: NSDecimalNumber
    @NSManaged public var volumeUnit: String
    @NSManaged public var isPartial: NSNumber
    @NSManaged public var missedLastFillup: NSNumber
    @NSManaged public var car: MyMPG.Car
    
    // optional
    @NSManaged public var pricePerVolumeUnit: NSDecimalNumber?
    @NSManaged public var currency: String?
    @NSManaged public var station: MyMPG.Station?
    @NSManaged public var drivingStyle: NSNumber?
    @NSManaged public var octane: NSDecimalNumber
}

extension Fillup {
    var totalPrice : NSDecimalNumber? {
        get {
            return pricePerVolumeUnit?.decimalNumberByMultiplyingBy(volume, withBehavior: noRaiseBehavior)
        }
    }
    
    var fuelVolume : Volume {
        get {
            let unit = VolumeUnit(rawValue: volumeUnit)
            return Volume(value: volume.doubleValue, unit: unit!)
        }
    }
    
    var fuelPricePerVolumeUnit : ValuePerVolumeUnit? {
        if let ppvu = pricePerVolumeUnit {
            let unit = VolumeUnit(rawValue: volumeUnit)
            return ValuePerVolumeUnit(value: ppvu.doubleValue, perUnit: unit!)
        } else {
            return nil
        }
    }
}
