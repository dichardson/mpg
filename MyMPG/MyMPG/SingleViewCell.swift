//
//  SingleViewCell.swift
//  MyMPG
//
//  Created by Douglas Richardson on 3/11/15.
//  Copyright (c) 2015 True Labs, Inc. All rights reserved.
//

import UIKit

class SingleViewCell : UITableViewCell {
    
    var view : UIView?
    
    func setSubview(view : UIView) {
        self.view?.removeFromSuperview()
        self.view = view
        contentView.addSubview(view)
    }
    
    override func layoutSubviews() {
        super.layoutSubviews()
        view?.frame = contentView.bounds
    }
}
