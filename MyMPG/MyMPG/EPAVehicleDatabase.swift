//
//  EPAVehicleDatabase.swift
//  MyMPG
//
//  Created by Douglas Richardson on 3/16/15.
//  Copyright (c) 2015 True Labs, Inc. All rights reserved.
//

import Foundation
import SQLite

public class EPAVehicleDatabase {
    private var db : Database
    private let year = Expression<Int>("year")
    private let make = Expression<String>("make")
    private let model = Expression<String>("model")
    private let highway = Expression<Int>("highway")
    private let city = Expression<Int>("city")
    private let combined = Expression<Int>("combined")
    
    private let transmission = Expression<String?>("transmission")
    private let drive = Expression<String?>("drive")
    private let cylinders = Expression<Int?>("cylinders")
    private let displacementX10 = Expression<Int?>("displacementX10")
    private let engineDescription = Expression<String?>("engine_description")
    private let transmissionDescription = Expression<String?>("transmission_description")
    private let fuelType = Expression<String?>("fuel_type")
    private let turbocharged = Expression<Bool>("turbocharged")
    private let supercharged = Expression<Bool>("supercharged")
    
    private let id = Expression<Int>("id")
    
    public required init() {
        let path = NSBundle.mainBundle().pathForResource("epa-vehicles", ofType: "db")!
        db = Database(path, readonly: true)
        //db.trace(println)
    }
    
    public enum Order {
        case Ascending
        case Descending
    }
    
    public func years(order : Order = .Descending) -> [Int] {
        var query = vehicles.select(distinct: year)
        switch order {
        case .Ascending:
            query = query.order(year.asc)
        case .Descending:
            query = query.order(year.desc)
        }
        return map(query) { $0[self.year] }
    }
    
    // Makes not ordered per current locale. If you need that, do it after the call.
    public func makesForYear(year : Int) -> [String] {
        let query = vehicles.select(distinct: make).filter(self.year == year)
        return map(query) { $0[self.make] }
    }
    
    public func modelsForYear(year : Int, make : String) -> [String] {
        let query = vehicles.select(distinct: model).filter(self.year == year && self.make == make)
        return map(query) { $0[self.model] }
    }
    
    private func epaVehiclesWithFilterCondition(condition : Expression<Bool>) -> [EPAVehicle] {
        let query = vehicles.filter(condition)
        let ten = NSDecimalNumber(integer: 10)
        return map(query) {
            let r = $0
            var displacement : NSDecimalNumber? = nil
            if let x10Displacement = r[self.displacementX10] {
                displacement = NSDecimalNumber(integer: x10Displacement).decimalNumberByDividingBy(ten)
            }
            return EPAVehicle(id: r[self.id],
                year: r[self.year],
                make: r[self.make],
                model: r[self.model],
                transmission: r[self.transmission],
                drive: r[self.drive],
                cylinders: r[self.cylinders],
                displacementInLiters: displacement,
                engineDescription: r[self.engineDescription],
                transmissionDescription: r[self.transmissionDescription],
                fuelType: r[self.fuelType],
                turbocharged: r[self.turbocharged],
                supercharged: r[self.supercharged],
                highwayMPG: r[self.highway],
                cityMPG: r[self.city],
                combinedMPG: r[self.combined])
        }
    }
    
    public func epaVehiclesForYear(year : Int, make : String, model : String) -> [EPAVehicle] {
        return epaVehiclesWithFilterCondition(self.year == year && self.make == make && self.model == model)
    }
    
    public func epaVehicleForId(id : Int) -> EPAVehicle? {
        return epaVehiclesWithFilterCondition(self.id == id).first
    }
    
    private var vehicles : Query {
        get {
            return db["epa_vehicles"]
        }
    }
}

public struct EPAVehicle {
    let id : Int
    let year : Int
    let make : String
    let model : String
    let transmission : String?
    let drive : String?
    let cylinders : Int?
    let displacementInLiters : NSDecimalNumber?
    let engineDescription : String?
    let transmissionDescription : String?
    let fuelType : String?
    let turbocharged : Bool
    let supercharged : Bool
    let highwayMPG : Int
    let cityMPG : Int
    let combinedMPG : Int
}

extension EPAVehicle {
    func displayOptionsWithNumberFormatter(numberFormatter : NSNumberFormatter) -> [String] {
        var r = [String]()
        if let t = transmission {
            r.append(t)
        }
        if let d = drive {
            r.append(d)
        }
        if let c = cylinders {
            if let s = numberFormatter.stringFromNumber(NSNumber(integer: c)) {
                let cylindersStr = NSString(format: LocalizedString("CAR_CYLINDERS_FORMAT"), s)
                r.append(cylindersStr as String)
            }
        }
        if let displacement = displacementInLiters {
            // In the US, some cars are customarily shown in liters while others are shown in
            // cubic inches. Since we can't distinguish, and since the rest of the world is unlikely
            // to use cubic inches, we'll always show liters here.
            if let displacementStr = numberFormatter.stringFromNumber(displacement) {
                let locKeys = VolumeUnitLocalizationKeys(volumeUnit: .liter)
                let liters = LocalizedString(locKeys.abbreviatedVolumeUnitKey)
                let str = NSString(format: LocalizedString("ABBREV_VOLUME_WITH_UNITS_FORMAT"), displacementStr, liters)
                r.append(str as String)
            }
        }
        if let ft = fuelType {
            if ft != "Regular" {
                // Only append if not regular, because almost all entries are regular.
                r.append(ft)
            }
        }
        if turbocharged {
            r.append(LocalizedString("TURBOCHARGED"))
        }
        if supercharged {
            r.append(LocalizedString("SUPERCHARGED"))
        }
        if let engDesc = engineDescription {
            r.append(engDesc)
        }
        if let transDesc = transmissionDescription {
            r.append(transDesc)
        }
        
        return r.filter{!$0.isEmpty}
    }
}

class EPAYearFormatter {
    let formatter : NSDateFormatter
    let displayCalendar : NSCalendar
    let epaYearCalendar = NSCalendar(calendarIdentifier: NSCalendarIdentifierGregorian)
    
    required init() {
        formatter = NSDateFormatter()
        formatter.dateFormat = NSDateFormatter.dateFormatFromTemplate("y", options: 0, locale: NSLocale.currentLocale())
        displayCalendar = NSCalendar.currentCalendar()
    }
    
    func displayYear(year : Int) -> String? {
        let c = NSDateComponents()
        c.year = year
        c.calendar = epaYearCalendar
        if let d = displayCalendar.dateFromComponents(c) {
            return formatter.stringFromDate(d)
        }
        return nil
    }
}
