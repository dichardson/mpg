//
//  Station.swift
//  MyMPG
//
//  Created by Douglas Richardson on 2/26/15.
//  Copyright (c) 2015 True Labs, Inc. All rights reserved.
//

import Foundation
import CoreData

public class Station: NSManagedObject {

    // required
    @NSManaged public var latitude: NSNumber
    @NSManaged public var longitude: NSNumber
    @NSManaged public var lastUsed: NSDate
    @NSManaged public var name: String // meant for display
    @NSManaged public var fillupRemindersDisabled: Bool
    
    // optional
    @NSManaged public var address: String? // meant for display
    @NSManaged public var fillups: NSSet?

}
