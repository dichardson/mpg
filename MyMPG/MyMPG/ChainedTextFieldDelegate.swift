//
//  ChaingedTextFieldDelegate.swift
//  MyMPG
//
//  Created by Douglas Richardson on 2/12/15.
//  Copyright (c) 2015 True Labs, Inc. All rights reserved.
//

import UIKit

class ChainedTextFieldDelegate : NSObject, UITextFieldDelegate
{
    @IBOutlet var nextDelegate : UITextFieldDelegate?
    
    func textField(textField: UITextField, shouldChangeCharactersInRange range: NSRange, replacementString string: String) -> Bool {
        return nextDelegate?.textField?(textField, shouldChangeCharactersInRange: range, replacementString: string) ?? true
    }
    
    func textFieldDidBeginEditing(textField: UITextField) {
        nextDelegate?.textFieldDidBeginEditing?(textField)
    }
    
    func textFieldDidEndEditing(textField: UITextField) {
        nextDelegate?.textFieldDidEndEditing?(textField)
    }
    
    func textFieldShouldBeginEditing(textField: UITextField) -> Bool {
        return nextDelegate?.textFieldShouldBeginEditing?(textField) ?? true
    }
    
    func textFieldShouldClear(textField: UITextField) -> Bool {
        return nextDelegate?.textFieldShouldClear?(textField) ?? true
    }
    
    func textFieldShouldEndEditing(textField: UITextField) -> Bool {
        return nextDelegate?.textFieldShouldEndEditing?(textField) ?? true
    }
    
    func textFieldShouldReturn(textField: UITextField) -> Bool {
        return nextDelegate?.textFieldShouldReturn?(textField) ?? true
    }
}
