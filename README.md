Building Fuel Econ for Submission to App Store
==============================================

1. In terminal, go to mpg/MyMPG and run: `./increment-build-version.sh`
2. Commit git changes caused by version number increase.
3. Select "Fuel Economy (Release)" scheme
4. Select "iOS Device" target
5. Hold down Option, and select "Product > Clean Build Folder...". This forces the EPA database to get rebuilt and redownloaded.
6. Build and Run Unit Tests on device (Product > Test)
7. Archive (Product > Archive)
8. Tag release candidate in git. For example: `git tag -a v1.0-rc2 -m "v1.0 Release candidate 2"`
9. Push tag to remote repository. E.g., `git push origin v1.0-rc2`
10. In Xcode, select Window > Organizer and then choose Archives.
11. Set the comment in the newly created archive to match the git tag. E.g., v1.0-rc2.
12. Choose Export... and save to Google Drive under Crazy Ideas/mpg/App Store Submissions with a name that has the git tag in it (follow the convension of the other files in that directory).
13. Submit exported archive via Application Loader 3.0 or later.
14. Open iTunes Connect, and submit for approval (either for test flight beta, store, or both).
